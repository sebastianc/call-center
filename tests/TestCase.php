<?php

namespace Tests;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\Traits\DataTables;
use Faker\Generator as Faker;
use App\User;
use Illuminate\Database\Eloquent\Collection;

abstract class TestCase extends BaseTestCase
{

    use CreatesApplication,
        RefreshDatabase,
        DataTables;

    /**
     * The property holds fake data for testing purposes.
     *
     * @var Faker
     */
    protected $faker;

    /**
     * Setup the test environment.
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->setFaker();
    }

    /**
     * Sets application faker.
     *
     * @return void
     */
    public function setFaker(): void
    {
        $this->faker = app(Faker::class);
    }
    
    /**
     * Create fake users in the database and return the collection.
     *
     * @param int $count
     * @param array $attributes
     *
     * @return Collection
     */
    public function getFakeUsers(int $count, array $attributes = []): Collection
    {
        return factory(User::class, $count)->create($attributes);
    }
    
    /**
     * Create fake user in the database and return the entity.
     *
     * @param array $attributes
     *
     * @return User
     */
    public function getFakeUser(array $attributes = []): User
    {
        return $this->getFakeUsers(1, $attributes)->first();
    }
    
    /**
     * Return the URL with the query string.
     *
     * @param string $uri
     * @param array $data
     *
     * @return string
     */
    public function buildUrlWithQueryString(string $uri, array $data = []): string
    {
        return $uri . (empty($data) ? '' : '?' . http_build_query($data));
    }
}

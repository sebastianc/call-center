<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Role;
use Illuminate\Http\Response;

class UsersListTest extends TestCase
{
    
    /**
     * A view users list page test.
     *
     * @return void
     */
    public function testViewPage(): void
    {
        $this->seed('RolesTableSeeder');
        $uri = '/crm/table/users';
        foreach (Role::all() as $role) {
            $user = $this->getFakeUser();
            $user->roles()->attach($role);
            if ($user->isTeamLeader() || $user->isAccountManager()) {
                $this->assertDataTablesViewResponse($user, $uri)
                        ->assertSeeText('This table shows all users in the system');
            } else {
                $this->actingAs($user)
                        ->get($uri)
                        ->assertStatus(Response::HTTP_FORBIDDEN)
                        ->assertSeeText('Unauthorized.');
            }
        }
    }
    
    /**
     * A users list data filters test.
     *
     * @return void
     */
    public function testDataFilters(): void
    {
        $this->seed('UserTableSeeder');
        $this->seed('RolesTableSeeder');
        $uri = $this->buildUrlWithQueryString('/crm/data/users', ['length' => 25]);
        foreach (Role::all() as $role) {
            $user = $this->getFakeUser();
            $user->roles()->attach($role);
            if ($user->isTeamLeader() || $user->isAccountManager()) {
                $this->assertDataTablesJsonResponse($user, $uri);
            } else {
                $this->actingAs($user)
                        ->getJson($uri)
                        ->assertStatus(Response::HTTP_FORBIDDEN)
                        ->assertSeeText('Unauthorized.');
            }
        }
    }
    
    /**
     * Return JSON structure for the data entity response.
     *
     * @return array
     */
    protected function getDataEntityJsonStructure(): array
    {
        return [
            'id', 'name', 'email', 'created_at', 'updated_at',
            'teams' => [
                '*' => [
                    'id', 'name', 'created_at', 'updated_at',
                    'pivot' => [
                        'user_id', 'team_id'
                    ]
                ]
            ],
            'roles' => [
                '*' => [
                    'id', 'title', 'created_at', 'updated_at', 'uri',
                    'pivot' => [
                        'user_id', 'role_id'
                    ]
                ]
            ],
            'edit_link', 'DT_RowId'
        ];
    }
}

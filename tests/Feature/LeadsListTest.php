<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Role;
use App\User;

class LeadsListTest extends TestCase
{
    
    /**
     * A view leads list page test.
     *
     * @return void
     */
    public function testViewPage(): void
    {
        $this->seed('RolesTableSeeder');
        foreach (Role::all() as $role) {
            $user = $this->getFakeUser();
            $user->roles()->attach($role);
            $this->assertDataTablesViewResponse($user, '/crm/table/leads')
                    ->assertSeeText($this->getViewPageHeading($user));
        }
    }
    
    /**
     * A leads list data filters test.
     *
     * @return void
     */
    public function testDataFilters(): void
    {
        $this->seed('LeadTableSeeder');
        $this->assertDataTablesJsonResponse(
            $this->getFakeUser(),
            $this->buildUrlWithQueryString(
                '/crm/data/leads',
                ['length' => 25]
            )
        );
    }
    
    /**
     * Return JSON structure for the data entity response.
     *
     * @return array
     */
    protected function getDataEntityJsonStructure(): array
    {
        return [
            'id', 'first_name', 'last_name', 'email_address', 'phone_number',
            'team_id', 'user_id', 'renewal_manager_id', 'callback', 'created_at',
            'updated_at', 'user' => [
                'id', 'name', 'email', 'created_at', 'updated_at'
            ], 'full_name', 'edit_link', 'DT_RowId', 'DT_RowId'
        ];
    }
    
    /**
     * Return the view page heading for the given user.
     *
     * @param User $user
     *
     * @return string
     */
    private function getViewPageHeading(User $user): string
    {
        $heading = 'This table shows all your leads';
        if ($user->isTeamLeader() || $user->isAccountManager()) {
            $heading = 'This table shows all the leads belonging to people in your teams';
        }
        
        return $heading;
    }
}

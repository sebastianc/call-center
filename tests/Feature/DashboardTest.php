<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Role;

class DashboardTest extends TestCase
{

    /**
     * A view dashboard page test. The page tests the dashboard for a user with
     * all available roles.
     *
     * @return void
     */
    public function testViewPage(): void
    {
        $this->seed('RolesTableSeeder');
        foreach (Role::all() as $role) {
            $methodName = camel_case('getLinksOf' . $role->title);
            $user = $this->getFakeUser();
            $user->roles()->attach($role);
            $this->actingAs($user)
                    ->get('/crm')
                    ->assertViewIs('crm.dashboard')
                    ->assertSuccessful()
                    ->assertSeeInOrder(array_merge([
                        'Dashboard',
                        'You are logged in!',
                        'You have the following roles assigned:',
                        $role->title,
                        'You can view the following tables:'
                    ], $this->{$methodName}()));
        }
    }

    /**
     * Return links visible to the account manager.
     *
     * @return array
     */
    protected function getLinksOfAccountManager(): array
    {
        return [
            '<a href="' . $this->getRouteByList('users') . '">All Users in your team</a>',
            '<a href="' . $this->getRouteByList('leads') . '">All Leads for your team</a>'
        ];
    }

    /**
     * Return links visible to the product manager.
     *
     * @return array
     */
    protected function getLinksOfProductManager(): array
    {
        return $this->getStandardLinks();
    }

    /**
     * Return links visible to the membership manager.
     *
     * @return array
     */
    protected function getLinksOfMembershipManager(): array
    {
        return $this->getStandardLinks();
    }

    /**
     * Return links visible to the renewal manager.
     *
     * @return array
     */
    protected function getLinksOfRenewalsManager(): array
    {
        return $this->getStandardLinks();
    }

    /**
     * Return links visible to the team leader.
     *
     * @return array
     */
    protected function getLinksOfTeamLeader(): array
    {
        return [
            '<a href="' . $this->getRouteByList('users') . '">All Users in all teams</a>',
            '<a href="' . $this->getRouteByList('leads') . '">All Leads for your teams</a>'
        ];
    }

    /**
     * Return links visible to all user roles.
     *
     * @return array
     */
    private function getStandardLinks(): array
    {
        return [
            '<a href="' . $this->getRouteByList('leads') . '">All your Leads</a>',
            '<a href="' . $this->getRouteByList('open-pot') . '">The open pot</a>'
        ];
    }

    /**
     * Return route by the given list.
     *
     * @param string $list
     *
     * @return string
     */
    private function getRouteByList(string $list): string
    {
        return route('crm.list', ['list' => $list]);
    }
}

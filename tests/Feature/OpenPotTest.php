<?php

namespace Tests\Feature;

use App\Lead;

class OpenPotTest extends LeadsListTest
{

    /**
     * A view open pot page test.
     *
     * @return void
     */
    public function testViewPage(): void
    {
        $user = $this->getFakeUser();
        $this->createLeadsForUser($user->id);
        $this->assertDataTablesViewResponse($user, '/crm/table/open-pot')
                ->assertSeeTextInOrder([
                    'This table shows the open pot (leads overdue by seven days)',
                    'You have overdue callbacks (highlighted in red). You will not '
                    . 'be able to view any new leads until these overdue leads have been actioned.'
                ]);
    }
    
    /**
     * A open pot data filters test.
     *
     * @return void
     */
    public function testDataFilters(): void
    {
        $this->seed('UserTableSeeder');
        $user = $this->getFakeUser();
        $this->createLeadsForUser();
        $this->createLeadsForUser($user->id);
        $this->assertDataTablesJsonResponse(
            $user,
            $this->buildUrlWithQueryString(
                '/crm/data/open-pot',
                ['length' => 25]
            )
        );
    }
    
    /**
     * Create dummy leads for the given user.
     *
     * @param int $userId
     *
     * @return void
     */
    private function createLeadsForUser(int $userId = null): void
    {
        factory(Lead::class, 10)->create([
            'user_id' => $userId ?: rand(1, 20),
            'callback' => now()->subDays(rand(7, 10))->format('Y-m-d H:i:s')
        ]);
    }
}

<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Http\Response;

class LoginTest extends TestCase
{

    /**
     * A view login page test.
     *
     * @return void
     */
    public function testViewPage(): void
    {
        $this->get('/login')
                ->assertViewIs('auth.login')
                ->assertSuccessful();
    }
    
    /**
     * A validate required fields on login page test.
     *
     * @return void
     */
    public function testValidationRequiredFields(): void
    {
        $this->post('/login')
                ->assertStatus(Response::HTTP_FOUND)
                ->assertSessionHasErrors([
                    'email', 'password'
                ]);
    }
    
    /**
     * A validate login attempt on login page test.
     *
     * @return void
     */
    public function testValidateLoginAttempt(): void
    {
        $this->post('/login', [
                    'email' => $this->faker->email,
                    'password' => $this->faker->word
                ])
                ->assertStatus(Response::HTTP_FOUND)
                ->assertSessionHasErrors(['email']);
    }
    
    /**
     * A login attempt on login page test.
     *
     * @return void
     */
    public function testLoginAttempt(): void
    {
        $password = 'password';
        $user = $this->getFakeUser(['password' => bcrypt($password)]);
        $this->post('/login', [
                    'email' => $user->email,
                    'password' => $password
                ])
                ->assertStatus(Response::HTTP_FOUND)
                ->assertSessionHasNoErrors()
                ->assertRedirect('/crm');
    }
}

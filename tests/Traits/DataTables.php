<?php

namespace Tests\Traits;

use Illuminate\Foundation\Testing\TestResponse;
use App\User;

trait DataTables
{

    /**
     * Test DataTables view for the given user and URL.
     *
     * @param User $user
     * @param string $uri
     *
     * @return TestResponse
     */
    protected function assertDataTablesViewResponse(User $user, string $uri): TestResponse
    {
        return $this->actingAs($user)
                ->get($uri)
                ->assertViewIs('crm.list')
                ->assertViewHasAll([
                    'page_header', 'list', 'columns', 'filters', 'editable_fields',
                    'warnings'
                ])
                ->assertSuccessful();
    }

    /**
     * Test DataTables JSON response for the given user and URL.
     *
     * @param User $user
     * @param string $uri
     *
     * @return TestResponse
     */
    protected function assertDataTablesJsonResponse(User $user, string $uri): TestResponse
    {
        return $this->actingAs($user)
                ->getJson($uri)
                ->assertJsonStructure($this->getJsonStructure())
                ->assertSuccessful();
    }

    /**
     * Return JSON structure for the response.
     *
     * @return array
     */
    protected function getJsonStructure(): array
    {
        return [
            'draw', 'recordsTotal', 'recordsFiltered',
            'data' => [
                '*' => $this->getDataEntityJsonStructure()
            ]
        ];
    }

    /**
     * Return JSON structure for the data entity response.
     *
     * @return array
     */
    protected function getDataEntityJsonStructure(): array
    {
        return [];
    }
}

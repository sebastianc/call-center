<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Collection;

class MigrateData extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'migrate:data {--force : This will remigrate the data} {--forcenousers : This will remigrate the data without the users table}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Migrate data from the old database in to the new database';

    /**
     * The name of the old database.
     *
     * @var string
     */
    protected $oldDatabaseName;
    
    /**
     * The name of the new database.
     *
     * @var string
     */
    protected $newDatabaseName;
    
    /**
     * The order in which you would like migration files to process.
     *
     * @var array
     */
    protected $orderOfFiles = [
        'users.sql', 'leads.sql', 'update_leads_user_id.sql', 'calls.sql', 'update_calls_user_id.sql',
        'update_calls_lead_id.sql', 'callbacks.sql', 'update_callbacks_user_id.sql',
        'update_callbacks_lead_id.sql', 'notes.sql', 'update_notes_user_id.sql',
        'update_notes_lead_id.sql', 'sms.sql', 'update_sms_user_id.sql', 'update_sms_lead_id.sql'
    ];

    /**
     * Create a new console command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->oldDatabaseName = config('database.connections.legacy-db.database');
        $this->newDatabaseName = config('database.connections.mysql.database');
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle(): void
    {
        $startTime = Carbon::now();
        DB::statement('SET FOREIGN_KEY_CHECKS = 0;');
        DB::statement('SET sql_mode = \'\'');

        if ($this->option('forcenousers')) {
            $this->alert('You have set the <error>--forcenousers</error> option. This will delete the existing data <error>Without deleting the base 48 users</error> from the new database before migration.');
            $this->truncateDatabaseWithoutUsers();
            $this->info('Truncation completed in ' . $this->getTimeIntervalString($startTime) . "\n");
            $startTime = Carbon::now();
        } elseif ($this->option('force')) {
            $this->alert('You have set the <error>--force</error> option. This will delete the existing data from the new database before migration.');
            $this->truncateDatabase();
            $this->info('Truncation completed in ' . $this->getTimeIntervalString($startTime) . "\n");
            $startTime = Carbon::now();
        }

        $this->processMigration();
        DB::statement('SET sql_mode = \'STRICT_ALL_TABLES\'');
        DB::statement('SET FOREIGN_KEY_CHECKS = 1;');
        $this->info('Migration completed in ' . $this->getTimeIntervalString($startTime));
    }
    
    /**
     * Truncate new database before starting migration.
     * 
     * @return void
     */
    private function truncateDatabase(): void
    {
        $tables = array_map('reset', DB::select('SHOW TABLES'));
        $progressBar = $this->output->createProgressBar(count($tables));
        foreach ($tables as $table) {
            $startTime = Carbon::now();
            $this->line('<comment>Truncating:</comment> ' . $table);
            DB::statement('TRUNCATE ' . $table);
            $this->line('<info>Truncated:</info> ' . $table . ' in ' . $this->getTimeIntervalString($startTime));
            $progressBar->advance();
            $this->output->newLine(2);
        }
    }

    /**
     * Truncate new database but exclude users table before starting migration.
     *
     * @return void
     */
    private function truncateDatabaseWithoutUsers(): void
    {
        $tables = array_map('reset', DB::select('SHOW TABLES'));
        $progressBar = $this->output->createProgressBar(count($tables));
        DB::statement('DELETE FROM `users` WHERE `id` > 48');
        foreach ($tables as $table) {
            if($table != 'users'){
                $startTime = Carbon::now();
                $this->line('<comment>Truncating:</comment> ' . $table);
                DB::statement('TRUNCATE ' . $table);
                $this->line('<info>Truncated:</info> ' . $table . ' in ' . $this->getTimeIntervalString($startTime));
                $progressBar->advance();
                $this->output->newLine(2);
            }
        }
    }

    /**
     * Start the database migration process.
     * 
     * @return void
     */
    private function processMigration(): void
    {
        $this->comment('Migrating <info>' . $this->oldDatabaseName . '</info> database to <info>' . $this->newDatabaseName . '</info> database.');
        $progressBar = $this->output->createProgressBar(count($this->orderOfFiles));
        foreach ($this->orderOfFiles as $file) {
            $this->migrateFile($file);
            $progressBar->advance();
            $this->output->newLine(2);
        }
        
        $this->resetUsersPassword();
    }
    
    /**
     * Migrate the given file from the old database to new database.
     * 
     * @param string $file
     * 
     * @return void
     */
    private function migrateFile(string $file): void
    {
        $startTime = Carbon::now();
        $this->line('<comment>Migrating:</comment> ' . $file);
        $result = DB::insert($this->getQueryFromFile($file));
        if ($result) {
            $this->line('<info>Migrated:</info> ' . $file . ' in ' . $this->getTimeIntervalString($startTime));
        } else {
            $this->line('<error>Migration Failed:</error> ' . $file);
        }
    }
    
    /**
     * Return the query from the given migration file.
     * 
     * @param string $file
     * 
     * @return string
     */
    private function getQueryFromFile(string $file): string
    {
        return str_replace(
            '__NEW_DATABASE__',
            $this->newDatabaseName,
            str_replace('__OLD_DATABASE__', $this->oldDatabaseName, File::get(database_path('imports/' . $file)))
        );
    }
    
    /**
     * Return the interval time string.
     * 
     * @param Carbon $startTime
     * 
     * @return string
     */
    private function getTimeIntervalString(Carbon $startTime): string
    {
        $endTime = Carbon::now();
        $diffInSeconds = $endTime->diffInSeconds($startTime);
        $diffInMinutes = $endTime->diffInMinutes($startTime);
        $diffTime = $diffInMinutes;
        $diffTimeString = 'minute';
        if ($diffInSeconds < 60) {
            $diffTime = $diffInSeconds;
            $diffTimeString = 'second';
        }
        
        return '<info>' . $diffTime . '</info> ' . $diffTimeString . '(s).';
    }
    
    /**
     * Reset migrated users password.
     * 
     * @return void
     */
    private function resetUsersPassword(): void
    {
        $startTime = now();
        $this->comment("\nUpdating migrated users password.");
        $rowCount = 0;
        $chunkValue = 2000;
        $totalCount = DB::table('users')->where('password', '')->count();
        $progressBar = $this->output->createProgressBar($totalCount);
        DB::table('users')
                ->select('id', 'name')
                ->where('password', '')
                ->oldest('id')
                ->chunk($chunkValue, function (Collection $users) use (&$rowCount, $totalCount, $chunkValue, $progressBar) {
                    $this->updateUsersPassword($users, $rowCount);
                    $this->comment('Processed <info>' . $rowCount . '</info> / <info>' . $totalCount . '</info> records.');
                    $progressBar->advance($totalCount < $chunkValue ? $totalCount : $chunkValue);
                    $this->output->newLine(2);
                });
        $this->info('Updated users password in ' . $this->getTimeIntervalString($startTime) . "\n");
    }
    
    /**
     * Update password of the given users collection.
     * 
     * @param Collection $users
     * @param int $rowCount
     * 
     * @return void
     */
    private function updateUsersPassword(Collection $users, int &$rowCount): void
    {
        $newUsers = [];
        foreach ($users as $user) {
            $password = explode(' ', $user->name);
            $firstName = $password[0];
            $newUsers[] = [
                'id' => $user->id,
                'password' => bcrypt(ucfirst($firstName) . '!23456')
            ];
            $rowCount++;
        }

        $this->batchUpdate('users', $newUsers);
    }
    
    /**
     * Update multiple database records.
     * 
     * @param string $table
     * @param array $values
     * @param string $index
     * 
     * @return bool
     */
    private function batchUpdate(string $table, array $values, string $index = 'id'): bool
    {
        $ids = $final = [];
        foreach ($values as $val) {
            $ids[] = $val[$index];
            foreach (array_keys($val) as $field) {
                if ($field !== $index) {
                    $value = is_null($val[$field]) ? 'NULL' : '"' . $val[$field] . '"';
                    $final[$field][] = 'WHEN `' . $index . '` = "' . $val[$index] . '" THEN ' . $value . ' ';
                }
            }
        }

        $cases = '';
        foreach ($final as $k => $v) {
            $cases .= '`' . $k . '` = (CASE ' . implode("\n", $v) . "\n" . 'ELSE `' . $k . '` END), ';
        }

        $query = "UPDATE `$table` SET " . substr($cases, 0, -2) . " WHERE `$index` IN(" . implode(',', $ids) . ");";
        return DB::statement($query);
    }
}

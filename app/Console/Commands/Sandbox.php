<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Survey;
use App\Call;
use App\User;
use App\Lead;
use App\Interest;

class Sandbox extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sandbox';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Some testing functions';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $interest = Interest::find(123);
        dump($interest->canBeRenewed());
    }
}

<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use DB;

use App\Lead;

class CacheLeadProperties extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'leads:cache {id?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cache the "call counts" "callback times" for a lead';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if ($lead_id = $this->argument('id')) {
            $leads = Lead::where('id', $lead_id);
            $bar   = false;
        }
        else {
            $leads = new Lead;
            $count = $leads->count();
            $bar   = $this->output->createProgressBar($count);
        }

        $leads->chunk(100, function ($leads) use ($bar) {
            foreach ($leads as $lead) {

                $update = [];

                $next_callback = $lead->callback();
                if (is_null($next_callback)) {
                    $update['cached_callback_time'] = null;
                }
                else {
                    $update['cached_callback_time'] = $next_callback->callback_time;
                }

                $call_count = $lead->calls()->count();
                $update['cached_call_count'] = $call_count;

                $pcb_count = $lead->callbacks()->withTrashed()->where('is_priority', 1)->count();
                $update['cached_pcb_count'] = $pcb_count;

                $total_pitches = $lead->calls()->where('pitched', 1)->count();
                $update['cached_total_pitches'] = $total_pitches;

                $last_contacted = $lead->calls()->where('answered', 1)->orderby('created_at', 'desc')->first();
                if (!is_null($last_contacted)) {
                    $update['cached_last_contacted'] = $last_contacted->created_at;
                }

                /**
                 * We update this outside of Eloquent so that we don't clog up the Audit log with changes;
                 * essentially, these cached columns are a way to break out of Eloquent, so we shouldn't
                 * use Eloquent to update the record either.
                 */
                DB::table('leads')
                    ->where('id', $lead->id)
                    ->update($update);


                if ($bar) {
                    $bar->advance();
                }
            }
        });

        $this->line("\nLead properties cached");
    }
}

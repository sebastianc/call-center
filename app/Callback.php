<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Carbon\Carbon;

use \App\Events\CallbackSaved;
use \App\Events\CallbackDeleted;

class Callback extends Model
{
    use SoftDeletes;

    protected $fillable = ['user_id','lead_id','callback_time','is_priority','is_qualified'];

    protected $dates = ['deleted_at'];

    protected $dispatchesEvents = [
        'saved'   => CallbackSaved::class,
        'deleted' => CallbackDeleted::class
    ];

    public function lead()
    {
        return $this->belongsTo("App\Lead");
    }

    public function users()
    {
        return $this->belongsToMany("App\Users");
    }

    public function isOverdue() {
        return $this::timeIsOverdue($this->callback_time);
    }

    public static function timeIsOverdue($callback_time) {
        if (is_null($callback_time)) {
            return false;
        }
        $carbonDate = new Carbon($callback_time);
        if ($carbonDate->isPast()) {
            return true;
        } else {
            return false;
        }
    }

}

<?php

namespace App;

use Carbon\Carbon;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Interest extends Model implements Auditable
{

    use \OwenIt\Auditing\Auditable;

    protected $fillable = ['lead_id','catalogue_item_id','interest_percentage','status','sold','sale_value','outstanding_value'];
    public $timestamps = true;

    /**
     * How many days before the expiry_date of this interest
     * do we consider an interest to be "renewable"?
     */
    public static $days_until_renewal = 90;

    public function lead()
    {
        return $this->belongsTo("App\Lead");
    }

    public function payment()
    {
        return $this->hasOne("App\Payment", "id", 'payment_id');
    }

    public function catalogueItem()
    {
        return $this->hasOne("App\CatalogueItem", "id", "catalogue_item_id");
    }

    /**
     * Work out the value of this item if we want to pay for it, based on value and discount (if any)
     *
     * @param boolean $with_discount Are we applying the discount?
     *
     * @return void
     */
    public function calculateCost($with_discount = true, $discount_percentage = null, $plus_vat = false) {

        $discount_percentage = $discount_percentage ?? $this->discount_percentage;

        if ($with_discount) {
            $discount = $this->catalogueItem->price * ($discount_percentage / 100);
            $price_ex_vat = round($this->catalogueItem->price - $discount, 2);
        } else {
            $price_ex_vat =  $this->catalogueItem->price;
        }
        if ($plus_vat) {
            $price_plus_vat = $price_ex_vat * (1 + (env('VAT_PERCENTAGE', 20) / 100));
            return $price_plus_vat;
        } else {
            return $price_ex_vat;
        }
    }

    /**
     * Can this interest be renewed?
     * Is it due to expire in the next $days days, or has it already expired?
     *
     * @return boolean
     */
    public function canBeRenewed()
    {
        $diff_in_days = Carbon::now()->diffInDays($this->expiry_date, false);
        if ($diff_in_days <= $this->days_until_renewal) {
            return true;
        } else {
            return false;
        }
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LeadMember extends Model
{
    protected $fillable = ['lead_id','member_id'];

    // No model needed;
}

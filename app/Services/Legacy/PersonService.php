<?php
/**
 * Created by PhpStorm.
 * User: robertlemm
 * Date: 02/08/2018
 * Time: 10:44
 */

namespace App\Services\Legacy;


use App\Services\HttpService;

class PersonService extends HttpService
{
    public function __construct()
    {
        $this->config = ['base_uri' => config(app()->environment('production') ? 'services.legacy-api-live.create-person' : 'services.legacy-api-dev.create-person'), 'headers' =>[]];
        parent::__construct();
    }
}
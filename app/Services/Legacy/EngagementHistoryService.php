<?php
/**
 * Created by PhpStorm.
 * User: robertlemm
 * Date: 02/08/2018
 * Time: 11:32
 */

namespace App\Services\Legacy;


use App\Services\HttpService;

class EngagementHistoryService extends HttpService
{
    public function __construct()
    {

        $this->config = ['base_uri' => app()->environment('production') ? 'https://api.NewCo.networks.com' : 'http://dev-api.NewCo.networks.com','headers' =>[]];
        parent::__construct();

    }
}
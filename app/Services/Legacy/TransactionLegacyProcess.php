<?php
/**
 * Created by PhpStorm.
 * User: robertlemm
 * Date: 02/08/2018
 * Time: 12:22
 */

namespace App\Services\Legacy;


use App\Lead;
use Carbon\Carbon;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;
use Webpatser\Uuid\Uuid;

class TransactionLegacyProcess
{
    // This could be extra and not needed
    public function process(int $leadID, int $salesID, float $amount, array $orderData)
    {
        $lead = Lead::find($leadID);
        $memberID = $lead->member_id;
        //Insert the transaction first

        try {
            $transactionID = DB::connection("legacy-db")->table('transaction')->insertGetId([
                'person_id' => $memberID,
                'status' => 1,
                'payment_method' => 1,
                'is_direct' => 0,
                'staff_id' => $salesID
            ]);
        } catch (QueryException $queryException) {
            return \response("failure at transaction insert : " . $queryException->getMessage(), 400);
        }
        // Transaction Basket
        try {
            $transactionBasketID = DB::connection('legacy-db')->table('transaction_basket')->insertGetId([
                'uuid' => Uuid::generate()->string,
                'person_id' => $memberID,
                'price' => round($amount / 1.2),
                'currency' => 77, // GBP in legacy
                'sales_basket' => $salesID,
                'transaction_id' => $transactionID,
            ]);
        } catch (QueryException $queryException) {
            return \response("failure at basket insert : " . $queryException->getMessage(), 400);
        }
        // Transaction Items (loop)
        foreach ($orderData as $orderDatum) {
            try {

                $orderPrice = $orderDatum['item_price'];
                $orderItem = $orderDatum['item_id'];
                $orderType = $orderDatum['item_type'];

                if ($orderType == 1) //TODO make this shorter/cleaner after testing
                {// Package !!
                    // Handle length
                    $startDate = Carbon::now()->toDateTimeString();
                    $endDate = Carbon::now();
                    if ($orderItem === 1) {
                        $endDate->addMonth(1)->toDateTimeString(); //1 month ITN membership
                    } elseif ($orderItem === 2) {
                        $endDate->addMonth(12)->toDateTimeString();// 12 month ITN membership
                    } else {
                        $endDate->addMonth(36)->toDateTimeString();// 36 month ITN membership
                    }
                    $transactionItemID = DB::connection('legacy-db')->table('transaction_item')->insertGetId([
                        'basket_id' => $transactionBasketID,
                        'package_id' => $orderItem,
                        'start_date' => $startDate,
                        'end_date' => $endDate,
                        'price' => $orderPrice,
                        'renewal' => 0,
                    ]);
                } else { // Product !!
                    $startDate = Carbon::now()->toDateTimeString();
                    $transactionItemID = DB::connection('legacy-db')->table('transaction_item')->insertGetId([
                        'basket_id' => $transactionBasketID,
                        'product_permission_id' => $orderItem,
                        'start_date' => $startDate,
                        'price' => $orderPrice,
                        'renewal' => 0,
                    ]);
                }
            } catch (QueryException $queryException) {
                return response("failure item insertion : " . $queryException->getMessage(), 400);
            }
        }
    }
}
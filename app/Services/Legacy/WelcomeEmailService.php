<?php
/**
 * Created by PhpStorm.
 * User: robertlemm
 * Date: 10/08/2018
 * Time: 15:26
 */

namespace App\Services\Legacy;


use Illuminate\Support\Facades\DB;

class WelcomeEmailService
{
    public function process(string $email,$member_id, array $data, int $templateID = 5) : bool
    {
        // FORCE EMAIL DIRECT INSERT
        $emailInsertID = DB::connection("legacy-db")->table('communication_email_queue')
            ->insertGetId([
                'receiver_id' => $member_id,
                'email_address' => (env("APP_ENV") === "production") ? $email : "rob.lemm.1991@googlemail.com",
                'template_id' => $templateID,
                'email_data' => json_encode($data),
                'priority_id' => 0
            ]);

        return true;
    }


    public function sortData(array $memberData) : array
    {
        return [
            'first_name' => $memberData['first_name'],
            'last_name' => $memberData['last_name'],
            'subject' => 'Welcome To NewCo. Networks',
            'network' => 'NewCo. Networks',
            'email_address' => $memberData['email'],
            'password' => $memberData['password'],
        ];
    }
}
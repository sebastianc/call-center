<?php
/**
 * Created by PhpStorm.
 * User: robertlemm
 * Date: 03/08/2018
 * Time: 09:38
 */

namespace App\Services\Audit;


use App\CatalogueItem;
use App\Interest;
use App\User;
use stdClass;

class LeadAuditObject implements AuditableObject
{
    /**
     * @var mixed|null
     */
    private $id;
    /**
     * @var mixed|null
     */
    private $user_id;
    /**
     * @var mixed|null
     */
    private $event;
    /**
     * @var mixed|null
     */
    private $auditable_type;
    /**
     * @var mixed|null
     */
    private $auditable_id;
    /**
     * @var mixed|null
     */
    private $old_values;
    /**
     * @var mixed|null
     */
    private $new_values;
    /**
     * @var mixed|null
     */
    private $url;
    /**
     * @var mixed|null
     */
    private $ip_address;
    /**
     * @var mixed|null
     */
    private $user_agent;
    /**
     * @var mixed|null
     */
    private $tags;
    /**
     * @var mixed|null
     */
    private $created_at;
    /**
     * @var mixed|null
     */
    private $updated_at;

    /**
     * LeadAuditObject constructor.
     * @param array $auditArray
     */
    public function __construct(array $auditArray)
    {
        $this->id = $auditArray['id'] ?? null;
        $this->user_id = $auditArray['user_id'] ??  null;
        $this->event = $auditArray['event'] ??  null;
        $this->auditable_type = $auditArray['auditable_type'] ?? null;
        $this->auditable_id = $auditArray['auditable_id'] ?? null;
        $this->old_values = $auditArray['old_values'] ?? null;
        $this->new_values = $auditArray['new_values'] ?? null;
        $this->url = $auditArray['url'] ?? null;
        $this->ip_address = $auditArray['ip_address'] ?? null;
        $this->user_agent = $auditArray['user_agent'] ?? null;
        $this->tags = $auditArray['tags'] ?? null;
        $this->created_at = $auditArray['created_at'] ?? null;
        $this->updated_at = $auditArray['updated_at'] ?? null;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return User::find($this->user_id);
    }

    public function getInterest(): string
    {
        return Interest::find($this->auditable_id)->catalogueItem->title;
    }

    /**
     * @return string
     */
    public function getEvent(): string
    {
        return $this->event;
    }

    /**
     * @return string
     */
    public function getAuditType(): string
    {
        return str_replace("App\\", "", $this->auditable_type);
    }

    /**
     * @return int
     */
    public function getAuditClassId(): int
    {
        return $this->auditable_id;
    }

    /**
     * @return stdClass
     */
    public function getOldValues(): stdClass
    {
        return json_decode($this->old_values);
    }

    /**
     * @return stdClass
     */
    public function getNewValues(): stdClass
    {
        return json_decode($this->new_values);
    }

    /**
     * @return string
     */
    public function getUrlCalled(): string
    {
        return $this->url;
    }

    /**
     * @return string
     */
    public function getIpAddress(): string
    {
        return $this->ip_address;
    }

    /**
     * @return string
     */
    public function getUserAgrent(): string
    {
        return $this->user_agent;
    }

    /**
     * @return string
     */
    public function getTags(): string
    {
        return $this->tags;
    }

    /**
     * @return string
     */
    public function getCreatedAt(): string
    {
        return $this->created_at;
    }

    /**
     * @return string
     */
    public function getUpdatedAt(): string
    {
        return $this->updated_at;
    }

    /**
     * @return array
     */
    public function getMetaData(): array
    {
        return [
            'audit_id' => $this->id,
            'audit_event' => $this->event,
            'audit_url' => $this->url,
            'audit_ip_address' => $this->ip_address,
            'audit_user_agent' => $this->user_agent,
            'audit_tags' => $this->tags,
            'audit_created_at' => $this->created_at,
            'audit_updated' => $this->updated_at,
            'user_id' => $this->user_id,
            'audit_type' => $this->auditable_type,
        ];
    }

    /**
     * @return array
     */
    public function getModified(): array
    {
        $data = [];
        foreach (json_decode($this->old_values) as $key => $value)
        {
            $data[$key]['old'] = $value;
        }

        foreach (json_decode($this->new_values) as $key => $value)
        {
            $data[$key]['new'] = $value;
        }

        return $data;
    }
}
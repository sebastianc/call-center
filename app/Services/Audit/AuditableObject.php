<?php
/**
 * Created by PhpStorm.
 * User: robertlemm
 * Date: 03/08/2018
 * Time: 09:18
 */

namespace App\Services\Audit;


use App\User;
use stdClass;

interface AuditableObject
{
    public function getId() : int ;
    public function getUser() : User;
    public function getEvent() : string;
    public function getAuditType(): string;
    public function getAuditClassId(): int;
    public function getOldValues(): stdClass;
    public function getNewValues(): stdClass;
    public function getUrlCalled(): string;
    public function getIpAddress(): string;
    public function getUserAgrent(): string;
    public function getTags(): string; // Not used currently
    public function getCreatedAt(): string;
    public function getUpdatedAt(): string;


    public function getMetaData():array;
    public function getModified():array;
}
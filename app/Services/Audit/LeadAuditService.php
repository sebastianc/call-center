<?php
/**
 * Created by PhpStorm.
 * User: robertlemm
 * Date: 03/08/2018
 * Time: 08:59
 */

namespace App\Services\Audit;


use App\Lead;
use Illuminate\Support\Facades\DB;

class LeadAuditService
{

    /**
     * @var array
     */
    private $auditDataArray;

    private $auditSortedArray = [];

    /**
     * @var Lead
     */
    private $lead;

    /**
     * LeadAuditService constructor.
     * @param int $leadID
     */
    public function __construct(int $leadID)
    {
        $this->lead = Lead::find($leadID);
        $this->setAuditData();
        $this->sortAuditData();
    }


    /**
     * @param mixed $auditData
     */
    private function setAuditData(): void
    {
        $this->auditDataArray = DB::table("audits")->where(function ($query) {
            $query->where('auditable_type', '=', 'App\Lead')
                ->where('auditable_id', '=', $this->lead->id);
        })->orWhere(function ($query2) {
            $query2->where('auditable_type', '=', 'App\Interest')
                ->whereIn('auditable_id', $this->lead->interests()->pluck('interests.id'));
        })->where('event','!=','created')->orderBy('created_at', 'desc')->get();
    }

    /**
     * Change raw data array to object LeadAuditObject
     */
    private function sortAuditData(): void
    {
        foreach ($this->auditDataArray as $auditData)
        {
            $this->auditSortedArray[] = new LeadAuditObject((array)$auditData);
        }
    }

    public function getAuditData()
    {
        return $this->auditSortedArray;
    }


}
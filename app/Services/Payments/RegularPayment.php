<?php
/**
 * Created by PhpStorm.
 * User: robertlemm
 * Date: 09/08/2018
 * Time: 08:58
 */

namespace App\Services\Payments;


use App\Services\HttpService;

class RegularPayment extends HttpService
{
    public function __construct()
    {
        $this->config = [
            'base_uri' => (env("APP_ENV") === "production") ? config('services.payment-service.production') : config('services.payment-service.development'),
            'headers' => ['Authorization' => 'Bearer '.env("PAYMENT_TOKEN"),
            'Accept' => 'application/json' ,
            'Content-Type' => 'application/json'
            ]
        ];
        parent::__construct();
    }

}
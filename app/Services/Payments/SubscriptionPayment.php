<?php
/**
 * Created by PhpStorm.
 * User: robertlemm
 * Date: 09/08/2018
 * Time: 11:01
 */

namespace App\Services\Payments;


use App\Services\HttpService;

class SubscriptionPayment extends HttpService
{
    public function __construct()
    {
        $this->config = ['base_uri' => app()->environment('production') ? config('services.payment-subscription.production') : config('services.payment-subscription.development'),'headers' => ['Authorization' => 'Bearer '.env("PAYMENT_TOKEN"),'Accept' => 'application/json' , 'Content-Type' => 'application/json']];
        parent::__construct();
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: robertlemm
 * Date: 10/08/2018
 * Time: 10:34
 */

namespace App\Services\Payments;


use App\Services\HttpService;

class StripePlans extends HttpService
{
    public function __construct()
    {
        $this->config = ['base_uri' => app()->environment('production') ? config('services.payment-plans.production') : config('services.payment-plans.development'),'headers' => ['Authorization' => 'Bearer '.env("PAYMENT_TOKEN"),'Accept' => 'application/json' , 'Content-Type' => 'application/json']];
        parent::__construct();
    }
}
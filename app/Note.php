<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Note extends Model
{
    protected $fillable = ['text','note_type_id','user_id','lead_id','created_at', 'interest_id'];


    public function user()
    {
        return $this->belongsTo("App\User");
    }

    public function lead()
    {
        return$this->belongsTo("App\Lead");
    }
}

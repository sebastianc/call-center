<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\UserCreated' => [
            'App\Listeners\UserCreatedListener',
        ],
        'App\Events\CallSaved' => [
            'App\Listeners\CallSavedListener',
        ],
        'App\Events\CallbackSaved' => [
            'App\Listeners\CallbackSavedListener',
        ],
        'App\Events\CallbackDeleted' => [
            'App\Listeners\CallbackDeletedListener',
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}

<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Validator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        /**
         * Add a custom rule to require *either* of two form fields to be submitted (not both)
         * Based loosely on: https://laraveldaily.com/laravel-custom-validation-one-of-the-fields-required-but-not-both/
         */
        Validator::extend('empty_with', function ($attribute, $value, $parameters, $validator) {
            $data = $validator->getData();
            $other_field = $data[$parameters[0]];
            return (!empty($value) && !empty($other_field)) ? false : true;
        });

        /**
         * Force URLs to be https on production (for load balancer)
         */
        if (env('APP_ENV') === 'production' || env('APP_ENV') === 'development') {
            $this->app['request']->server->set('HTTPS', true);
        }
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}

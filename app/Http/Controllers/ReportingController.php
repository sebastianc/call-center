<?php

namespace App\Http\Controllers;

use App\Exports\LeadCallbacks;
use App\Exports\LeadCallStats;
use App\Exports\LeadExport;
use App\Exports\LeadStatus;
use App\Exports\LeadSource;
use App\Exports\TeamSales;
use App\Exports\UserSales;
use Doctrine\DBAL\DBALException;


class ReportingController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth']);
    }

    public function reportList()
    {
        // Internal reporting
        return view("crm.themes.basic.screens.reports");
    }

    public function foiRequest($id)
    {
        return (new LeadExport($id))->download('FOI-request-' . $id . '.xlsx');
    }

    public function leadStatusRequest($startPoint, $endPoint)
    {
        return (new LeadStatus($startPoint, $endPoint))->download('lead-status.xlsx');
    }

    public function leadCallsRequest($startPoint, $endPoint)
    {
        return (new LeadCallStats($startPoint, $endPoint))->download('lead-call-stats.xlsx');
    }

    public function leadCallsBacksRequest()
    {
        return (new LeadCallbacks())->download('lead-call-backs.xlsx');
    }

    public function leadSourceRequest($length = 6)
    {
        return (new LeadSource($length))->download('lead-sources.xlsx');
    }

    public function userSalesRequest($id)
    {
    }

    public function teamSalesRequest($teamID)
    {
        return (new TeamSales($teamID))->download('team-'.$teamID.'-sales-reports.xlsx');
    }

    public function test()
    {

    }
}

<?php

namespace App\Http\Controllers;

use App\Lead;
use App\LeadMember;
use App\Services\Legacy\EngagementHistoryService;
use App\Services\Legacy\PersonService;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use \Webpatser\Uuid\Uuid;

class ExternalController extends Controller
{

    private $legacyApiKey;

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @param string $url
     * @param array $requestFields
     * @return mixed
     *
     * Make Postable curl request and return response;
     */
    private function makePostRequest(string $url, array $requestFields)
    {
        // urlencode all values in array to ensure proper curling occurs
        foreach ($requestFields as $key => $requestField) {
            $requestFields[$key] = urlencode($requestFields[$key]);
        }
        //url-ify the data for a POST request
        $fields_string = '';
        foreach ($requestFields as $key => $value) {
            $fields_string .= $key . '=' . $value . '&';
        }
        rtrim($fields_string, '&');

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_POST, count($requestFields));
        curl_setopt($curl, CURLOPT_POSTFIELDS, $fields_string);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $response = curl_exec($curl);
        curl_close($curl);

        return $response;

    }

    /**
     * @param string $url
     * @return mixed
     *
     * Run a curl get request
     */
    private function makeGetRequest(string $url)
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec($curl);
        curl_close($curl);

        return $response;
    }



    /**
     * @param $leadID
     * @return \Illuminate\Contracts\Routing\ResponseFactory|mixed|\Symfony\Component\HttpFoundation\Response
     *
     * TODO ask for non authenticated route as its currently locked off
     */
    public function getEngagementHistory($leadID)
    {
        $memberRecord = Lead::find($leadID);
        $memberID = $memberRecord->member_id;
        $response = (new EngagementHistoryService())->get('/people/'.$memberID.'/engagement');
        return $response;
    }

    /*
   |--------------------------------------------------------------------------
   | Old transactions
   |--------------------------------------------------------------------------
   |
   | Whilst 1.5 is being supported these routes will be needed to create transactions inside the database
   */

    public function processLegacyTransactions(int $leadID, int $salesID, float $amount, array $orderData)
    {
        $lead = Lead::find($leadID);
        $memberID = $lead->member_id;
        //Insert the transaction first

        try {
            $transactionID = DB::connection("legacy-db")->table('transaction')->insertGetId([
                'person_id' => $memberID,
                'status' => 1,
                'payment_method' => 1,
                'is_direct' => 0,
                'staff_id' => $salesID
            ]);
        } catch (QueryException $queryException) {
            return \response("failure at transaction insert : " . $queryException->getMessage(), 400);
        }
        // Transaction Basket
        try {
            $transactionBasketID = DB::connection('legacy-db')->table('transaction_basket')->insertGetId([
                'uuid' => Uuid::generate()->string,
                'person_id' => $memberID,
                'price' => round($amount / 1.2),
                'currency' => 77, // GBP in legacy
                'sales_basket' => $salesID,
                'transaction_id' => $transactionID,
            ]);
        } catch (QueryException $queryException) {
            return \response("failure at basket insert : " . $queryException->getMessage(), 400);
        }
        // Transaction Items (loop)
        foreach ($orderData as $orderDatum) {
            try {

                $orderPrice = $orderDatum['item_price'];
                $orderItem = $orderDatum['item_id'];
                $orderType = $orderDatum['item_type'];

                if ($orderType == 1) //TODO make this shorter/cleaner after testing
                {// Package !!
                    // Handle length
                    $startDate = Carbon::now()->toDateTimeString();
                    $endDate = Carbon::now();
                    if ($orderItem === 1) {
                        $endDate->addMonth(1)->toDateTimeString(); //1 month ITN membership
                    } elseif ($orderItem === 2) {
                        $endDate->addMonth(12)->toDateTimeString();// 12 month ITN membership
                    } else {
                        $endDate->addMonth(36)->toDateTimeString();// 36 month ITN membership
                    }
                    $transactionItemID = DB::connection('legacy-db')->table('transaction_item')->insertGetId([
                        'basket_id' => $transactionBasketID,
                        'package_id' => $orderItem,
                        'start_date' => $startDate,
                        'end_date' => $endDate,
                        'price' => $orderPrice,
                        'renewal' => 0,
                    ]);
                } else { // Product !!
                    $startDate = Carbon::now()->toDateTimeString();
                    $transactionItemID = DB::connection('legacy-db')->table('transaction_item')->insertGetId([
                        'basket_id' => $transactionBasketID,
                        'product_permission_id' => $orderItem,
                        'start_date' => $startDate,
                        'price' => $orderPrice,
                        'renewal' => 0,
                    ]);
                }
            } catch (QueryException $queryException) {
                return response("failure item insertion : " . $queryException->getMessage(), 400);
            }
        }

        return \response('success', 200);
    }
}

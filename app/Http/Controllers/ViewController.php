<?php

namespace App\Http\Controllers;

use App\Lead;
use Illuminate\Http\Request;

class ViewController extends Controller
{
    // Route all view loading through?

    public function __construct()
    {
        // Todo Wrap in auth:middleware
    }

    public function AuditLead(Request $request)
    {
        $lead = Lead::find($request->leadID);
        $audit = $lead->audits;
        return view("auditing.lead_auditing",['lead'=>$lead,'audits'=>$audit]);
    }
}

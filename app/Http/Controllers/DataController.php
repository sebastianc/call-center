<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use DB;
use Log;
use Carbon\Carbon;

use App\Call;
use App\Callback;
use App\CatalogueItem;
use App\Interest;
use App\Lead;
use App\LeadStatus;
use App\Note;
use App\SMS;
use App\Team;
use App\User;

use Cookie;

use Yajra\Datatables\Datatables;

class DataController extends Controller
{

    /**
     * Require the user to be logged in for everything in this controller
     */
    public function __construct()
    {
        $this->middleware('auth');

        $this->view_folder = 'crm.themes.' . config('crm.theme');
    }

    /**
     * Return the records to display in a datatable, for the current lst
     * (and for "Interests", the parent to which they belong, ie the current lead)
     *
     * @param string $list
     * @param integer $parent_id
     * @return void
     */
    public function getData($list, $parent_id = null)
    {

        switch ($list) {
            case 'leads':
                return $this->getDataForLeads();
                break;
            case 'open-pot':
                return $this->getDataForPot('open');
                break;
            case 'renewals-pot':
                return $this->getDataForPot('renewals');
                break;
            case 'engagement-pot':
                return $this->getDataForPot('engagement');
                break;
            case 'users':
                return $this->getDataForUsers();
                break;
            case 'teams':
                return $this->getDataForTeams();
                break;
            case 'interests':
                return $this->getDataForInterests($parent_id);
                break;
            case 'test-users':
                return $this->getDataForTesting('user');
                break;
            case 'test-leads':
                return $this->getDataForTesting('lead');
                break;
            default:
                trigger_error("Unhandled list $list");
        }

    }

    /*
     * Data for leads view of a user
     */
    protected function getDataForLeads()
    {

        $query = Lead::with('user')->select('leads.*');
        $this->filterLeadsByUser($query);

        return Datatables::of($query)
            /**
             * Editor requires a unique row indentifier
             */
            ->setRowId('id')
            /**
             * Set some row classes depending on the lead
             */
            ->setRowClass(function ($lead) {
                if (!is_null($lead->cached_callback_time) && Callback::timeIsOverdue($lead->cached_callback_time)) {
                    return 'table-danger';
                }

                if($lead->hasOutstandingPayments() == true)
                {
                    return 'table-warning';
                }
            })
            /**
             * Concat the first and last names to give us a full name
             */
            ->addColumn('full_name', function ($lead) {
                $full_name = implode(' ', [$lead->first_name, $lead->last_name]);
                if ($badge = $this->getBadgeForLead($lead)) {
                    $full_name .= ' '. $badge;
                }
                return $full_name;
            })

            /**
             * Add the callback date, formatted using Carbon
             */
            ->editColumn('cached_callback_time', function ($lead) {
                return !empty($lead->cached_callback_time) ? with(new Carbon($lead->cached_callback_time))->format('d/m/Y H:i') : 'No';
            })
            ->filterColumn('cached_callback_time', function ($query, $keyword) {
                $query->whereRaw("DATE_FORMAT(cached_callback_time,'%d/%m/%Y %H:%i') like ?", ["%$keyword%"]);
            })
            /**
             * Handle leads that aren't assigned to anyone
             */
            ->editColumn('user.name', function ($lead) {
                return $lead->user ? $lead->user->name : 'Not allocated';
            })
            ->filterColumn('user.id', function ($query, $id) {
                $query = $this->filterEmptyRelationship($query, 'user_id', $id);
            })
            /**
             * Add an "Edit" URL
             */
            ->addColumn('edit_link', function ($lead) {
                return route('crm.edit', ['class' => 'lead', 'id' => $lead->id]);
            })
            ->make(true);
    }

    /**
     * Return data to Datatables via Ajax in JSON format
     *
     * @param string $pot_name The name of the pot (eg open, engagement, renewals)
     * @return void
     */
    protected function getDataForPot($pot_name)
    {

        switch ($pot_name) {
            case 'engagement':
                $query = Lead::filterForEngagementPot();
                break;
            case 'renewals':
                $query = Lead::filterForRenewalsPot();
                break;
            case 'open':
                $query = Lead::filterForOpenPot();
                break;
            default:
                \trigger_error("Unhandled pot name $pot_name");
        }

        return Datatables::of($query)
            /**
             * Editor requires a unique row indentifier
             */
            ->setRowId('id')
            /**
             * Concat the first and last names to give us a full name
             */
            ->addColumn('full_name', function ($lead) {
                return implode(' ', [$lead->first_name, $lead->last_name]);
            })
            /**
             * Add the callback date, formatted using Carbon
             */
            ->editColumn('cached_last_contacted', function ($lead) {
                return !empty($lead->cached_last_contacted) ? with(new Carbon($lead->cached_last_contacted))->format('d/m/Y H:i') : 'Never';
            })
            ->filterColumn('cached_last_contacted', function ($query, $keyword) {
                $query->whereRaw("DATE_FORMAT(cached_last_contacted,'%d/%m/%Y %H:%i') like ?", ["%$keyword%"]);
            })
            /**
             * Handle leads that aren't assigned to anyone, and leads with no status
             */
            ->editColumn('user.name', function ($lead) {
                return $lead->user ? $lead->user->name : 'Not allocated';
            })
            ->editColumn('leadstatus.title', function ($lead) {
                return $lead->leadstatus ? $lead->leadstatus->title : 'No status set';
            })
             /**
             * Allow us to filter for leads with no owners, and leads with no status
             */
            ->filterColumn('user.id', function ($query, $id) {
                $query = $this->filterEmptyRelationship($query, 'user_id', $id);
            })
            ->filterColumn('leadstatus.id', function ($query, $id) {
                $query = $this->filterEmptyRelationship($query, 'lead_status_id', $id);
            })
            /**
             * Add an "Edit" URL
             */
            ->addColumn('edit_link', function ($lead) {
                return route('crm.edit', ['class' => 'lead', 'id' => $lead->id]);
            })
            ->make(true);
    }

    protected function getDataForUsers()
    {
        $query = User::with(['teams', 'roles'])->select('*');

        $this->filterUsersByUser($query);

        return DataTables::of($query)
            /**
             * Editor requires a unique row indentifier
             */
            ->setRowId('id')
            /**
             * Allow us to filter the teams/roles columns using the dropdown in the table header;
             * this is more complicated than usual because teams and role are hasMany relationships
             */
            ->filterColumn('teams.name', function ($query, $team_id) {
                /**
                 * Datatables posts this as a regex, which we need to convert to an integer ID:
                 */
                $team_id = intval(trim($team_id, '^$'));
                if ($team_id) {
                    $usersInTeam = DB::table('team_user')->where('team_id', $team_id)->pluck('user_id');
                    $query = $this->filterByMultipleRelationship($query, 'users', 'teams', $usersInTeam);
                }
            })
            ->filterColumn('roles.title', function ($query, $role_id) {
                $role_id = intval(trim($role_id, '^$'));
                if ($role_id) {
                    $usersWithRole = DB::table('role_user')->where('role_id', $role_id)->pluck('user_id');
                    $query = $this->filterByMultipleRelationship($query, 'users', 'roles', $usersWithRole);
                }
            })
            /**
             * Add an "Edit" URL
             */
            ->addColumn('edit_link', function ($user) {
                return route('crm.edit', ['class' => 'user', 'id' => $user->id]);
            })
            ->make(true);
    }

    protected function getDataForTeams()
    {

        $query = Team::select('*');

        return DataTables::of($query)
            /**
             * Editor requires a unique row indentifier
             */
            ->setRowId('id')
            /**
             * Add an "Edit" URL
             */
            ->addColumn('edit_link', function ($team) {
                return route('crm.edit', ['class' => 'team', 'id' => $team->id]);
            })
            ->make(true);
    }

    protected function getDataForInterests($lead_id)
    {

        $query = Interest::where('lead_id', $lead_id)->with('catalogueItem')->select('*');

        return DataTables::of($query)
            /**
             * Editor requires a unique row indentifier
             */
            ->setRowId('id')

            ->setRowClass(function ($interest) {
                if($interest->outstanding_value > 0 )
                {
                    return 'table-warning';
                }
            })
            // ->editColumn('interest_percentage', '{{$interest_percentage}}%')
            ->editColumn('created_at', function (Interest $interest) {
                return Carbon::parse($interest->created_at)->format('d/m/Y');
            })
            ->editColumn('catalogue_item.price', function (Interest $interest) {
                return '£' . number_format($interest->catalogueItem->price, 2);
            })
            ->addColumn('sale_options', function (Interest $interest) {

                /**
                 * ITBOV-417 This is where we check whether an interest can be renewed...
                 */
                $contents = [];
                if ($interest->sold) {
                    $contents[] = '<strong>&pound;' . number_format($interest->sale_value, 2) . ' (incl. VAT)</strong> on ' . Carbon::parse($interest->sold_on)->format('d/m/Y');
                }
                if (!$interest->sold) {

                    /**
                     * The maximum discount available on an item is set in the interests table,
                     * _unless_ we've entered the manager override password for increasing discounts.
                     * which currently removes the upper limit altogether.
                     */
                    $cookie_key = $this->getDiscountCookieKey($interest->lead);
                    if (Cookie::get($cookie_key)) {
                        $max_discount = 100;
                    }
                    else {
                        $max_discount = $interest->catalogueItem->max_discount;
                    }

                    $contents[] = view('crm.themes.' . config('crm.theme').'.partials.interest-table-add-options', [
                        'interest'     => $interest,
                        'max_discount' => $max_discount
                    ]);
                }
                return \implode($contents, "\n");

            })
            ->editColumn('discount_percentage', function (Interest $interest) {
                return (is_null($interest->discount_percentage)? 0 : $interest->discount_percentage) . '%';
            })
            ->make(true);
    }

    protected function getDataForTesting($list)
    {

        switch ($list) {
            case 'user':
                $query = User::with(['teams', 'roles'])->select('users.*');

                return DataTables::of($query)
                    ->setRowId('id')
                    ->filterColumn('teams.name', function ($query, $team_id) {
                        /**
                         * Datatables posts this as a regex, which we need to convert to an integer ID:
                         */
                        $team_id = intval(trim($team_id, '^$'));
                        $usersInTeam = DB::table('team_user')->where('team_id', $team_id)->pluck('user_id');
                        $query->whereIn('users.id', $usersInTeam);
                    })
                    ->filterColumn('roles.title', function ($query, $role_id) {
                        $role_id = intval(trim($role_id, '^$'));
                        $usersWithRole = DB::table('role_user')->where('role_id', $role_id)->pluck('user_id');
                        $query->whereIn('users.id', $usersWithRole);
                    })
                    ->addColumn('edit_link', function ($user) {
                        return route('crm.edit', ['class' => 'user', 'id' => $user->id]);
                    })
                    ->make(true);
                break;
            case 'lead':
                $query = Lead::with('user')->select('leads.*');

                return Datatables::of($query)
                    ->setRowId('id')
                    /*
                    ->filterColumn('user.name', function($query, $user_id) {
                        $query->where('leads.user_id', $user_id);
                    })
                    */

                    ->addColumn('edit_link', function ($lead) {
                        return route('crm.edit', ['class' => 'lead', 'id' => $lead->id]);
                    })
                    ->make(true);

                break;
        }
    }
     /**
     * Handle the post from DataTables editor, to update multiple records at once
     * or to edit fields inline
     * @param  Request $request
     * @param string $list A valid class (leads, users, interests etc)
     * @return
     */
    public function processEditorUpdate(Request $request, $list)
    {
        $this->validate($request, [
            'action' => 'required',
            'data' => 'required',
        ]);

        $data = $request->input('data');
        $class = $this->getClassForList($list);

        switch ($class) {
            case 'lead':
                $this->processEditorUpdateForLeads($data);
                break;
            case 'user':
                $this->processEditorUpdateForUsers($data);
                break;
            case 'interest':
                $this->processEditorUpdateForInterests($data);
                break;
            default:
                trigger_error("Unhandled class $class");
        }

        /**
         * Return the updated data here, to refresh the table:
         */

        return $this->getData($list);

    }

    protected function processEditorUpdateForLeads($data)
    {

        foreach ($data as $id => $update) {

            /**
             * Format the date correctly
             */
            if (!empty($update['callback'])) {
                /**
                 * If we've not set a new value, $update['callback'] here may be
                 * a "No" value, or a value in the "friendly" d/m/y format. Check first:
                 */
                $postedDateFormat = 'd/m/Y H:i';
                $databaseDateFormat = 'Y-m-d H:i:s';
                $date = Carbon::createFromFormat($postedDateFormat, $update['callback']);
                if ($date !== false) {
                    $update['callback'] = $date->format($databaseDateFormat);
                } else {
                    unset($update['callback']);
                }
            }
            /**
             * Update the record, using Eloquent:
             */
            Lead::where('id', $id)->update($update);
        }
    }

    protected function processEditorUpdateForUsers($data)
    {

        foreach ($data as $id => $update) {

            $user = User::where('id', $id)->first();

            /**
             * Handle posted Teams
             */
            if (!empty($update['teams'])) {
                $team_ids = [];
                foreach ($update['teams'] as $u) {
                    $team_ids[] = $u['id'];
                }
                $user->teams()->sync($team_ids);
            }

            /**
             * Handle posted Roles
             */
            if (!empty($update['roles'])) {
                $role_ids = [];
                foreach ($update['roles'] as $u) {
                    $role_ids[] = $u['id'];
                }
                $user->roles()->sync($role_ids);
            }
        }
    }

    /**
     * Handle the posted data from the "Add Interest" modal
     *
     * @return void
     */
    protected function processEditorUpdateForInterests($data)
    {
        foreach ($data as $id => $update) {
            if ($id) { /* If we're editing an existing interest */
                /**
                 * A special case; if we choose 'remove' from the dropdown, remove this interest
                 * Note that we shouldn't remove sold interests, as these are a useful record
                 */

                if (isset($update['interest_percentage']) && $update['interest_percentage'] == 'remove') {
                    $interest = Interest::where('id', $id)->first();
                    if (!$interest->sold) {
                        $interest->delete();
                    }
                }
                else {
                    Interest::where('id', $id)->update($update);
                }
            }
            else {
                /* Create a new one */

                /**
                 * Check for catalogue_item_id because we restrict the dropdown options
                 * and sometimes only have one null value labelled "All interests already registered"
                 */

                $item = CatalogueItem::where('id', $update['catalogue_item_id'])->first();

                if (!empty($update['catalogue_item_id'])) {
                    Interest::firstOrCreate([
                        'catalogue_item_id' => $update['catalogue_item_id'],
                        'lead_id'           => $update['lead_id'],
                        'sale_value'        => $item->price*1.2,
                        'outstanding_value' => 0.00
                    ], [
                        'interest_percentage' => $update['interest_percentage']
                    ]);
                }
            }
        }
    }

    /**
     * Filter a Leads query to only show those belonging to the current user, if necessary
     *
     * @param Eloquent $query
     * @return void
     */
    protected function filterLeadsByUser(&$query)
    {

        $user = Auth::user();

        /**
         * Basic permissions here, these need strengthening
         */
        if ($user->isTeamLeader()) {
            /**
             * Team Leaders and Account Managers can see all leads for their team(s)
             */
            $team_ids = $user->teams()->pluck('teams.id');
            $query->whereHas('user.teams', function ($q) use ($team_ids) {
                $q->whereIn('teams.id', $team_ids);
            });
        } else {
            /**
             * Otherwise we just show the user "their" leads
             */
            $query->where('user_id', $user->id);
            /**
             * If a user has overdue callbacks -- that is, leads that they haven't called in time --
             * then we prevent them from seeing any "new" leads (leads that don't have a callback set)
             * We also need to flag this to the user; is a global ->warning array the best approach?
             */
            if ($user->hasOverdueCallbacks()) {
                // $query->whereNotNull('callback');
                $query->whereNotNull('cached_callback_time');
            }

        }
    }

    /**
     * Filter a Users query to only show those that should be visible to the current user
     *
     * @param Eloquent $query
     * @return void
     */
    protected function filterUsersByUser(&$query)
    {
        $user = Auth::user();

        // See filterLeadsByUser above
        /**
         * Basically we want to filter users so that:
         * - Team Leaders can see everyone
         * - Account Managers can only see users on their team
         * - No-one else can see anyone but themselves
         */
        if ($user->isTeamLeader()) {
            // No filter required
        } elseif ($user->isAccountManager()) {
            $team_ids = $user->teams()->pluck('teams.id');
            $query->whereHas('teams', function ($q) use ($team_ids) {
                $q->whereIn('teams.id', $team_ids);
            });
        } else {
            $query->where('user_id', $user->id);
        }
    }

    /**
     * Filter a datatables query to handle an empty mutiple relationship ID (eg, show users with no teams)
     *
     * @param object $query The current query
     * @param string $table The table belonging to the class we're filtering (eg, users)
     * @param string $property The property we're filtering on
     * @param integer $value The requested filter values (eg, teams 2 and 3, or null/0 for "none")
     * @return object $query The updated query
     */
    public function filterByMultipleRelationship($query, $table, $property, $values) {
        if ($values->count() > 0) {
            $query->whereIn($table.'.id', $values);
        }
        else {
            $query->doesntHave($property);
        }
        return $query;
    }

        /**
     * Filter a datatables query to handle an empty relationship ID (eg, show leads with no users)
     *
     * @param object $query The current query
     * @param string $properuy The property we're filtering on
     * @param integer $value The requested filter value (eg, user 2, or null/0 for "none")
     * @return object $query The updated query
     */
    public function filterEmptyRelationship($query, $property, $value) {
        /**
         * Datatables posts this as a regex, which we need to convert to an integer ID:
         */

         /**
          * Because of the way DataTables works, this is being triggered for text searches too.
          * We only want to run this for column filters; we know if it's a column filter
          * because the string is wrapped in ^$. So, if we don't have those delimeters, do nothing:
          */
        $filtered_value = intval(trim($value, '^$'));

        if ($filtered_value == $value) return $query;

        if (!empty($filtered_value)) {
            $query->where($property, $filtered_value);
        }
        else if ($filtered_value === 0) {
            /**
             * Handle null and zero values
             */
            $query->whereRaw("COALESCE(".$property.", 0) = 0");
        }
        else {
            /**
             * An empty string value for $user_id should return all leads, so don't filter here
             */
        }
        return $query;
    }

    /**
     * Handle the posted "Interest" form from within the Interests table (ie, inline editing)
     * @param  Request $request
     * @return
     */
    public function processInterestUpdate(Request $request)
    {

        $this->validate($request, [
            'action' => 'required',
            'interest_id' => 'required',
        ]);

        $interest = Interest::find($request->input('interest_id'));
        $action = $request->input('action');
        $discount = $request->input('discount');

        switch ($action) {
            case 'add':
            case 'refresh':
                $interest->discount_percentage = ($discount === NULL )? 0 : $discount;
                $interest->in_basket = 1;
                $interest->save();
                break;
            case 'remove':
                $interest->discount_percentage = 0;
                $interest->in_basket = 0;
                $interest->save();
                break;
            default:
                \trigger_error("Unhandled action $action");
        }

    }

    /**
     * Return the class name that we're listing when we're viewing a certain $list
     *
     * @param $list list name
     * @return void
     */
    protected function getClassForList($list)
    {
        switch ($list) {
            case 'leads':
            case 'open-pot':
            case 'test-leads':
                return 'lead';
                break;
            case 'users':
            case 'test-users':
                return 'user';
                break;
            case 'interests':
                return 'interest';
                break;
            default:
                trigger_error("Unhandled list name $list");
                break;
        }
    }

    /**
     * Handle the post from the "Increase Discounts" modal (i.e., where the password override is entered)
     * This sets a session for the current user that allows increased discounts for this lead, for ten minutes
     *
     * @return void
     */
    public function increaseDiscounts(Request $request, $lead_id) {
        // Set a session for ten minutes

        $this->validate($request, [
            'password' => [
                'required',
                function ($attribute, $value, $fail) {
                    $password = DB::table('configs')->value('manager_override_password');
                    if (is_null($password)) {
                        return $fail("System error: no password set in configs table");
                    }
                    if ($value != $password) {
                        return $fail($attribute.' is incorrect');
                    }
                },
            ]
        ]);

        $lead = Lead::find($lead_id);

        $cookie_key     = $this->getDiscountCookieKey($lead);
        $cookie_timeout = 10; // In minutes

        if ($request->ajax()) {
            return \Response::json([
                'success'  => 1,
                'callback' => 'discounts-increased'
            ], 200)->cookie($cookie_key, true, $cookie_timeout);
        } else {
            return redirect()->back()->with(['success' => 'Discounts increased'])->cookie($cookie_key, true, $cookie_timeout);
        }
    }



    protected function getDiscountCookieKey($lead) {

        return sprintf("increase-discounts-user-%d-lead-%d", Auth::user()->id, $lead->id);
    }

    /**
     * Do we want to add a badge to the row for this lead?
     * Currently we use this to flag unread SMS messages
     * and append it to the "full name" column
     *
     * @param object $lead
     * @return string HTML for the badge
     */
    protected function getBadgeForLead($lead) {
        if ($unread = $lead->getUnreadSMSCount()) {
            return '<span class="badge badge-danger">'.$unread.' unread SMS</span>';
        }
    }


}

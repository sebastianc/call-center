<?php

namespace App\Http\Controllers;

use App\Services\Audit\LeadAuditService;
use App\Services\Legacy\EngagementHistoryService;
use Aws\S3\Exception\S3Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

use Auth;
use DB;
use Carbon\Carbon;

use Illuminate\Http\Response;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;
use Lab404\Impersonate\Impersonate;
use Yajra\Datatables\Datatables;

use App\Team;
use App\User;
use App\Role;
use App\Lead;
use App\Sms;
use App\Survey;
use App\Allocation;
use App\Call;
use App\Callback;
use App\CatalogueItem;
use App\Interest;
use App\LeadStatus;
use App\Network;
use App\Note;

use App\Rules\StrongPasswordRule;
use Lab404\Impersonate\Services\ImpersonateManager;

class CRMController extends Controller
{

    /**
     * Use this to store any warnings we want to show the user; we can use this for errors and successes too
     * This may not be the best approach, we should review this in time.
     *
     * @var [type]
     */
    protected $warnings;

    /**
     * Require the user to be logged in for everything in this controller
     */
    public function __construct()
    {
        $this->middleware('auth');

        $this->warnings = [];

        $this->view_folder = 'crm.themes.' . config('crm.theme');
    }

    protected function view($template, $variables = [])
    {
        return view($this->view_folder . '.' . $template, $variables);
    }

    public function dashboard()
    {
        $counts = [];

        /**
         * TODO: cache these values
         */
        $counts['users']           = User::count();
        $counts['catalogue-items'] = CatalogueItem::count();
        $counts['leads']           = Lead::count();
        $counts['teams']           = Team::count();

        $counts['open-pot']        = Lead::filterForOpenPot()->count();

        return $this->view('screens.dashboard', [
            'counts' => $counts
        ]);
    }

    /**
     * Load a datatable for the class specified
     *
     * @param string $class A valid class (Lead, User)
     * @return void
     */
    public function listObjects($list)
    {

        $columns = $this->getColumnsForList($list);
        $filters = $this->getFiltersForList($list);
        $editable_fields = $this->getEditableFieldsForList($list);

        /**
         * $filters is a more generic version of $colleagues and will eventually replace it
         */
        $colleagues = Auth::user()->colleagues(false)->pluck('name', 'id');

        /**
         * This needs to be customised (ie, it might be "all leads for users in your team")
         */
        switch ($list) {
            case 'leads':
                if (Auth::user()->isTeamLeader()) {
                    $page_header = 'This table shows all the leads belonging to people in your team(s)';
                } else {
                    $page_header = 'This table shows all your leads';
                }
                break;
            case 'open-pot':
                $page_header = 'This table shows the open pot';
                break;
            case 'renewals-pot':
                if(Auth::user()->onRenewalsTeam() || Auth::user()->isTeamLeader()) {
                    $page_header = 'This table shows the renewals pot';
                } else {
                    return Redirect::to('crm')->with('unauth', 'You are not authorised to access these leads
                                                  , contact your manager for access');
                }
                break;
            case 'engagement-pot':
                if (Auth::user()->onEngagementTeam() || Auth::user()->isTeamLeader() || Auth::user()->onRenewalsTeam()) {
                    $page_header = 'This table shows the engagement pot';
                } else {
                    return Redirect::to('crm')->with('unauth', 'You are not authorised to access these leads
                                                  , contact your manager for access');
                }
                break;
            case 'users':
                if (Auth::user()->isTeamLeader()) {
                    $page_header = 'This table shows all users in the system';
                } else {
                    return Redirect::to('crm')->with('unauth', 'You shall not pass!!!!!!!!' );
                }
                break;
            case 'teams':
                $page_header = 'This table shows all teams';
                break;
            case 'test-users':
            case 'test-leads':
                $page_header = 'This is a dummy table for testing purposes';
                break;
            default:
                trigger_error("Unhandled list $list");
        }

        /**
         * This will definitely need to be refactored, it's too generic
         * and it's possibly to clunky to have to call before every page render
         */
        $this->setWarnings();

        return $this->view('screens.datatable', [
            'page_header'     => $page_header,
            'list'            => $list,
            'columns'         => $columns,
            'filters'         => $filters,
            'editable_fields' => $editable_fields,
            'warnings'        => $this->warnings,
            /** We could put this in a shared view variable perhaps */
        ]);

    }


    /**
     * Get the columns we'll need to render a specific list, by name
     *
     * @param string $list A valid list name
     * @return void
     */
    public function getColumnsForList($list)
    {
        switch ($list) {
            case 'leads':
                return $this->getColumnsForClass('lead');
                break;
            case 'open-pot':
            case 'engagement-pot':
            case 'renewals-pot':
                return $this->getColumnsForOpenPot('lead');
                break;
            case 'users':
                return $this->getColumnsForClass('user');
                break;
            case 'interests':
                return $this->getColumnsForClass('interest');
                break;
            case 'test-users':
                return $this->getColumnsForTesting('user');
                break;
            case 'test-leads':
                return $this->getColumnsForTesting('lead');
                break;
            case 'teams':
                return $this->getColumnsForTesting('team');
                break;
            default:
                trigger_error("Unhandled list name $list");
                break;
        }
    }

    /**
     * Get the columns we'll need to render a specific class of items
     *
     * @param string $class A valid class (Lead, User)
     * @return void
     */
    public function getColumnsForClass($class)
    {

        switch ($class) {
            case 'lead':
                return $this->getColumnsForLeads();
                break;
            case 'user':
                return $this->getColumnsForUsers();
                break;
            case 'interest':
                return $this->getColumnsForInterests();
                break;
            default:
                trigger_error("Unhandled class $class");
        }
    }

    /*
     * Set columns for leads view
     */
    protected function getColumnsForLeads()
    {

        $columns = [];

        $column = [
            'data' => 'first_name',
            'name' => 'first_name',
            'searchable' => true,
            'visible' => false,
            'label' => 'First name' // Used to render <th>
        ];
        $columns[] = (object)$column;


        $column = [
            'data' => 'last_name',
            'name' => 'last_name',
            'searchable' => true,
            'visible' => false,
            'label' => 'Last name'
        ];
        $columns[] = (object)$column;

        $column = [
            'data' => 'full_name',
            'name' => 'full_name',
            'searchable' => false,
            'label' => 'Name'
        ];
        $columns[] = (object)$column;


        if (Auth::user()->isTeamLeader()) {
            /**
             * Team Leaders and Account managers view lists of leads for all users,
             * so need to be able to filter the list by user
             */
            $column = [
                'data' => 'user.name',
                'name' => 'user.id',
                'label' => 'Owner',
                'orderable' => false,
                /** We use a dropdown filter here, no point in sorting **/
            ];

            $columns[] = (object)$column;
        }

        $column = [
            'data' => 'cached_callback_time',
            'name' => 'cached_callback_time',
            'label' => 'Callback set?',
        ];
        $columns[] = (object)$column;

        $column = [
            'data' => 'cached_call_count',
            'name' => 'cached_call_count',
            'label' => 'Call count'
        ];
        $columns[] = (object)$column;

        return $columns;
    }


    /*
     * Set columns for the open pot, very similar to leads but not identical
     */
    protected function getColumnsForOpenPot()
    {

        $columns = [];

        $column = [
            'data' => 'first_name',
            'name' => 'first_name',
            'searchable' => true,
            'visible' => false,
            'label' => 'First name' // Used to render <th>
        ];
        $columns[] = (object)$column;


        $column = [
            'data' => 'last_name',
            'name' => 'last_name',
            'searchable' => true,
            'visible' => false,
            'label' => 'Last name'
        ];
        $columns[] = (object)$column;

        $column = [
            'data' => 'full_name',
            'name' => 'full_name',
            'searchable' => false,
            'label' => 'Name'
        ];
        $columns[] = (object)$column;

        $column = [
            'data' => 'user.name',
            'name' => 'user.id',
            'label' => 'Owner',
            'orderable' => false,
            /** We use a dropdown filter here, no point in sorting **/
        ];

        $columns[] = (object)$column;

        $column = [
            'data' => 'leadstatus.title',
            'name' => 'leadstatus.id',
            'label' => 'Status',
            'orderable' => false,
            /** We use a dropdown filter here, no point in sorting **/
        ];

        $columns[] = (object)$column;

        $column = [
            'data' => 'cached_last_contacted',
            'name' => 'cached_last_contacted',
            'label' => 'Last contacted',
        ];
        $columns[] = (object)$column;

        $column = [
            'data' => 'cached_pcb_count',
            'name' => 'cached_pcb_count',
            'label' => 'PCBs'
        ];
        $columns[] = (object)$column;

        $column = [
            'data' => 'cached_call_count',
            'name' => 'cached_call_count',
            'label' => 'Calls'
        ];
        $columns[] = (object)$column;

        $column = [
            'data' => 'cached_total_pitches',
            'name' => 'cached_total_pitches',
            'label' => 'Pitches'
        ];
        $columns[] = (object)$column;


        return $columns;
    }

    /*
     * Set columns for users data table.
     */
    protected function getColumnsForUsers()
    {
        $columns = [];

        $column = [
            'data' => 'name',
            'name' => 'users.name',
            'label' => 'Name',
        ];
        $columns[] = (object)$column;

        /**
         * If this user is a Team Leader, OR an account manager on more than one team,
         * allow them to filter users by team (ie, view users across multiple teams)
         */
        if (
            Auth::user()->isTeamLeader()
            ||
            (Auth::user()->isAccountManager() && Auth::user()->teams()->count() > 1)
        ) {
            $column = [
                'data' => 'teams',
                'name' => 'teams.name',
                'label' => 'Team',
                'orderable' => false,
            ];
            $columns[] = (object)$column;

        }

        $column = [
            'data' => 'roles',
            'name' => 'roles.title',
            'label' => 'Role',
            'orderable' => false,
        ];
        $columns[] = (object)$column;

        return $columns;
    }

    /*
     * Set columns for the "Interests" data table on the Edit lead page
     */
    protected function getColumnsForInterests()
    {
        $columns = [];

        $column = [
            'data' => 'catalogue_item.title',
            'name' => 'catalogue_item.title',
            'label' => 'Product',
        ];
        $columns[] = (object)$column;

        $column = [
            'data' => 'created_at',
            'name' => 'interests.created_at',
            'label' => 'Date',
        ];

        $columns[] = (object)$column;

        $column = [
            'data' => 'interest_percentage',
            'name' => 'interests.interest_percentage',
            'label' => 'Interest',
            'className' => 'editable',
        ];
        $columns[] = (object)$column;

        $column = [
            'data' => 'catalogue_item.price',
            'name' => 'catalogue_item.price',
            'label' => 'RRP',
        ];

        $columns[] = (object)$column;

        $column = [
            'data' => 'sale_options',
            'name' => 'sale_options',
            'label' => 'Sale',
        ];

        $columns[] = (object)$column;

        $column = [
            'data' => 'discount_percentage',
            'name' => 'discount_percentage',
            'label' => 'Discount',
        ];

        $columns[] = (object)$column;

        return $columns;
    }

    protected function getColumnsForTesting($list)
    {
        $columns = [];

        switch ($list) {
            case 'user':
                $column = [
                    'data' => 'name',
                    'name' => 'users.name',
                    'label' => 'Name',
                ];
                $columns[] = (object)$column;

                $column = [
                    'data' => 'teams',
                    'name' => 'teams.name',
                    'label' => 'Team',
                    'orderable' => false,
                    'render' => "[,<br>].name"
                ];
                $columns[] = (object)$column;

                $column = [
                    'data' => 'roles',
                    'name' => 'roles.title',
                    'label' => 'Role',
                    'orderable' => false,
                    'render' => "[,<br>].title"
                ];
                $columns[] = (object)$column;
                break;
            case 'lead':
                $column = [
                    'data' => 'first_name',
                    'name' => 'leads.first_name',
                    'label' => 'First name',
                ];
                $columns[] = (object)$column;

                $column = [
                    'data' => 'last_name',
                    'name' => 'leads.last_name',
                    'label' => 'Last name',
                ];
                $columns[] = (object)$column;

                $column = [
                    'data' => 'user.name',
                    'name' => 'user.id',
                    'label' => 'Owner',
                    'orderable' => false,
                ];
                $columns[] = (object)$column;

                break;
            case 'team':
                $column = [
                    'data' => 'name',
                    'name' => 'name',
                    'label' => 'Name',
                ];
                $columns[] = (object)$column;
                break;
        }

        return $columns;
    }

    /**
     * Get the filters that we can apply to this $list
     *
     * @param string $list A valid list name
     * @return void
     *
     */
    public function getFiltersForList($list)
    {
        switch ($list) {
            case 'leads':
            case 'open-pot':
            case 'renewals-pot':
            case 'engagement-pot':
                return $this->getFiltersForClass('lead');
                break;
            case 'users':
                return $this->getFiltersForClass('user');
                break;
            case 'test-users':
                return $this->getFiltersForTesting('user');
                break;
            case 'test-leads':
                return $this->getFiltersForTesting('lead');
                break;
            case 'teams':
                return [];
                break;
            default:
                trigger_error("Unhandled list name $list");
                break;
        }
    }

    /**
     * Get the filters that we can apply to these $class objects
     *
     * @param string $class A valid class (Lead, User)
     * @return void
     */
    public function getFiltersForClass($class)
    {

        switch ($class) {
            case 'lead':
                return $this->getFiltersForLeads();
                break;
            case 'user':
                return $this->getFiltersForUsers();
                break;
            default:
                trigger_error("Unhandled class $class");
        }
    }

    /*
     *  Filterable columns for leads
     */
    protected function getFiltersForLeads()
    {
        $filters = [];

        $colleagues = Auth::user()->colleagues(false)->pluck('name', 'id');
        $filters['user.id:name'] = $colleagues;

        $status = LeadStatus::get()->pluck('title', 'id');
        $filters['leadstatus.id:name'] = $status;
        return $filters;

    }

    /*
    *  Filterable columns for users
    */
    protected function getFiltersForUsers()
    {
        $filters = [];

        /**
         * Filter so that CAs can only see teams to which they belong
         */
        $user = Auth::user();
        if ($user->isTeamLeader()) {
            $teams = Team::pluck('name', 'id');
        }
        else {
            $teams = $user->teams()->pluck('teams.name', 'teams.id');
        }

        $filters['teams.name:name'] = $teams;

        $roles = Role::pluck('title', 'id');
        $filters['roles.title:name'] = $roles;

        return $filters;
    }

    protected function getFiltersForTesting($list)
    {
        $filters = [];

        switch ($list) {
            case 'user':
                $teams = Team::pluck('name', 'id');
                $filters['teams.name:name'] = $teams;

                $roles = Role::pluck('title', 'id');
                $filters['roles.title:name'] = $roles;
                break;
            case 'lead':

                $colleagues = Auth::user()->colleagues(false)->pluck('name', 'id');
                $filters['user.id:name'] = $colleagues;

                break;
        }

        return $filters;
    }

    /**
     * Get the fields that we can edit for this $list
     *
     * @param string $list A valid list name
     * @param integer $id The ID of an object that we use to filter the fields, if necessary
     * @return void
     *
     */
    public function getEditableFieldsForList($list, $id = false)
    {
        switch ($list) {
            case 'leads':
            case 'open-pot':
            case 'renewals-pot':
            case 'engagement-pot':
                return $this->getEditableFieldsForClass('lead');
                break;
            case 'users':
                return $this->getEditableFieldsForClass('user');
                break;
            case 'interests':
                return $this->getEditableFieldsForClass('interest', $id);
                break;
            case 'teams':
            case 'test-users':
            case 'test-leads':
                return [];
                break;
            default:
                trigger_error("Unhandled list name $list");
                break;
        }
    }

    /**
     * Get the fields that we can edit for these $class objects (ie, inline or bulk editing_)
     *
     * @param string $class A valid class (Lead, User)
     * @param integer $id The ID of an object that we use to filter the fields, if necessary
     * @return void
     */
    public function getEditableFieldsForClass($class, $id = false)
    {

        switch ($class) {
            case 'lead':
                return $this->getEditableFieldsForLeads();
                break;
            case 'user':
                return $this->getEditableFieldsForUsers();
                break;
            case 'interest':
                return $this->getEditableFieldsForInterests($id);
                break;
            default:
                trigger_error("Unhandled class $class");
        }
    }

    protected function getEditableFieldsForLeads()
    {

        $editable_fields = [];

        /**
         * Pull the options for users from getFilters(), as they're the same values
         * -- although we need to key the array differently
         */
        $filters = $this->getFiltersForLeads();
        $options = [];
        foreach ($filters['user.id:name'] as $id => $value) {
            $options[] = [
                'value' => $id,
                'label' => $value,
            ];
        }

        $editable_field = [
            'label' => 'Owner:',
            'name' => 'user_id',
            'type' => 'select',
            'options' => $options,
        ];
        $editable_fields[] = (object)$editable_field;

        return $editable_fields;
    }

    protected function getEditableFieldsForUsers()
    {

        $editable_fields = [];

        $filters = $this->getFiltersForUsers();
        $team_options = [];
        $role_options = [];
        foreach ($filters['teams.name:name'] as $id => $value) {
            $team_options[] = [
                'value' => $id,
                'label' => $value,
            ];
        }
        foreach ($filters['roles.title:name'] as $id => $value) {
            $role_options[] = [
                'value' => $id,
                'label' => $value,
            ];
        }

        $editable_field = [
            'label' => 'Teams:',
            'name' => 'teams[].id',
            'type' => 'checkbox',
            'options' => $team_options
        ];
        $editable_fields[] = (object)$editable_field;

        $editable_field = [
            'label' => 'Roles:',
            'name' => 'roles[].id',
            'type' => 'checkbox',
            'options' => $role_options
        ];
        $editable_fields[] = (object)$editable_field;

        return $editable_fields;
    }

    protected function getEditableFieldsForInterests($lead_id)
    {

        $editable_fields = [];

        /**
         * Establish the catalogue items we're already interested in, and exclude them from the list:
         */
        $lead = Lead::find($lead_id);
        /**
         * Note that we exclude interests that are renewals of other interests; these can always be registered.
         * This means that we can have a user who has purchased membership in the past, but who can renew that
         * membership repeatedly. At some point in 2020 this might become an issue, if a renewal needs to be
         * renewed, but the system will have no doubt evolved and changed by then anyway.
         */
        $interest_ids = $lead->interests->where('renews_catalogue_item_id', 0)->pluck('catalogue_item_id')->toArray();
        $catalogue_items = CatalogueItem::get();

        $options = [];
        foreach ($catalogue_items as $catalogue_item) {
            if (!in_array($catalogue_item->id, $interest_ids)) {
                $options[] = [
                    'value' => $catalogue_item->id,
                    'label' => $catalogue_item->title,
                ];
            }
        }
        if (!$options) {
            $options[] = [
                'value' => '',
                'label' => 'All possible interests already listed'
            ];
        }

        $editable_field = [
            'label' => 'Product:',
            'name' => 'catalogue_item_id',
            'type' => 'select',
            'options' => $options,
        ];
        $editable_fields[] = (object)$editable_field;

        $options = [];
        $options[] = [
            'value' => '0',
            'label' => 'None',
        ];
        foreach ([25, 50, 75, 100] as $percentage) {
            $options[] = [
                'value' => $percentage,
                'label' => $percentage . '%',
            ];
        }
        $options[] = [
            'value' => 'remove',
            'label' => 'Remove',
        ];

        $editable_field = [
            'label' => 'Interest level',
            'name' => 'interest_percentage',
            'type' => "select",
            'options' => $options,
            'attr' => ['class' => 'form-control-sm']
        ];
        $editable_fields[] = (object)$editable_field;

        return $editable_fields;
    }

    protected function setWarnings()
    {
        $user = Auth::user();

        /**
         * We don't restrict the view of leads for account managers or team leaders
         */
        if (!$user->isTeamLeader()) {
            if ($user->hasOverdueCallbacks()) {
                $this->warnings[] = 'You have overdue callbacks (highlighted in red). You will not be able to view any new leads until these overdue leads have been actioned.';
            }
        }
    }

    /**
     *  Audit logs
     */

    public function history($class, $id)
    {
        switch ($class) {
            case 'lead':
                $lead = Lead::find($id);
                $audits = $lead->audits;
                return view("crm/leadHistory", ['audits' => $audits]);
                break;
            case 'user':
                // Edit user screen to be included here?
                break;
            default:
                trigger_error("Unhandled class $class");
        }
    }

    /**
     * Main edit lead entry point
     */
    /**
     * This needs refactoring into separate functions, possibly in different controllers
     */
    public function editObject(ImpersonateManager $manager, $class, $id = false)
    {
        switch ($class) {
            case 'lead':
                $lead = Lead::findOrFail($id);
                $interests = $lead->interests;
                $columns = $this->getColumnsForList('interests');
                $editable_fields = $this->getEditableFieldsForList('interests', $lead->id);
                $status = LeadStatus::all();
                $users = Auth::user()->colleagues(false);
                $survey = Survey::firstOrNew([ /** No need to save this to the DB yet **/
                    'lead_id'=>$lead->id
                ]);
                $member_survey_questions = Survey::getQuestions();

                return $this->view(
                    "screens.edit-lead",
                    [
                        'lead'                    => $lead,
                        'interests'               => $interests,
                        'columns'                 => $columns,
                        'editable_fields'         => $editable_fields,
                        'status'                  => $status,
                        'users'                   => $users,
                        'survey'                  => $survey,
                        'member_survey_questions' => $member_survey_questions,
                        'impersonated' => $manager->isImpersonating(),
                    ]
                );
                break;
            case 'user':
                if (Auth::user()->isTeamLeader() || Auth::user()->isAccountManager()) {
                    $user = User::find($id);
                    $teams = $user->teams;
                    $roles = $user->roles;
                    $allocations = $user->allocations;
                    return $this->view("screens.edit-user", [
                        'user' => $user,
                        'user_teams' => $teams,
                        'user_roles' => $roles,
                        'allocations' => $allocations,
                        'roles' => Role::all(),
                        'teams' => Team::all(),
                        'current_teams' => $user->teams()->pluck('teams.id')->toArray(),
                        'current_roles' => $user->roles()->pluck('roles.id')->toArray(),
                    ]);
                } else {
                    return response("Unable to view this page", Response::HTTP_I_AM_A_TEAPOT);
                }
                break;
            case 'team':
                if (!Auth::user()->isTeamLeader()) {
                    abort(403);
                }
                if ($id) {
                    $team = Team::findOrFail($id);
                }
                else {
                    $team = new Team;
                }

                return $this->view("screens.edit-team", [
                    'team' => $team,
                ]);
                break;
            default:
                trigger_error("Unhandled class $class");
        }
    }


    public function createObject($class)
    {
        switch ($class) {
            case 'lead':
                return $this->view("screens.createLead", ['networks' => Network::all(), 'users' => User::all()]);
                break;
            case 'user':
                return $this->view(
                    "screens.createUser",
                    ['teams' => Team::all(), 'roles' => Role::all(), 'networks' => Network::all()]
                );
                break;
            default:
                trigger_error("unhandled class $class");
        }
    }


    /**
     * Add interest
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     *
     * Todo expand out into 1 master function
     */
    public function addInterest($id, Request $request)
    {
        $request->validate([
            'catalogue_value' => 'required'
        ]);

        try {
            $interest = new Interest;
            $interest->lead_id = $id;
            $interest->catalogue_item_id = $request->input("catalogue_items");
            $interest->interest_percentage = $request->input("catalogue_interest");
            $interest->sold = 0;
            $interest->sale_value = $request->input("catalogue_value");
            $interest->save();
        } catch (QueryException $exception) {
            return redirect()->back()->withErrors(['Error' => "Issue has arisen registering new interests"]);
        }

        return redirect()->back()->with(['success' => 'Lead interest registered']);

    }

    /**
     * Render a block of content for an Ajax call
     *
     * @param Request $request
     * @param string $block The name of the block
     * @param integer $id If we're loading a block for a specific element
     * @return void
     */
    /**
     * TODO: stick all this in a separate RenderController or something
     */
    public function renderAjaxBlock(Request $request, $block, $id = false)
    {
        switch ($block) {
            case 'payment-card':
                return $this->renderPaymentPanel($request);
                break;
            case 'lead-notes':
                return $this->renderNotesPanel($id);
                break;
            case 'lead-calls':
                return $this->renderCallsPanel($id);
                break;
            case 'lead-engagement':
                return $this->renderEngagementPane($id);
                break;
            case 'lead-sms':
                return $this->renderSMSPanel($id);
                break;
            case 'lead-emails':
                return 'EMAILS GO HERE';
                break;
            case 'lead-audit':
                return $this->renderAuditPanel($id);
                break;
            case 'add-note':
                return $this->renderAddNoteModal($id);
                break;
            case 'send-sms':
                return $this->renderSendSMSModal($id);
                break;
            case 'reschedule-callback':
                return $this->renderRescheduleCallbackModal($id);
                break;
            case 'increase-discounts':
                return $this->renderIncreaseDiscountsModal($id);
                break;
            case 'next-callback':
                return $this->renderNextCallbackPanel($id);
                break;
            default:
                \trigger_error("Unhandled block $block");
        }
    }

    /**
     * Render the "Payment" panel for the "Edit Lead" screen, for a given lead
     * This is so we can pull the content into the page using Ajax
     * and thereby update it quickly whenever we add items to the "basket"
     *
     * @return void
     */
    public function renderPaymentPanel($request)
    {
        $lead_id = $request->input('lead_id');
        $lead = Lead::find($lead_id);

        if (is_null($lead)) {
            return 'An error has occurred';
        }

        $interests = $lead->interests()->where('in_basket', 1)->get();
        return $this->view('cards.payment', [
            'lead' => $lead,
            'interests' => !$interests->isEmpty() ? $interests : false,
        ]);
    }

    public function renderNotesPanel($lead_id)
    {

        $lead = Lead::find($lead_id);
        $notes = $lead->notes;

        return $this->view('tabs.notes', [
            'lead' => $lead,
            'notes' => $notes
        ]);

    }

    public function renderNextCallbackPanel($lead_id) {
        $lead = Lead::find($lead_id);

        return $this->view('cards.next-callback', [
            'lead' => $lead,
        ]);
    }

    public function renderAuditPanel($lead_id)
    {
        $lead = Lead::find($lead_id);
        //$audits = $lead->audits()->orderBy('created_at', 'desc')->get();
        $audits = (new LeadAuditService($lead_id))->getAuditData();
        $currentPage = LengthAwarePaginator::resolveCurrentPage();
        $auditCollection = collect($audits);
        $perPage = 5;
        $currentPageItems = $auditCollection->slice(($currentPage *  $perPage) -  $perPage, $perPage)->all();
        $auditedItems = new LengthAwarePaginator($currentPageItems, count($auditCollection),$perPage);
        $auditedItems->setPath(route('crm.ajax-block', ['block'=>'lead-audit', 'id'=>$lead_id])); //TODO..... figure out how to get request into the page
        return $this->view('tabs.audit', [
            'lead' => $lead,
            'audits' => $auditedItems,
            'total_pages' => ceil(round(count($audits) / $perPage,0)),
            'current_page' => $currentPage
        ]);

    }


    public function renderSMSPanel($lead_id)
    {

        $lead = Lead::find($lead_id);
        $sms = $lead->sms;

        /**
         * Mark all SMS as read
         */
        SMS::where([
            'lead_id'=>$lead->id,
            'inbound'=>1,
        ])->update([
            'read'=>1
        ]);

        return $this->view('tabs.sms', [
            'lead' => $lead,
            'sms' => $sms
        ]);

    }

    public function renderCallsPanel($lead_id)
    {

        $lead = Lead::find($lead_id);
        $calls = $lead->calls;

        return $this->view('tabs.calls', [
            'lead' => $lead,
            'calls' => $calls
        ]);

    }

    public function renderEngagementPane($lead_id)
    {
        $lead = Lead::find($lead_id);

        $engagement = (is_null($lead->member_id)) ? null :  (new EngagementHistoryService())->get('/people/'.$lead->member_id.'/engagement');

        return $this->view('tabs.engagement',[
            'engagement_history' => $engagement
        ]);
    }

    public function renderAddNoteModal($lead_id)
    {

        $lead = Lead::find($lead_id);

        return $this->view('modals.add-note', [
            'lead' => $lead,
        ]);
    }


    public function renderSendSMSModal($lead_id)
    {

        $lead = Lead::find($lead_id);

        return $this->view('modals.send-sms', [
            'lead' => $lead,
        ]);
    }

    public function renderRescheduleCallbackModal($lead_id)
    {

        $lead = Lead::find($lead_id);

        return $this->view('modals.reschedule-callback', [
            'lead' => $lead,
        ]);
    }

    public function renderIncreaseDiscountsModal($lead_id)
    {

        $lead = Lead::find($lead_id);

        return $this->view('modals.increase-discounts', [
            'lead' => $lead,
        ]);
    }

    /**
     * Check that the user is logged in, and optionally
     * owns (or has permission to modify) this lead
     *
     * @return mixed
     */
    public function getBlockingModal(Request $request)
    {
        $blocked      = false;
        $modalContent = '';
        $lead_id = $request->input('lead_id');

        if (!Auth::check()) {
            $blocked      = true;
            $modalContent = $this->view('modals.logged-out')->render();
        } else if ($lead_id) {
            /**
             * Eloquent isn't ideal here as we hammer this query quite a bit
             * If we add too much load on the server, we'll have to rewrite the
             * visibleTo() method in pure SQL.
             */
            $lead = Lead::find($lead_id);
            if (!$lead->isVisibleTo()) {
                $blocked      = true;
                $modalContent = $this->view('modals.lead-taken')->render();
            }
        }

        return \Response::json([
            'blocked'      => $blocked,
            'modalContent' => $modalContent,
        ], 200);
    }

}

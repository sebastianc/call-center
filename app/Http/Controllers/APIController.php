<?php

namespace App\Http\Controllers;

use App\Allocation;
use App\CatalogueItem;
use App\EmailLog;
use App\Interest;
use App\Lead;
use App\LeadSource;
use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;

class APIController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     *
     *  Logic for creating a new lead.
     */
    public function generateNewLead(Request $request)
    {
        // Important to call own validator or re-direct rule is auto-used
        $validation = Validator::make($request->all(), [
            'phone_number' => 'required|min:6|max:18', // allow +44
            'first_name' => 'required|alpha|min:2|max:100',
            'last_name' => 'required|alpha|min:2|max:100',
            'email_address' => 'required|email|min:5|max:256',
            'network_id' => 'required|digits_between:1,99',
            'country_id' => 'digits_between:1,99',
            'user_id' => 'digits_between:1,9999'
        ]);
        // Return error response
        if ($validation->fails()) {
            $responseArray = array(
                'success' => false,
                'message' => 'Request has failed validation test ',
                'response_code' => Response::HTTP_BAD_REQUEST,
                'data' => $validation->errors()
            );
            return response()->json($responseArray, $responseArray['response_code']);
        }

        // perform a find on existing
        $existingLead = $this->leadExists($request->input('email_address'), $request->input("phone_number"));


        //TODO recap logic?
        if ($existingLead > 0) {
            $responseArray = array(
                'success' => false,
                'message' => 'Lead already exists with submitted email address / phone number ',
                'response_code' => Response::HTTP_BAD_REQUEST,
                'data' => array()
            );
            return response()->json($responseArray, $responseArray['response_code']);
        } else {

            $newID = $this->createLead($request);

            $responseArray = array(
                'success' => true,
                'message' => 'Successfully created new lead with ID : ' . $newID,
                'response_code' => Response::HTTP_ACCEPTED,
                'data' => array(Lead::find($newID))
            );

            // Distribute
            return response()->json($responseArray, $responseArray['response_code']);
        }

    }

    /**
     * @param $request
     * @return \Illuminate\Http\JsonResponse
     */
    private function createLead($request)
    {
        try {
            if($request->exists('user_id') && $request->input('user_id') > 0){
                $allocation = array('user_id' => $request->input('user_id') , 'team' => User::find($request->input('user_id'))->teams->first()->id);
            }else{
                $allocation = $this->leadDistribution($request->input('network_id')); // Fetch who it should go to.
            }

            // Fresh new leads
            $lead = new Lead;
            $lead->first_name = $request->input('first_name');
            $lead->last_name = $request->input('last_name');
            $lead->email_address = $request->input('email_address');
            $lead->phone_number = $request->input('phone_number');
            $lead->team_id = $allocation['team'];
            $lead->user_id = $allocation['user_id'];
            $lead->save();
            $newLeadID = $lead->id;

            // Update allocation

            $user_allocation = Allocation::where('network_id',$request->input('network_id'))->where('user_id',$allocation['user_id'])->first();
            $user_allocation->allocated = $user_allocation->allocated + 1;
            $user_allocation->save();

            $source = new LeadSource;
            $source->lead_id = $newLeadID;
            $source->network_id = $request->input('network_id');
            $source->lead_source = ($request->exists('source'))? $request->input('source') : "N/A";
            $source->created_at = Carbon::now()->format("Y-m-d H:i:s");
            $source->save();


            return $newLeadID;

        } catch (QueryException $queryException) {
            $responseArray = array(
                'success' => false,
                'message' => 'Server error has occurred, unable to create a new lead, please contact development',
                'response_code' => Response::HTTP_INTERNAL_SERVER_ERROR,
                'data' => array('error' => $queryException->getMessage())
            );
            return response()->json($responseArray, $responseArray['response_code']);
        }
    }

    /**
     * @param $email
     * @param $phone_number
     * @return int
     *
     * Check if eloquent finds someone by email or first name
     */
    private function leadExists($email, $phone_number)
    {
        // check for email first as this is key no 1
        $findLeadByEmail = Lead::where('email_address', 'LIKE', "%{$email}%")->count();
        $findLeadByPhone = Lead::where('phone_number', 'LIKE', "%{$phone_number}%")->count();

        if ($findLeadByEmail > 0) {
            $lead = Lead::where('email_address', 'LIKE', "%{$email}%")->first();
            return $lead->id;
        } elseif ($findLeadByPhone > 0) {
            $lead = Lead::where('phone_number', 'LIKE', "%{$phone_number}%")->first();
            return $lead->id;
        }

        return 0;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     *
     * Creation method for existing members, essentially to handle direct payments which would be unknown to the CRM
     */
    public function migrateCustomer(Request $request)
    {
        // Important to call own validator or re-direct rule is auto-used
        $validation = Validator::make($request->all(), [
            'phone_number' => 'required|alpha_num|min:6|max:18', // allow +44
            'first_name' => 'required|alpha|min:2|max:100',
            'last_name' => 'required|alpha|min:2|max:100',
            'email_address' => 'required|email|min:5|max:256',
            'catalogue_item_id' => 'required|digits_between:1,99',
            'purchase_amount' => 'required',
            'purchase_date' => 'required',
        ]);
        // Return error response
        if ($validation->fails()) {
            $responseArray = array(
                'success' => false,
                'message' => 'Request has failed validation test ',
                'response_code' => Response::HTTP_BAD_REQUEST,
                'data' => $validation->errors()
            );
            return response()->json($responseArray, $responseArray['response_code']);
        }
        // Assume the script is going to pass
        $purchased_item = $request->input('catalogue_item_id');

        try {
            CatalogueItem::findOrFail($purchased_item);
        } catch (ModelNotFoundException $modelNotFoundException) {
            $responseArray = array(
                'success'       => false,
                'message'       => 'Catalogue item not found, item does not exist',
                'response_code' => Response::HTTP_FORBIDDEN,
                'data'          => array()
            );
            return response()->json($responseArray, $responseArray['response_code']);
        }

        // check for existing lead
        $existingLead = $this->leadExists($request->input('email_address'), $request->input('phone_number'));
        // TODO perhaps expand into for-each to ensure multiple items can be added in one swoosh
        if ($existingLead > 0) {
            // Lead exists to register interest only
            try {
                $interests = new Interest;
                $interests->lead_id = $existingLead;
                $interests->catalogue_item_id = $purchased_item;
                $interests->sold = 1;
                $interests->interest_percentage = 100;
                $interests->sale_value = $request->input('purchase_amount');
                $interests->created_at = $request->input('purchase_date');
                $interests->save();
            } catch (QueryException $queryException) {
                $responseArray = array(
                    'success' => false,
                    'message' => 'Server error has occurred, unable to register interest to existing, please contact development : ' . $queryException->getMessage(),
                    'response_code' => Response::HTTP_INTERNAL_SERVER_ERROR,
                    'data' => array()
                );
                return response()->json($responseArray, $responseArray['response_code']);
            }
        } else {
            // no lead go make one
            try {
                $newID = $this->createLead($request);
                $interests = new Interest;
                $interests->lead_id = $newID;
                $interests->catalogue_item_id = $purchased_item;
                $interests->sold = 1;
                $interests->interest_percentage = 100;
                $interests->sale_value = $request->input('purchase_amount');
                $interests->created_at = $request->input('purchase_date');
                $interests->save();
            } catch (QueryException $queryException) {
                $responseArray = array(
                    'success' => false,
                    'message' => 'Server error has occurred, unable to create a new lead with existing data , please contact development : ' . $queryException->getMessage(),
                    'response_code' => Response::HTTP_INTERNAL_SERVER_ERROR,
                    'data' => array()
                );
                return response()->json($responseArray, $responseArray['response_code']);
            }

        }

    }

    /**
     * @param $leadNetwork
     * @return array
     */
    private function leadDistribution($leadNetwork)
    {
        // New lead created, now move it over to the person who deserves to get it next
        $allocations = Allocation::where('enabled', 1)->where('network_id',
            $leadNetwork)->whereRaw('allocations.allocation != allocations.allocated')->get();

        if ($allocations->isEmpty()) {
            return array('user_id' => 1337, 'team' => 0);
        }

        $allocations = $allocations->toArray(); // Turn to array so can expand filtering

        // Add total for the day so far into the array
        foreach ($allocations as $key => $allocation) {
            $user = $allocation['user_id'];
            $allocations[$key]['allocated_total'] = Allocation::where('user_id', $user)->sum('allocated');
        }

        // Sort array but total account lowest then compare count for that network;
        array_multisort(array_column($allocations, 'allocated_total'), SORT_ASC,
            array_column($allocations, 'allocated'), SORT_ASC,
            $allocations);

        $whoToAllocateTo = $allocations[0]['user_id'];
        $team = User::find($whoToAllocateTo)->teams->first()->id;

        return array('user_id' => $whoToAllocateTo, 'team' => $team);
    }

    /**
     * Post function for marketing.NewCo.networks.com to send data too
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function recordEmailActions(Request $request)
    {

        $validator = Validator::make($request->all(),[
           'email_address' => 'required',
           'note' => 'required',
       ]);

        // Return error response if it doesn't work
        if ($validator->fails()) {
            $responseArray = array(
                'success' => false,
                'message' => 'Request has failed validation test ',
                'response_code' => Response::HTTP_BAD_REQUEST,
                'data' => $validator->errors()
            );
            return response()->json($responseArray, $responseArray['response_code']);
        }
       // find the culprit

        $leadInQuestion = Lead::where('email_address',$request->input('email_address'))->firstOrFail(); // can't log again anyone?


        $log = new EmailLog;
        $log->lead_id = $leadInQuestion->id;
        $log->email_address = $request->input('email_address');
        $log->note = $request->input('note');
        $log->save();

        return response()->json(['success' => true, 'message' => 'log recorded'],201);

    }
}

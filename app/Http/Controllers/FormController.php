<?php

namespace App\Http\Controllers;

use App\Rules\StrongPasswordRule;
use App\Rules\ValidTelephoneNumberRule;
use App\User;
use Cron\AbstractField;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

use Auth;
use DB;
use Carbon\Carbon;

use App\Allocation;
use App\Call;
use App\Callback;
use App\CatalogueItem;
use App\Interest;
use App\Lead;
use App\Note;
use App\SMS;
use App\Team;
use App\Survey;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class FormController extends Controller
{

    /**
     * Require the user to be logged in for everything in this controller
     */
    public function __construct()
    {
        $this->middleware('auth');

    }

    /**
     * Edit a leads basic info
     *
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateLead($id, Request $request)
    {
        $request->validate([
            'first_name' => 'required|min:2|max:100',
            'last_name' => 'required|min:2|max:100',
            'email_address' => 'required|min:2|max:256|email',
            'timezone' => 'required',
            'phone_number' => [new ValidTelephoneNumberRule()]

        ]);

        // Extra careful find the model or exit
        try {
            $lead = Lead::findOrFail($id);
        } catch (ModelNotFoundException $modelNotFoundException) {
            return redirect()->back()->withErrors(['Error' => "Unable to located Lead with ID :" . $id]);
        }

        /**
         * Could $lead->fill($_POST) here
         */
        $lead->first_name = $request->input("first_name");
        $lead->last_name = $request->input("last_name");
        $lead->email_address = $request->input("email_address");
        $lead->phone_number = $request->input("phone_number");
        $status = $request->input("lead_status_id");
        if(!empty($status)) {
            $lead->lead_status_id = $status;
        }
        $lead->timezone = $request->input("timezone");
        $lead->save();

        if ($request->ajax()) {
            return \Response::json(['success' => 1], 200);
        } else {
            return redirect()->back()->with(['success' => 'Lead updated successfully']);
        }

    }

    /**
     * Edit a Team
     *
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateTeam(Request $request, $team_id = false)
    {
        $request->validate([
            'name' => 'required',
        ]);

        if ($team_id) {
            $team = Team::findOrFail($team_id);
            $back = route('crm.list', ['list' => 'teams']);
            $message = 'Team updated successfully';
        } else {
            $team = new Team;
            $back = route('crm.dashboard');
            $message = 'Team added successfully';
        }

        $team->fill($request->all());
        $team->save();

        if ($request->ajax()) {
            return \Response::json(['success' => 1], 200);
        } else {
            return redirect($back)->with(['success' => $message]);
        }

    }


    /**
     * Add note
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     *  Todo expand out into 1 master function
     */
    public function addNote($id, Request $request)
    {
        $request->validate([
            'note_text' => 'required'
        ]);

        $note = Note::create([
            'text' => $request->input("note_text"),
            'user_id' => Auth::user()->id,
            'lead_id' => $id
        ]);

        $response = [
            'callback' => 'note-added',
            'success' => 1
        ];

        if ($request->ajax()) {
            return \Response::json($response, 200);
        } else {
            return redirect()->back()->with(['success' => 'Note saved']);
        }
    }

    public function updateStatus($id, Request $request)
    {
        $request->validate(['status_id' => 'required']);
        try {
            $lead = Lead::find($id);
            $lead->lead_status_id = $request->input('status_id');
            $lead->save();
        } catch (QueryException $queryException) {
            return \response("Error updating lead status id : " . $queryException->getMessage(),
                Response::HTTP_BAD_REQUEST);
        }
        return \response("Successfully update lead status id", Response::HTTP_OK);
    }


    public function updateOwner($id, Request $request)
    {
        $request->validate(['user_id' => 'required']);
        try {
            $lead = Lead::find($id);
            $lead->user_id = $request->input('user_id');
            $lead->save();
        } catch (QueryException $queryException) {
            return \response("Error updating lead status id : " . $queryException->getMessage(),
                Response::HTTP_BAD_REQUEST);
        }
        return \response("Successfully update lead status id", Response::HTTP_OK);
    }

    /**
     * Handle the posted "Schedule Callback" modal
     * A slight rewrite of the earlier version so that we can use Ajax posts
     *
     * @return An ajax response or a redirect
     */
    public function updateCallback(Request $request, $lead_id)
    {

        $validatedData = $request->validate([
            'defer' => [
                'required_without:callback',
                'empty_with:callback'
            ],
            'callback' => [
                'required_without:defer',
                'empty_with:defer'
            ],
            'is_priority' => 'nullable',
            'is_qualified' => 'nullable'
        ]);

        $lead = Lead::find($lead_id);

        $existing_callback = $lead->callback();

        if (!empty($validatedData['defer'])) {
            // Deferred logic here based on existing
            if (!is_null($existing_callback)) {
                $starting_time = Carbon::parse($existing_callback->callback_time);
            } else {
                $starting_time = Carbon::now();
            }

            // validate the callback

            $callback_time = $starting_time->addHours($validatedData['defer']);

            $responseObject = $this->validCallbackChecking($callback_time);
            if ($responseObject->success == false) {
                $response = [
                    'callback' => 'callback-updated',
                    'errors' => ['defer' => $responseObject->reason],
                    'success' => 0
                ];
                if ($request->ajax()) {
                    return \Response::json($response, 422); // The HTTP code 422 indicates that this is an error response
                } else {
                    return redirect()->back()->withErrors(['Invalid' => 'Error : ' . $responseObject->reason]);
                }
            }

            $callback_time = $callback_time->toDateTimeString();
        } else {
            // Non deferred hard coded
            /** Note that we need to match the datepicker format here */
            $date_time_format = 'd/m/Y H:i'; // e.g., Mon 24th Sep 18:30

            $callback_time = Carbon::createFromFormat($date_time_format, $validatedData['callback']);

            // validate the callback

            $responseObject = $this->validCallbackChecking($callback_time);
            if ($responseObject->success == false) {
                $response = [
                    'callback' => 'callback-updated',
                    'errors' => ['callback' => $responseObject->reason],
                    'success' => 0
                ];
                if ($request->ajax()) {
                    return \Response::json($response, 422); // The HTTP code 422 indicates that this is an error response
                } else {
                    return redirect()->back()->withErrors(['Invalid' => 'Error : ' . $responseObject->reason]);
                }
            }
            $callback_time->toDateTimeString();
        }

        Callback::create([
            'user_id' => Auth::user()->id,
            'lead_id' => $lead->id,
            'callback_time' => $callback_time,
            'is_priority' => $validatedData['is_priority'] ?? 0,
            'is_qualified' => $validatedData['is_qualified'] ?? 0
        ]);

        if (!is_null($existing_callback)) {
            $existing_callback->delete();
        }

        $response = [
            'callback' => 'callback-updated',
            'success' => 1
        ];

        if ($request->ajax()) {
            return \Response::json($response, 200);
        } else {
            return redirect()->back()->with(['success' => 'Callback updated']);
        }
    }

    public function validCallbackChecking(Carbon $carbonObject)
    {
        if ($carbonObject->isWeekend() === true) {
            return (object)['success' => false, 'reason' => 'A callback cannot be set for the weekend'];
        }
        if ($carbonObject->hour < 7 || $carbonObject->hour > 17) {
            return (object)['success' => false, 'reason' => 'A callback cannot be set outside working hours'];

        }
        if ($carbonObject->isPast() === true) {
            return (object)['success' => false, 'reason' => 'A callback cannot be set in the past'];
        }
        // Bank holidays
        $jsonArray = file("https://www.gov.uk/bank-holidays.json"); // Official GOV data
        $decoded = json_decode($jsonArray[0]);
        foreach ($decoded as $division) // Split division england-wales , scotland , ireland
        {
            foreach ($division->events as $event) // each divsion has an array of events to check against
            {
                if ($carbonObject->format("Y-m-d") == $event->date) {

                    return (object)['success' => false, 'reason' => 'A callback cannot be set for a bank holiday'];
                }
            }
        }

        return (object)['success' => true, 'reason' => 'Callback set'];
    }

    /**
     * Delete the current callback for this lead
     *
     * @param Request $request
     * @param [type] $lead_id
     * @return void
     */
    public function deleteCallback(Request $request, $id, $callbackId)
    {

        $lead = Lead::find($id);

        $existing_callback = $lead->callback($callbackId);

        if (!is_null($existing_callback)) {
            $existing_callback->delete();
            $response = [
                'callback' => 'callback-deleted',
                'success' => 1
            ];
        } else {
            $response = [
                'error' => 1
            ];
        }

        if ($request->ajax()) {
            return \Response::json($response, 200);
        } else {
            return redirect()->back()->with(['success' => 'Callback deleted']);
        }
    }

    public function createUser(Request $request)
    {

        $request->merge(['email' => $request->input('email_address') . '@NewCo.networks.com']);

        $request->validate([
            'first_name' => 'required|min:2|max:100',
            'last_name' => 'required|min:2|max:100',
            'email_address' => 'required',
            'email' => 'required|email|unique:users',
            'password' => ['required', 'min:8', new StrongPasswordRule()],
            'roles' => 'required',
            'teams' => 'required',

        ]);

        try {
            // create user first
            $token = ($request->exists('api_token')) ? md5(microtime()) : null; // exists -> checkboxes don't post un-ticked

            $newUser = new User;
            $newUser->name = $request->input("first_name") . " " . $request->input("last_name");
            $newUser->email = $request->input("email_address") . "@NewCo.networks.com";
            $newUser->password = Hash::make($request->input("password"));
            $newUser->api_token = $token;
            $newUser->active = 1;
            $newUser->save();

            $newUserID = $newUser->id;

            // now create teams associated
            $teams = $request->input('teams');
            if (!empty($teams)) {
                foreach ($teams as $team) {
                    $newUser->teams()->attach([$team]);
                    $newUser->save();
                }
            }

            // Now create roles given
            $roles = $request->input('roles');
            if (!empty($roles)) {
                foreach ($roles as $role) {
                    $newUser->roles()->attach($role);
                    $newUser->save();
                }
            }

            // Assign network
            if ($request->exists('networks')) {
                $networks = $request->input('networks');
                if (!empty($networks)) {
                    foreach ($networks as $network) {
                        //                $allocation = Allocation::where('user_id', $newUserID)->where('network_id', $network)->first();
                        $allocation = $newUser->allocations->where("network_id", $network)->first();
                        $allocation->enabled = 1;
                        $allocation->save();
                    }
                }
            }

            return redirect()->route('crm.edit', [
                'class' => 'user',
                'id' => $newUserID
            ], Response::HTTP_CREATED)->with(['success' => 'User created']);

        } catch (QueryException $queryException) {
            return redirect()->back()->withErrors(['Database Error ' => $queryException->getMessage()]);
        }
    }

    public function updateCall(Request $request)
    {
        $ended = $request->input('ended');
        $answered = $request->input('answered');
        $pitched = $request->input('pitched');
        if ($ended == 1 || $pitched == 1) {
            $call = Call::updateOrCreate([
                'id' => $request->input('id'),
            ], [
                /**
                 * These values must all be in Call::$fillable
                 */
                'ended_at' => $request->input('ended') ? Carbon::now()->toDateTimeString() : null,
                'answered' => $request->input('answered') ?? 0,
                'pitched' => $request->input('pitched') ?? 0,
                'lead_id' => $request->input('lead_id'),
                'user_id' => Auth::user()->id
            ]);

        } else {
            $call = Call::updateOrCreate([
                'id' => $request->input('id'),
            ], [
                /**
                 * These values must all be in Call::$fillable
                 */
                'ended_at' => $request->input('ended') ? Carbon::now()->toDateTimeString() : null,
                'answered' => $request->input('answered') ?? 0,
                'pitched' => $request->input('pitched') ?? 0,
                'lead_id' => $request->input('lead_id'),
                'user_id' => Auth::user()->id,
                'answered_at' => $answered ? Carbon::now()->toDateTimeString() : null
            ]);

        }





        /**
         * ITBOV-186 If someone calls a lead, that lead should automatically be assigned to them.
         * Note that this will update the lead even if we're hanging up, but that shouldn't matter.
         */

        $lead = Lead::find($request->input('lead_id'));
        $lead->user_id = Auth::user()->id;
        $lead->save();

        /**
         * We should only ever process this via Ajax, but we'll handle a non-Ajax request too:
         * this is handy if the JavaScript fails, if nothing else
         */
        if ($request->ajax()) {

            /**
             * If we've just ended the call, don't return the ID, as we don't want to edit this call again:
             */
            if ($request->input('ended')) {
                $response = [];
            } else {
                $response = ['id' => $call->id];
            }
            return \Response::json($response, 200);
        } else {
            return redirect()->back()->with(['success' => 'Call updated']);
        }
    }


    public function editUser(Request $request)
    {
        $request->validate([
            'user_name' => 'required|min:2|max:100',
            'email_address' => 'required',
        ]);

        try {
            $user = User::find($request->id);
            $user->name = $request->input("user_name");
            $user->email = $request->input("email_address");

            if (strlen($request->input("password")) > 0) {

                $validator = Validator::make($request->all(), [
                    'password' => [new StrongPasswordRule()],
                ]);

                if ($validator->fails()) {
                    return redirect()->back()->withErrors($validator->errors());
                }

                $user->password = Hash::make($request->input("password"));
            } // password changed

            $user->active = $request->input('active') ? true : false;
            $user->api_token = ($request->exists('api_token') && $user->api_token < 1) ? md5(microtime()) : null;
            $user->save();

            // team assignment
            $user->teams()->detach();
            foreach ($request->input('teams') as $teamKey => $team) {
                ($team == "on") ? $user->teams()->attach($teamKey) : '';
            }
            $user->roles()->detach();
            foreach ($request->input('roles') as $roleKey => $role) {
                ($role == "on") ? $user->roles()->attach($roleKey) : '';
            }

            Allocation::where('user_id', $request->id)->update(['enabled' => 0]); // Disable all allocations
            if ($request->exists('enabled')) {
                Allocation::where('user_id', $request->id)->whereIn('network_id',
                    $request->input("enabled"))->update(['enabled' => 1]); // Re-enabled selected ones
            }
            $count = $request->input('count');
            if ($request->exists('allocation')) {
                foreach ($request->input("allocation") as $key => $value) {

                    $allocation = Allocation::where('id', $value)->first();
                    $allocation->allocation = $count[$key];
                    $allocation->save();

                }
            }
        } catch (QueryException $queryException) {
            return redirect()->back()->withErrors(['DB Error' => $queryException->getMessage()]);
        }
        return redirect()->back()->with(['success' => 'User information updated successfully!']);
    }

    public function editUserImage($id, Request $request)
    {
        $request->validate(['image' => 'mimes:jpeg,jpg,png,gif']);
        $imagePath = $request->file("image")->storePublicly('images/Sales-Module', 's3');
        User::where('id', $id)->update(['image' => $imagePath]);
        return redirect()->back()->with(['success' => "Image successfully updated"]);
    }

    /**
     * Handle the posted Member Survey
     * @param  Request $request
     * @return
     */
    public function processMemberSurvey(Request $request)
    {
        $validatedData = $this->validate($request, [
            'lead_id' => 'required'
        ]);

        $lead = Lead::find($validatedData['lead_id']);

        $post = $request->all();
        /**
         * Convert any array values into comma-separated strings;
         * these are SET values and must be passed to updateOrCreate as strings
         */
        array_walk($post, function (&$item, $key) {
            if (is_array($item)) {
                $item = implode(',', $item);
            }
        });
        $survey = Survey::updateOrCreate([
            'lead_id' => $lead->id,
        ], $post);

        /**
         * We should only ever process this via Ajax, but we'll handle a non-Ajax request too:
         */
        if ($request->ajax()) {
            return \Response::json([
                'success' => 1,
                'callback' => 'member-survey'
            ], 200);
        } else {
            return redirect()->back()->with(['success' => 'Call updated']);
        }
    }


}

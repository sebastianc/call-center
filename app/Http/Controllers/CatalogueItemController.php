<?php

namespace App\Http\Controllers;

use App\CatalogueItem;
use App\Services\Payments\StripePlans;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

class CatalogueItemController extends Controller
{
    //

    public function __construct()
    {
        $this->middleware('auth');

    }


    public function create(Request $request)
    {

        $request->validate([
            "catalogue_item_title" => "required",
            "catalogue_item_rrp" => "required",
            "catalogue_item_type" => "required",
            "catalogue_item_discount" => "required",
        ]);

        try {
            $item = new CatalogueItem;
            $item->title = $request->input("catalogue_item_title");
            $item->price = $request->input("catalogue_item_rrp");
            $item->type = $request->input("catalogue_item_type");
            $item->legacy_type = $request->input("catalogue_legacy_type");
            $item->max_discount = $request->input("catalogue_item_discount");
            $item->stripe_plan_id = $request->input("catalogue_stripe_plan");

            $item->save();
        } catch (QueryException $queryException) {
            return redirect()->back()->withErrors(['Error' => "Unable to save item to catalogue"]);

        }
        return redirect("/crm/catalogue/view/all")->with(['success' => 'New Catalogue Item Added']);
    }

    public function update($id, Request $request)
    {
        $request->validate([
            "catalogue_item_title" => "required",
            "catalogue_item_rrp" => "required",
            "catalogue_item_type" => "required",
            "catalogue_item_discount" => "required",
        ]);
        try {
            $item = CatalogueItem::findOrFail($id);
            $item->title = $request->input("catalogue_item_title");
            $item->price = $request->input("catalogue_item_rrp");
            $item->type = $request->input("catalogue_item_type");
            $item->legacy_type = $request->input("catalogue_legacy_type");
            $item->max_discount = $request->input("catalogue_item_discount");
            $item->stripe_plan_id = $request->input("catalogue_stripe_plan");
            $item->save();
        } catch (QueryException $queryException) {
            return redirect()->back()->withErrors(['Error' => "Unable to save item to catalogue"]);
        }
        return redirect("/crm/catalogue/view/all")->with(['success' => 'Catalogue Item Updated']);


    }

    public function reactivate($id)
    {
        try {
            $item = CatalogueItem::withTrashed()->where('id', $id)->first();
            $item->deleted_at = null;
            $item->save();
        } catch (QueryException $queryException) {
            return redirect()->back()->withErrors(['Error' => "Unable to reactivate item from catalogue"]);
        }
        return redirect("/crm/catalogue/view/all")->with(['success' => 'Catalogue Item Reactivate']);
    }

    public function delete($id)
    {
        try {
            $item = CatalogueItem::findOrFail($id);
            $item->delete();
        } catch (QueryException $queryException) {
            return redirect()->back()->withErrors(['Error' => "Unable to delete item from catalogue"]);
        }
        return redirect("/crm/catalogue/view/all")->with(['success' => 'Catalogue Item Removed']);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function views($action, $id = 0)
    {
        switch ($action) {
            case "new":
                return view("crm.themes.basic.screens.catalogueItem", ['edit' => 0]);
                break;
            case "edit":
                return view("crm.themes.basic.screens.catalogueItem",
                    ['catalogueItem' => CatalogueItem::find($id), 'edit' => 1]);
                break;
            case "all":
                return view("crm.themes.basic.screens.catalogueItems", ['catalogueItems' => CatalogueItem::withTrashed()->get()]);
            case "plans":
                return view("crm.themes.basic.screens.stripePlans", ['stripe_data' => $stripeData = (new StripePlans())->get('plans')]);
                break;
            default:
                trigger_error("Unhandled request");
        }
    }

}

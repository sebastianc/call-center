<?php

namespace App\Http\Controllers;

use App\Lead;
use App\Sms;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use \Illuminate\Support\Facades\Validator;
use MessageBird\Client;
use MessageBird\Exceptions\MessageBirdException;
use MessageBird\Objects\Message;

class SMSController extends Controller
{
    private static $messageBird;

    /**
     * SMSController constructor.
     */
    public function __construct()
    {
        self::$messageBird = new Client( env("MESSAGE_BIRD_KEY"));
    }

    public function readBalance()
    {
        try {
            $balance = self::$messageBird->balance->read();
            echo "<pre>";
            print_r($balance);
            echo "</pre>";
        } catch (MessageBirdException $messageBirdException) {
            die($messageBirdException->getMessage());
        }
    }

    /**
     * Send an SMS
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function sendSMS($id, Request $request)
    {
        $request->validate([
            'message' => 'required'
        ]);

        // verify number
        $verify = $this->verifyNumber($request->input("phone_number"));

        if ($verify === false) {
            return \Response::json([
                'success' => 0,
                'callback' => 'sms-non-verified'
            ], Response::HTTP_OK);
        }

        $note = SMS::create([
            'message' => $request->input("message"),
            'phone_number' => $request->input("phone_number"),
            'user_id' => Auth::user()->id,
            'lead_id' => $id
        ]);

        // Calls SMS controller method

        $successSent = $this->sendMessage("inbox", $request->input('phone_number'),
            $request->input('message'));

        if ($successSent === true) {
            if ($request->ajax()) {
                return \Response::json([
                    'success' => 1,
                    'callback' => 'sms-sent'
                ], 200);
            } else {
                return redirect()->back()->with(['success' => 'SMS sent']);
            }
        } else {
            if ($request->ajax()) {
                return \Response::json([
                    'success' => 0,
                    'callback' => 'sms-failure'
                ], 200);
            } else {
                return redirect()->back()->withErrors(['error' => 'SMS failed to send']);
            }
        }


    }

    /**
     * @param $originator
     * @param $recipient
     * @param $message
     */
    public function sendMessage($originator, $recipient, $message)
    {
        $newMessage = new Message();
        $newMessage->originator = $originator;
        $newMessage->recipients = array($recipient);
        $newMessage->body = $message;
        try {
            $response = self::$messageBird->messages->create($newMessage);
            return true;
        } catch (MessageBirdException $messageBirdException) {
            return false;
        }
    }

    /**
     * @param $recipient
     */
    public function verifyNumber($recipient)
    {
        // check the number for +44
        (strpos($recipient,
                "+44") === false) ? $recipient = "+44" . $recipient : $recipient; // Needs country code to verify
        try {
            $request = self::$messageBird->lookup->read($recipient);
            return ($request->getType() == "mobile") ? true : false;
        } catch (MessageBirdException $messageBirdException) {
            return false;
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function readMessage(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'originator' => 'required',
            'body' => 'required',
            'createdDatetime' => 'required',
        ]);

        if ($validator->fails()) {
            return response("Bad Request", Response::HTTP_UNPROCESSABLE_ENTITY);
        }

//        $lead = Lead::where('phone_number',$request->input('originator'))->firstOrFail();

        try {
            Sms::create([
                'message' => $request->input("body"),
                'phone_number' => $request->input("originator"),
                'lead_id' => 1337,
                'inbound' => '1',
            ]);
        } catch (QueryException $queryException) {
            $queryException->getMessage();
        }

        return response("Success", Response::HTTP_OK);

    }
}

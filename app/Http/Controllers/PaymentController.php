<?php

namespace App\Http\Controllers;

use App\Interest;
use App\Lead;
use App\Payment;
use App\Services\Legacy\PersonService;
use App\Services\Legacy\TransactionLegacyProcess;
use App\Services\Legacy\WelcomeEmailService;
use App\Services\Payments\RegularPayment;
use App\Services\Payments\SubscriptionPayment;
use App\User;
use Carbon\Carbon;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use PhpParser\Node\Stmt\Return_;

class PaymentController extends Controller
{
    /**
     * Method for viewing outstanding payments
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function viewOutstanding()
    {
        $payment_types = DB::table('payment_type')->select("id", "title")->get();
        return view("crm.outstanding-payments", [
            'outstanding_interests' => Interest::where('outstanding_value', ">", 0)->with('payment')->get(),
            'users' => User::all(),
            'payment_types' => $payment_types,
            'current_user' => auth()->id()
        ]);
    }


    /** --------------------------------------|
     * Entry Point 1 -- Full Payment
     *
     * Process a full payment through stripe and on success instantly create membership
     * on legacy systems with transactions.
     *
     * @param $id The ID of a lead
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * ---------------------------------------|
     */
    public function processPayment($id, Request $request)
    {
        $request->validate([
            'stripeEmail' => 'required',
            'stripeToken' => 'required',
            'order_amount' => 'required',
        ]);

        $amount = round($request->input("order_amount") / 100, 2);

        $stripeData = (new RegularPayment())->post('', [
            'stripe_token' => $request->input('stripeToken'),
            'amount' =>  $amount,
            'description' => 'Regular Payment',
            'member_email_address' => $request->input('stripeEmail'),
            'save_card' => 1
        ]);

        /**
         * Stripe Status OK is success, ERROR is failure
         */
        if (isset($stripeData->status) || !isset($stripeData->response_payload)) {
            //dd($stripeData);
            if($stripeData->status == "ERROR"){
                if (isset($stripeData->data->message)) {
                    $error = $stripeData->data->message;
                }
                elseif (isset($stripeData->data)) {
                    $error = $stripeData->data;
                }
                else {
                    $error = "An unknown error has occurred";
                    // dd($stripeData);
                }
                if ($request->ajax()) {
                    return \Response::json(['errors'=>['error'=>$error]], 422);
                } else {
                    return redirect()->back()->withErrors($error);
                }
            } elseif($stripeData->status == "OK"){
                $error = "An GET request was made instead of a POST because BMS is not called via https";
                return redirect()->back()->withErrors($error);
            }
        }

        $paymentID = $this->createPaymentRecord($id, $amount,
            $stripeData->response_payload->data->miscellaneous->balance_transaction, $stripeData->response_payload->data->miscellaneous->id);

        //Legacy Calls
        $memberData = $this->buildLegacyMemberData($id);
        $orderData = $this->buildOrderData($id);
        $member_id = $this->makeLegacyPerson($memberData);
        $transactionResponse = (new TransactionLegacyProcess())->process($id, auth()->id(),
            round($request->input("order_amount") / 100, 2), $orderData);
        // Finally done so update interests as done

        $this->updateInterests($id, $paymentID, $amount);

        // check for sub
        foreach ($orderData as $orderDatum) {
            if ($orderDatum['type'] === 2) { // Catch subscription and create
                $this->createMembershipSubscription(round($orderDatum['item_price'] * 100,2),
                    $request->input('stripeEmail'),$orderDatum['stripe_plan_id']);
            }
        }
        $emailData = (new WelcomeEmailService())->sortData($memberData);
        (new WelcomeEmailService())->process($request->input('stripeEmail'),$member_id,$emailData);

        if ($request->ajax()) {
            return \Response::json(['success' => 1], 200);
        } else {
            return route('crm.edit', ['class' => 'lead', 'id' => $id])->with(['success' => 'Payment completed !']);
        }

    }

    private function makeLegacyPerson(array $memberData)
    {
        $lead = Lead::find($memberData['lead_id']);

        if($lead->member_id > 0) // exists so don't
        {
            return $lead->member_id;
        }

        $personResponse = (new PersonService())->post('', $memberData);
        if ($personResponse->success != false) {
            $newID = $personResponse->data;
            Lead::where('id', $memberData['lead_id'])->update(['member_id' => $newID]);
        }

        return $newID;
    }

    /**
     * Create Payment Records
     *
     * @param $leadID
     * @param $amount
     * @param $orderReference
     * @param $orderID
     * @return mixed
     */
    private function createPaymentRecord($leadID, $amount, $orderReference, $orderID)
    {
        try {
            $purchase = new Payment;
            $purchase->lead_id = $leadID;
            $purchase->user_id = auth()->id();
            $purchase->amount = $amount;
            $purchase->paid = 1;
            $purchase->payment_type = 1;
            $purchase->reference = $orderReference;
            $purchase->unique_order_id = $orderID;
            $purchase->save();

            return $purchase->id;
        } catch (QueryException $queryException) {
            die($queryException->getMessage());
        }
    }

    /**
     * Create data array for processing on legacy
     * TODO remove this in 2.0 - should be redundant by then
     *
     * @param $leadID
     * @return array
     */
    private function buildLegacyMemberData($leadID)
    {
        $lead = Lead::find($leadID);

        return [
            'lead_id' => $leadID,
            'first_name' => $lead->first_name,
            'last_name' => $lead->last_name,
            'email' => $lead->email_address,
            'phone_number' => preg_replace('/\D+/', '', $lead->phone_number),
            'network_id' => '1',
            'type' => '1',
            'form_url' => '/',
            'country_id' => '77',
            'password' => strtoupper(str_random(2)) . rand(0, 25) . str_random(6) . "$"
        ]; // No formula
    }

    /**
     * Create order data for processing on legacy
     * TODO remove this in 2.0 - should be redundant by then
     * @param $leadID
     * @return array
     */
    private function buildOrderData($leadID, $fullOrder = true)
    {
        $lead = Lead::find($leadID);
        $interests = ($fullOrder === true) ? $lead->interests->where('in_basket',
            1) : $lead->interests->where('partial_payment', 1);
        $orderData = [];
        foreach ($interests as $interest) {
            array_push($orderData, [
                'item_price' => $interest->calculateCost(),
                'item_id' => $interest->catalogueItem->legacy_id,
                'item_type' => $interest->catalogueItem->legacy_type, // Package or product
                'type' => $interest->catalogueItem->type, //
                'stripe_plan_id' => $interest->catalogueItem->stripe_plan_id,
            ]);
        }
        return $orderData;
    }

    /**
     * |
     * | PART PAYMENTS
     * |
     */

    /** --------------------------------------|
     * Entry Point 2 -- Part Payment
     * Process part payment via stripe, take an initial amount up front the customer but call the
     * updateInterestsPartial().
     *
     * Unlike the full payment method, at the end of the payment check to see if there is any outstanding interests left over,
     * if there are none outstanding then assume the payment is fully complete and proceed to make the memebership.
     *
     * @param $id
     * @param Request $request
     * ---------------------------------------|
     */
    public function processPartPayment($id, Request $request)
    {
        $request->validate([
            'stripeEmail' => 'required',
            'stripeToken' => 'required',
            'order_amount' => 'required',
        ]);

        //pennies checking
        $amount = (strpos($request->input('order_amount'), ".") === true) ? str_replace(".", "",
            $request->input('order_amount')) : $request->input('order_amount');

        $stripeData = (new RegularPayment())->post('', [
            'stripe_token' => $request->input('stripeToken'),
            'amount' =>  round($request->input("order_amount") / 100, 2),
            'description' => 'Partial Payment',
            'member_email_address' => $request->input('stripeEmail'),
            'save_card' => 1
        ]);

        if(isset($stripeData->status)){
            if ($stripeData->status === "ERROR") {
                return redirect()->back()->withErrors($stripeData->data->errors);
            }
        }

        try {

            $purchase = new Payment;
            $purchase->lead_id = $id;
            $purchase->user_id = auth()->id();
            $purchase->amount = round($request->input("order_amount") / 100, 2);
            $purchase->paid = 1;
            $purchase->payment_type = 2;
            $purchase->reference = $stripeData->response_payload->data->miscellaneous->balance_transaction;
            $purchase->unique_order_id = $stripeData->response_payload->data->miscellaneous->id;
            $purchase->save();
        } catch (QueryException $queryException) {
            die($queryException->getMessage());
        }

        $lead = Lead::find($id);
        $interests = $lead->interests->where('in_basket', 1)->where('outstanding_value', '<>', '0.00');
        if(count($interests)){
            $this->updateInterestsPartial(round($request->input("order_amount") / 100, 2), $id, $purchase->id, "existing");
        } else {
            $this->updateInterestsPartial(round($request->input("order_amount") / 100, 2), $id, $purchase->id);
        }

        $Lead = Lead::find($id);
        if ($Lead->hasOutstandingPayments() === false) {
             $memberData = $this->buildLegacyMemberData($id);
            $orderData = $this->buildOrderData($id,
                false); // <---- Pass false in so the order looks for partial payments even if they've left the basket
            $OrderTotal = 0;
            foreach ($orderData as $orderDatum) {
                $OrderTotal += $orderDatum['item_price'];
                if ($orderDatum['type'] === 2) { // Catch subscription and create
                    $this->createMembershipSubscription($orderDatum['item_price'],
                        $request->input('stripeEmail'),$orderDatum['stripe_plan_id']);
                }
            }
            $member_id = $this->makeLegacyPerson($memberData);
            $transactionResponse = (new TransactionLegacyProcess())->process($id, auth()->id(), $OrderTotal,
                $orderData);

            Interest::where('lead_id', $id)->where('partial_payment',
                1)->update(['partial_payment' => 0, 'in_basket' => 0 , 'sold' => 1]); // finish off the whole thing and remove partial payment flag so it's not included in the future

            $emailData = (new WelcomeEmailService())->sortData($memberData);
            (new WelcomeEmailService())->process($request->input('stripeEmail'),$member_id,$emailData);
        }


        return redirect()->back()->with(['Success' => 'Partial payment successfully taken, please ensure you follow up with the remaining amount']);


    }

    /** --------------------------------------|
     * Entry Point 3 -- Defer payment
     * Pass in 0 as the amount, don't call payments API and run through function @updatePartial Interests
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     *----------------------------------------|
     */
    public function defferPayment($id)
    {
        $amount = 0;
        // straight to payment to set up 0 paid and outstanding to full
        $paymentID = $this->createPaymentRecord($id, $amount,
            "Deferred_Payment_Ref" . $id . "_" . auth()->id() . "_" . time(),
            "Deferred_Payment_Id" . $id . "_" . auth()->id() . "_" . time());
        $this->updateInterestsPartial($amount, $id, $paymentID);
        return redirect("/crm/edit/lead/" . $id)->with(['success' => 'Deferred Payment Set Up, Balance now Outstanding']);

    }

    /**
    -     * Update partial payment interests || TODO make this function cleaner / efficient
    -     *
    -     * for each item/s in the order attempt to add the full amount of payment
    -     * against the agreed upon amount
    -     *
    -     * if the initial / remaining amount of money from the payment is less than the
    -     * current item/s then associate whats left and calculate the outstanding balance
    -     * and assign it to that interest in the basket.
    -     *
    -     * 2 cases, fresh (first time running) && existing (already has partial payment against it)
    +     * Update payment interests where the payment was taken partially or delayed
     */

    public function updateInterestsPartial($amount, $leadID, $paymentID, $type = "fresh")
    {
        $lead = Lead::find($leadID);
        $interests = $lead->interests->where('in_basket', 1);
        $count = $lead->interests->where('in_basket', 1)->count();

        $amountAvailable = $amount;
        switch ($type) {
            case "fresh":
                foreach ($interests as $interest) {
                    if ($amountAvailable - $interest->calculateCost(true, null, true) > 0) {
                        // funds available to sell
                        $interest->sold = 1;
                        $interest->sale_value = $interest->calculateCost(true, null, true);
                        $interest->payment_id = $paymentID;
                        $interest->sold_on = date("Y-m-d H:i:s");
                        $interest->in_basket = 0;
                        $interest->partial_payment = 1;
                        $interest->outstanding_value = null;
                        $interest->save();
                        // Update amount
                        $amountAvailable = round($amountAvailable - $interest->calculateCost(), 2);
                    } else {
                        $outstanding = round($interest->calculateCost(true, null, true) - $amountAvailable, 2);
                        // insufficient funds
                        $interest->sold = 0;
                        // We don't need to update sales value
                        //$interest->sale_value = $amountAvailable;
                        $interest->payment_id = $paymentID;
                        $interest->sold_on = date("Y-m-d H:i:s");
                        $interest->in_basket = 1;
                        $interest->partial_payment = 1;
                        $interest->outstanding_value = $outstanding;
                        $interest->save();
                        $amountAvailable = 0;
                    }
                }
                break;
            case "existing":
                if($count > 1) {
                    foreach ($interests as $interest) {
                        if ($amountAvailable - $interest->outstanding_value >= 0) {
                            // funds available to sell
                            // We don't need to modify sale value
                            //$interest->sale_value = $interest->sale_value + $interest->oustanding_value;
                            $interest->sold_on = date("Y-m-d H:i:s");
                            $interest->in_basket = 0;
                            $interest->sold = 1;
                            $interest->outstanding_value = 0;
                            $interest->partial_payment = 0;
                            $interest->payment_id = $paymentID;
                            $interest->save();
                            // Update amount
                            // Removed, this function does not return amount
                            //$amountAvailable = round($amountAvailable - $interest->outstanding_value, 2);
                        } else {
                            // insufficient funds
                            $interest->sold = 0;
                            // Don't update sale value
                            //$interest->sale_value = ($interest->sale_value + $amountAvailable);
                            $interest->sold_on = date("Y-m-d H:i:s");
                            $interest->in_basket = 1;
                            $interest->partial_payment = 1;
                            $interest->outstanding_value = $interest->outstanding_value - $amountAvailable;
                            $interest->save();

                            // Removed, this function does not return amount
                            //$amountAvailable = 0;
                        }
                    }
                } else {
                    //Correct but fails on multiple interests in the basket
                    $interest = $lead->interests->where('in_basket', 1)
                        ->where('outstanding_value', $amountAvailable)
                        ->first();

                    if ((intval($interest->outstanding_value) - $amountAvailable) == 0) {

                        $interest->sold_on = date("Y-m-d H:i:s");
                        $interest->in_basket = 0;
                        $interest->sold = 1;
                        $interest->outstanding_value = 0;
                        // partial should be probably 0 in the case of full payment
                        $interest->partial_payment = 0;
                        $interest->payment_id = $paymentID;
                        $interest->save();

                    } elseif($amountAvailable > $interest->outstanding_value) {

                        return redirect()->back()->withErrors('Funds entered exceed the outstanding value');

                    } elseif($amountAvailable < $interest->outstanding_value){
                        $interest->sold = 0;
                        $interest->sold_on = date("Y-m-d H:i:s");
                        $interest->in_basket = 1;
                        $interest->partial_payment = 1;
                        $interest->outstanding_value = $interest->outstanding_value - $amountAvailable;
                        $interest->save();
                    }
                }

                break;
        }

    }

    /**
     * Update payment interests where the payment was taken partially or delayed
     */
    public function updateInterestsPartialOutstanding($amount, $leadID, $paymentID = null, $type = "fresh", $interestID = null)
    {
        $lead = Lead::find($leadID);
        if($interestID) {
            $interest = $lead->interests;

            $interest = $lead->interests->where('id', $interestID)
                ->where('in_basket', 1)
                ->where('outstanding_value', '<>', 'NULL')
                ->where('outstanding_value', '<>', '0.00')
                ->first();

            if(!$interest){
                return redirect()->back()->withErrors('Record not found, please refresh the page.');
            }
        } else {
            $interests = $lead->interests->where(
                ['in_basket', '=', '1'],
                //['payment_id', '=', $paymentID],
                ['outstanding_value', '<>', 'NULL'],
                ['outstanding_value', '<>', '0.00']
            );

            if(!$interests){
                return redirect()->back()->withErrors('Records not found, please refresh the page.');
            }
        }

        $amountAvailable = $amount;
        switch ($type) {
            // First case needs testing & updating when we get to it
            case "fresh":
                foreach($interests as $interest) {
                    if ($amountAvailable - $interest->calculateCost(false, null, true) > 0) {
                        // funds available to sell
                        $interest->sold = 1;
                        $interest->sale_value = $interest->calculateCost(false, null, true);
                        $interest->payment_id = $paymentID;
                        $interest->sold_on = date("Y-m-d H:i:s");
                        $interest->in_basket = 0;
                        $interest->partial_payment = 1;
                        $interest->outstanding_value = null;
                        $interest->save();
                        // Update amount
                        $amountAvailable = round($amountAvailable - $interest->calculateCost(), 2);
                    } else {
                        $outstanding = round($interest->calculateCost(false, null, true) - $amountAvailable, 2);
                        // insufficient funds
                        $interest->sold = 0;
                        $interest->sale_value = $amountAvailable;
                        $interest->payment_id = $paymentID;
                        $interest->sold_on = date("Y-m-d H:i:s");
                        $interest->in_basket = 1;
                        $interest->partial_payment = 1;
                        $interest->outstanding_value = $outstanding;
                        $interest->save();

                        $amountAvailable = 0;
                    }
                }
                break;
            case "existing":

                if ((intval($interest->outstanding_value) - $amountAvailable) == 0) {
                    $interest->sold_on = date("Y-m-d H:i:s");
                    $interest->in_basket = 0;
                    $interest->sold = 1;
                    $interest->outstanding_value = 0;
                    // partial should be probably 0 in the case of full payment
                    $interest->partial_payment = 0;
                    $interest->payment_id = $paymentID;
                    $interest->save();

                } elseif($amountAvailable > $interest->outstanding_value) {
                    // funds exceeding outstanding value
                    /*$interest->sold = 0;
                    $interest->sold_on = date("Y-m-d H:i:s");
                    $interest->in_basket = 1;
                    $interest->partial_payment = 1;
                    $interest->save();

                    $amountAvailable = 0;*/

                    return redirect()->back()->withErrors('Funds entered exceed the outstanding value');

                } elseif($amountAvailable < $interest->outstanding_value){
                    $interest->sold = 0;
                    $interest->sold_on = date("Y-m-d H:i:s");
                    $interest->in_basket = 1;
                    $interest->partial_payment = 1;
                    $interest->outstanding_value = $interest->outstanding_value - $amountAvailable;
                    $interest->save();
                }
                break;
        }

    }


    /** Create new subscription for monthly membership to be taken next month
     *  TODO replace with generic function for all subs
     * @param $amount
     * @param $token
     * @param $email
     */
    private function createMembershipSubscription($amount, $email,$planID)
    {
        $stripeData = (new SubscriptionPayment())->post('subscriptions', [
            'amount' => $amount,
            'description' => 'NewCo. Monthly Subscription',
            'member_email_address' => $email,
            'starts_at' => Carbon::now()->addMonth()->timestamp,
            'plan_id' => $planID,
        ]);


        if ($stripeData->status === "ERROR") {
            return redirect()->back()->withErrors($stripeData->data->errors);
        }
    }

    /**
     * Update all interests in basket with payment id
     * unset in_basket as they've paid
     * @param $leadID
     * @param $paymentID
     */
    private function updateInterests($leadID, $paymentID, $amount)
    {
        $lead = Lead::find($leadID);
        $interests = $lead->interests->where('in_basket', 1);
        //TODO refactor with ammaar to avoid this
        foreach ($interests as $interest) {
            $interest->sold = 1;
            // Sale vale does not need to change
            //$interest->sale_value = $interest->calculateCost();

            /**
             * CG: I don't understand this. updateInterests seems to be used to process
             * a FULL payment. However, we're looping through all interests here, so why
             * would we subtract the total amount paid, off every interest...? Won't that
             * potentially give us negative outstanding_value values?
             */

            $interest->outstanding_value = $interest->sale_value - $amount;
            $interest->payment_id = $paymentID;
            $interest->sold_on = date("Y-m-d H:i:s");
            $interest->in_basket = 0;
            $interest->save();
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     *  Overwrite payment record.
     */
    public function outstandingPaymentUpdate(Request $request)
    {
        $lead = Lead::find($request->input("lead_id"));

        if ($request->input('amount') > $lead->calculateCost(false, true)) {
            return redirect()->back()->with(['error' => 'Payment amount is greater than outstanding balance, over-charged?']);
        }
        // Generate new payment

        $payment = new Payment;
        $payment->lead_id = $lead->id;
        $payment->user_id = $request->input("user_id");
        $payment->amount = $request->input("amount");
        $payment->paid = 1;
        $payment->payment_type = $request->input("payment_method");
        $payment->reference = "Additional_Payment_" . date('Y_m_d_H_i_s', time()); // request doesnt return reference
        $payment->unique_order_id = "Additional_Payment_" . date('Y_m_d_H_i_s', time());
        $payment->save();

        $this->updateInterestsPartialOutstanding($request->input("amount"), $lead->id, $payment->id, "existing", $request->input("interest_id"));

        return redirect()->back()->with(['success' => 'Payment successfully recorded']);

    }

}

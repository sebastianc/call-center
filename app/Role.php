<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{

    protected $fillable = ['title','uri'];

    public function users() {
        return $this->hasMany('App\User'); // implies role_user, role_id, user_id as additional args
    }

    /**
     * This function is superseded by users() above
     * but left in place for reference, as a learning exercise
     * Note that roles->users is a straight hasMany relationship
     * and not a hasManyThrough (which would imply an intermediate
     * model, which we don't have or need in this case)
     *
     * @return void
     */
    public function staff()
    {
        /**
         *  Try explain for future reference, bad naming conventions aside.
         *
         *  Roles --> StaffMembersRoles --> StaffMembers
         *
         *  Provides a relationship from a roles perspective to bypass StaffMembersRoles to get staff
         */
        return $this->hasManyThrough(
            'App\User', // Table your after (Staff Members)
            'App\UserRoles', // Joining table to pass through (Staff Members Roles)
            'role_id', // key on the through table reference starting table (Staff Members Roles . role_id )
            'id', // key on table trying to connect to (Staff Member .id)
            'id', // local key on the base table (Roles .id)
            'user_id' // key connects from through table to end table (Staff Members Roles . staff_id )

        );
    }
}

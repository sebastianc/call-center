<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Storage;
use Lab404\Impersonate\Models\Impersonate;
use \App\Events\UserCreated;

use Cache;
use Gravatar;

class User extends Authenticatable
{
    use Notifiable;
    use Impersonate;

    protected $dispatchesEvents = [
        'created' => UserCreated::class
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public function roles()
    {
        return $this->belongsToMany('App\Role'); // implies role_user, user_id, role_id as additional args
    }

    public function allocations()
    {
        return $this->hasMany("App\Allocation");
    }

    /**
     * Return the teams to which this user belongs
     */
    public function teams()
    {
        return $this->belongsToMany('App\Team');
    }

    /**
     * Return all this user's leads
     */
    public function leads()
    {
        return $this->hasMany('App\Lead');
    }

    public function payments()
    {
        return $this->hasMany("App\Payment");
    }

    /**
     * Return a user's colleagues (eg, all users on the same team as this user)
     * @param boolean $exclude_self Should we return the current user in this list?
     *
     * @return void
     */
    public function colleagues($exclude_self = true)
    {
        $this->load('teams.users');
        $colleagues = $this->teams->pluck('users')->unique()->collapse();
        if ($exclude_self) {
            $colleagues = $colleagues->filter(function ($item) {
                /*
                * return $item->id != \Auth::user()->id;
                */
                return $item->id != $this->id;
            });
        }
        return $colleagues;
    }

    public function hasRole($role_uri) {
        $roles = $this->roles()->pluck('uri');
        return $roles->contains($role_uri);
    }

    public function isAccountManager() {
        return $this->hasRole('account-manager');
    }

    public function isTeamLeader() {
        return $this->hasRole('team-leader');
    }

    public function onRenewalsTeam()
    {
        return $this->hasRole('renewals-manager');
    }

    public function onEngagementTeam()
    {
        return $this->hasRole('product-manager');
    }

    /**
     * Returns an avatar for this user
     *
     * @return string
     */
    public function getAvatar()
    {
        return Storage::disk("s3")->url($this->image);
    }

    /**
     * Get the initials of this user
     *
     * @return string
     */
    public function getInitials() {
        $names = \explode(' ',$this->name);
        $initials = array_map(function ($n) {
            return $n[0];
        }, $names);
        return \implode($initials);
    }

    public function hasOverdueCallbacks()
    {
        $earliestCallback = $this
                            ->leads()
                            ->whereNotNull('cached_callback_time')
                            ->orderBy('cached_callback_time', 'ASC')
                            ->first();

        if (is_null($earliestCallback)) {
            /**
             * The user has no callbacks set, so we can't be said to have any that are overdue
             */
            return false;
        }
        else {
            $earliestCallbackTime = $earliestCallback->cached_callback_time;
            return Callback::timeIsOverdue($earliestCallbackTime);
        }
    }

}

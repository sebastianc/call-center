<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NoteType extends Model
{
    protected $table = "note_type";
    protected $primaryKey = "id";

    protected $fillable = ['title'];
    protected $guarded = ['created_at','updated_at'];
    protected $hidden = [];
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

use DB;
use Auth;
use Log;
use Carbon\Carbon;

class Lead extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    protected $fillable = ['first_name','last_name','email_address','phone_number','team_id','user_id','renewal_manager_id','callback','gdpr_deleted_at','lead_status_id', 'created_at'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function surveys()
    {
        return $this->hasMany('App\Survey');
    }

    public function notes()
    {
        return $this->hasMany("App\Note")->orderBy('created_at', 'desc');
    }

    public function interests()
    {
        return $this->hasMany("App\Interest");
    }

    public function calls()
    {
        return$this->hasMany("App\Call")->orderBy('created_at', 'desc');
    }

    public function sms()
    {
        return$this->hasMany("App\Sms")->orderBy('created_at', 'desc');
    }

    public function callback($id = null)
    {
        if ($id) {
            $callback = $this->callbacks()->whereNull('deleted_at')->orderBy("callback_time", "asc")->find($id);
        } else {
            $callback = $this->callbacks()->whereNull('deleted_at')->orderBy("callback_time", "asc")->first();
        }

        return $callback;
    }

    public function leadstatus()
    {
        return $this->belongsTo('App\LeadStatus', 'lead_status_id', 'id');
    }

    public function callbacks()
    {
        return $this->hasMany("App\Callback");
    }

    public function sources()
    {
        return $this->hasMany("App\LeadSource");
    }

    /**
     * Work out the value of all outstandig interests for this lead if we want to pay for it
     *
     * @param boolean $with_discount Are we applying the discount?
     *
     * @return void
     */
    public function calculateCost($with_discount = true, $plus_vat = false)
    {
        $cost = 0;
        $interests_in_basket = $this->interests()->where('in_basket', 1)->get();
        foreach ($interests_in_basket as $interest) {
            $cost += $interest->calculateCost($with_discount, null, $plus_vat);
        }
        return $cost;
    }

    public function calculateCostOutstanding()
    {
        $cost = 0;
        $interests_in_basket = $this->interests()->where('in_basket', 1)
                                                 ->where('outstanding_value', '>', 0)
                                                 ->get();
        foreach ($interests_in_basket as $interest) {
            $cost += $interest->outstanding_value;
        }
        return $cost;
    }

    public function isMobileNumber()
    {
        if (strpos($this->phone_number, "+447")
            !== false ||
            substr($this->phone_number, 0, 2) == "07") {
            return true;
        }

        return false;
    }

    public function isUkMobile()
    {
        $regex = '/^(((\+44\s?\d{4}|\(?0\d{4}\)?)\s?\d{3}\s?\d{3})|((\+44\s?\d{3}|\(?0\d{3}\)?)\s?\d{3}\s?\d{4})|((\+44\s?\d{2}|\(?0\d{2}\)?)\s?\d{4}\s?\d{4}))(\s?\#(\d{4}|\d{3}))?$/';
        return (preg_match($regex, $this->phone_number));
    }

    public function deleteOnRequest()
    {
        $this->first_name = strtoupper($this->first_name[0]);
        $this->last_name = strtoupper($this->last_name[0]);
        $this->email_address = "ErasureGDPR";
        $this->phone_number = "ErasureGDPR";
        $this->team_id = 0;
        $this->user_id = 0;
        $this->renewal_manager_id = 0;
        $this->callback = null;
        $this->gdpr_deleted_at = date("Y-m-d H:i:s");
        $note = new Note;
        $note->text = "Right to be forgotten / GDPR Erasure Requested @ ".date("Y-m-d H:i:s");
        $note->note_type_id = 1;
        $note->user_id = 0;
        $note->lead_id = $this->id;
        $note->interest_id = 0;
        $note->save();
        $this->save();
    }

    /**
     * Do we have unread inbound SMS messages forr this lead?
     *
     * @return integer The number of unread SMS
     */
    public function hasUnreadSMS()
    {
        if ($this->getUnreadSMSCount() > 0) {
            return true;
        }
        else {
            return false;
        }
    }

    /**
     * How many unread SMS do we have for this lead?
     *
     * @return integer The number of unread SMS
     */
    public function getUnreadSMSCount()
    {
        $unread = DB::table('sms')->where([
            ['lead_id', '=', $this->id],
            ['inbound', '=', 1],
            ['read', '=', 0]
        ])->count();
        return $unread;
    }

    /**
     * Is this lead visible to the current user (or, the user with the ID $user_id)?
     *
     * @param integer $user_id
     * @return boolean
     */
    public function isVisibleTo($user_id = 0) {

         /**
         * When can a user view another lead?
         * - When they're a normal user and it's "their" lead
         * - When they're a Team Leader
         * - When they're a CA and this lead belongs to someone in their team
         * - When the lead is in the open pot
         */

        if ($user_id) {
            $user = User::find($user_id);
        } else {
            $user = Auth::user();
        }

        if ($this->user_id == $user->id) { // When they're a normal user and it's "their" lead
            return true;
        } else if ($user->isTeamLeader()) { // When they're a Team Leader
            return true;
        } else {
            if ($user->isAccountManager()) { // When they're a CA and this lead belongs to someone in their team
                $colleague_ids = $user->colleagues()->pluck('id')->toArray();
                if (in_array($this->user_id, $colleague_ids)) {
                    /* change back to true if AC's need to be able to see team member leads*/
                    return false;
                }
            }
            if ($this->isInOpenPot()) { // When the lead is in the open pot
                return true;
            }
        }

        return false;
    }

    public function hasOutstandingPayments()
    {
        $outstandings = $this->interests()->where('outstanding_value', '>', 0)->count();

        return ($outstandings > 0) ? true : false;
    }

    /**
     * Is this lead in the Open Pot?
     * TODO: can we use this to replace the filterLeadsForOpenPot() method of CRMController?
     * I'm not sure we can, but we define the open pot in about three places now, which needs
     * to be sorted out.
     *
     * When is a lead in the Open Pot? This needs to be defined. Currently:
     * - When it hasn't been assigned to anyone
     * - When it is assigned to someone but has a callback more than a week overdue
     * - When it is assigned to someone, but hasn't been updated for a week?
     *
     * @return void
     */
    public function isInOpenPot()
    {
        $overdue_date = \Carbon\Carbon::now()->subDays(7)->format('Y-m-d H:i:s');
        if (is_null($this->user_id)) {
            return true;
        }
        else if (!is_null($this->cached_callback_time) && $this->cached_callback_time <= $overdue_date) {
            return true;
        }
        else if ($this->updated_at <= $overdue_date) {
            return true;
        }
        return false;
    }

    public function isInOpenPotNew() {
       //  return Lead::where('id', $lead_id)->filter
    }

    /**
     * Filter the Lead object to only show leads in the Engagement Pot. Rules:
     * - The lead must have made a valid purchase of a product or membership, more than three weeks ago
     * - There must be no callback set on the lead.
     *
     * @return void
     */
    public static function filterForEngagementPot()
    {
        /**
         * DataTables needs with() to correctly display related data
         */
        $query = self::with(['user','leadstatus'])->select('leads.*');

        /**
         * We only include leads who made a purchase more than three weeks ago
         */
        $date = \Carbon\Carbon::now()->subWeeks(3)->format('Y-m-d H:i:s');
        $query->whereHas('interests', function ($q) use ($date) {
            $q->where('sold', 1);
            $q->where('sold_on', '<=', $date);
        });

        /**
         * We exclude any leads with a callback set
         */
        $query->doesntHave('callbacks');

        /**
         * TODO: how do we exclude leads in the "Renewals" pot? Should we?
         */

        return $query;
    }

    /**
     * Filter the Lead object to only show leads in the Renewals Pot. Rules:
     * - all leads who have purchased anything in the past
     * - and are either due to renew soon, or have since expired.
     *
     * @return void
     */
    public static function filterForRenewalsPot()
    {
        /**
         * If an interest has been purchased in the past, and is due to expire in x days
         * then consider this lead eligible for renewal
         */
        $renewal_date = Carbon::now()->addDays(Interest::$days_until_renewal)->format('Y-m-d H:i:s');

        /**
         * DataTables needs with() to correctly display related data
         */
        $query = self::with(['user','leadstatus'])->select('leads.*');

        /**
         * We only include leads who made a purchase
         */
        $query->whereHas('interests', function ($q) use ($renewal_date) {
            $q->where('sold', 1);
            $q->where('expiry_date', '<=', $renewal_date);
        });

       /**
        * We need to include leads who are due to renew soon, or who have since expired
        * TODO: how do we access this information, given that only the portal knows this?
        */

        /**
         * TODO: how do we exclude leads in the "Engagement" pot? Should we?
         */

        return $query;
    }

    /**
     * Filter the Lead object to only show leads in the Open Pot. Rules:
    *
    * It isn’t assigned to anyone (but isn’t a brand new lead)
    * OR It doesn’t have a callback set, and hasn't been updated for two working days
    * OR it has a callback set that is more than two working days overdue
    * Only leads that have never purchased anything will appear in the Open Pot.
     *
     * @return void
     */
    public static function filterForOpenPot()
    {
        /**
         * DataTables needs with() to correctly display related data
         */
        $query = self::with(['user','leadstatus'])->select('leads.*');

        $overdue_date = Carbon::now()->subDays(2)->format('Y-m-d H:i:s');

        /**
         *
         */
        $query->where(function ($query) use ($overdue_date) {
            /**
             * It isn’t assigned to anyone (but isn’t a brand new lead)
             */
            $query->whereNull('user_id');
            /**
             * OR it has a callback set that is more than two working days overdue
             * Following ITBOV-128, we can now use the cached value here:
             */
            $query->orWhere(function ($query) use ($overdue_date) {
                $query->where('leads.cached_callback_time', '<=', $overdue_date)
                    ->orWhereNull('leads.cached_callback_time');
            });

            /**
             * OR It doesn’t have a callback set, and hasn't been updated for two working days
             */
            $query->orWhere(function ($query) use ($overdue_date) {
                $query->where('leads.updated_at', '<=', $overdue_date)
                    ->whereNull('leads.cached_callback_time');
            });

            /**
             * TODO: ignore "brand new" leads. How do we identify these?
            */
        });

        /**
         * Only leads that have never purchased anything will appear in the Open Pot.
         */
        $query->whereDoesntHave('interests', function ($query) {
            $query->where('sold', 1);
        });

        return $query;
    }

}
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CatalogueItemType extends Model
{
    protected $table = "catalogue_item_types";
    protected $primaryKey = "id";

    protected $fillable = ['title'];
    protected $guarded = ['created_at','updated_at'];
    protected $hidden = [];
}

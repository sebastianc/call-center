<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LeadStatus extends Model
{
    protected $table = 'lead_status';
    protected $fillable = ['title','category'];

    public function leads()
    {
        return $this->hasMany('App\Lead');
    }

}

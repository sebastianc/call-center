<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use \App\Events\CallSaved;

use Carbon;

class Call extends Model
{
    protected $fillable = ['user_id','lead_id','pitched','answered','created_at', 'ended_at', 'answered_at'];

    protected $dispatchesEvents = [
        'saved' => CallSaved::class
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function lead()
    {
        return $this->belongsTo('App\Lead');
    }

    /**
     * Return the duration of this call in hours and minutes and seconds, if less than an hour only show minutes
     * if less than minute only show seconds
     *
     * @return void
     */
    public function getDurationDescription()
    {
        if (!$this->answered_at) {
            return "Unknown";
        } else {
            if (!$this->ended_at) {
                return false;
            } else {
                $start = Carbon\Carbon::createFromTimestamp(strtotime($this->answered_at));
                $end = Carbon\Carbon::createFromTimestamp(strtotime($this->ended_at));
                /*$duration_in_minutes = $start->diffInMinutes($end);*/

                $difference = $start->diff($end)->format('%h hours, %i minutes and %s seconds');
                $minDiff = $start->diff($end)->format('%i minutes and %s seconds');
                $secDiff = $start->diff($end)->format('%s seconds');

                if ($start->diffInHours($end) > 0) {
                    return $difference;
                } elseif ($start->diffInMinutes($end) > 0) {
                    return $minDiff;
                } else {
                    return $secDiff;
                }
            }
        }
    }

}

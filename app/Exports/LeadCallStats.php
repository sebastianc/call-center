<?php
/**
 * Created by PhpStorm.
 * User: robertlemm
 * Date: 20/07/2018
 * Time: 09:52
 */

namespace App\Exports;

use App\Lead;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;


class LeadCallStats implements FromCollection, WithMapping, WithHeadings
{
    use Exportable;

    private $startTime;
    private $endTime;

    public function __construct($start, $end)
    {
        $this->startTime = $start;
        $this->endTime = $end;
    }

    /**
     * @return Collection
     */
    public function collection()
    {
        return Lead::whereHas('calls',function ($query){
            $query->where('created_at', '>=', $this->startTime)->where('created_at', '<=', $this->endTime);
        })->get();
    }

    public function headings(): array
    {
        return [
            'Unique ID',
            'First Name',
            'Last Name',
            'Calls',
            'Answers',
            'Pitches',

        ];
    }

    /**
     * @param mixed $row
     *
     * @return array
     */
    public function map($lead): array
    {
        return [
            $lead->id,
            $lead->first_name,
            $lead->last_name,
            count($lead->calls->where('created_at',">",$this->startTime)->where('created_at',"<",$this->endTime)) + 0,
            count($lead->calls->where('created_at',">",$this->startTime)->where('created_at',"<",$this->endTime)->where('answered', 1)) + 0,
            count($lead->calls->where('created_at',">",$this->startTime)->where('created_at',"<",$this->endTime)->where('pitched', 1)) + 0,
        ];
    }
}
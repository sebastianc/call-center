<?php
/**
 * Created by PhpStorm.
 * User: robertlemm
 * Date: 10/07/2018
 * Time: 10:26
 */

namespace App\Exports;

use App\Lead;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;


class LeadExport implements FromCollection, WithMapping, WithHeadings
{
    use Exportable;

    private $leadID;

    public function __construct($lead_id)
    {
        $this->leadID = $lead_id;
    }

    public function headings(): array
    {
        return [
            'Unique ID',
            'First Name',
            'Last Name',
            'Email Address',
            'Telephone Number',
            'First Recorded',
            'Last Update',

        ];
    }

    public function map($lead): array
    {
        return [
            $lead->id,
            $lead->first_name,
            $lead->last_name,
            $lead->email_address,
            $lead->phone_number,
            $lead->created_at,
            $lead->updated_at,
        ];
    }

    public function collection()
    {
        return Lead::where('id', $this->leadID)->get();
    }
}
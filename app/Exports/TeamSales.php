<?php

namespace App\Exports;

use App\Interest;
use App\Team;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;

class TeamSales implements FromCollection, WithMapping, WithHeadings
{

    private $teamID;

    use Exportable;

    public function __construct($teamID)
    {
        $this->teamID = $teamID;
    }

    /**
     * Build the eloquent queries in this function
     *
     * @return Collection
     */
    public function collection()
    {
        return Interest::whereNotNull('payment_id')->where('sold','=',1)->whereNull('outstanding_value')->whereHas('payment',function ($query){
            $query->whereIn('payments.user_id',Team::find($this->teamID)->users()->pluck('users.id'));
        })->get();
    }

    /**
     * Setup the array of the headings which are put into the excel file
     *
     * @return array
     */
    public function headings(): array
    {
        return [
            'Lead ID',
            'First Name',
            'Last Name',
            'Staff Member',
            'Iteam Bought',
            'Sale value',
            'Sold On'
        ];
    }

    /**
     *  Put in logic for the rows which will output, length of array must be the same as @headings
     *
     * @param mixed $row <--- Name of the object from the collection e.g. lead , user etc
     *
     * @return array
     */
    public function map($row): array
    {
        return [
            $row->lead_id,
            $row->lead->first_name,
            $row->lead->last_name,
            $row->payment->user->name,
            $row->catalogueItem->title,
            number_format($row->sale_value,2),
            $row->sold_on
        ];
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: robertlemm
 * Date: 23/07/2018
 * Time: 07:38
 */

namespace App\Exports;

use App\Lead;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;

class LeadSource implements FromCollection, WithMapping, WithHeadings
{
    use Exportable;

    private $searchLength;

    public function __construct($length = 6)
    {
        $this->searchLength = $length;
    }

    public function headings(): array
    {
        return [
            'Unique ID',
            'First Name',
            'Last Name',
            'Email Address',
            'Telephone Number',
            'First Recorded',
            'Last Update',
            'Incoming Source'

        ];
    }

    public function map($lead): array
    {
        $source = 'N/A';
        if ($lead->sources()->count() > 0) {
            $source = $lead->sources()->orderBy('created_at', 'desc')->first()->lead_source;
        }

        return [
            $lead->id,
            $lead->first_name,
            $lead->last_name,
            $lead->email_address,
            $lead->phone_number,
            $lead->created_at,
            $lead->updated_at,
            $source,
        ];
    }

    public function collection()
    {
        return Lead::where('created_at', ">=",
            Carbon::now()->subMonths($this->searchLength)); // Return only past 6 months
    }
}
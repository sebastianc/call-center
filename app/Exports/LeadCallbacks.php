<?php
/**
 * Created by PhpStorm.
 * User: robertlemm
 * Date: 20/07/2018
 * Time: 10:58
 */

namespace App\Exports;

use App\Lead;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;


class LeadCallbacks implements FromCollection, WithMapping, WithHeadings
{
    use Exportable;

    /**
     * @return Collection
     */
    public function collection()
    {
        return Lead::whereHas('callbacks',function ($query){
            $query->where('callback_time','>',Carbon::now()->toDateTimeString())->whereNull('deleted_at');
        })->get();
    }

    /**
     * @return array
     */
    public function headings(): array
    {
        return [
            'Lead ID',
            'First Name',
            'Last Name',
            'Phone Number',
            'Callback',
            'Callback Type'
        ];
    }

    /**
     * @param mixed $row
     *
     * @return array
     */
    public function map($lead): array
    {
        $type = "Callback";
        if($lead->callback()->is_priority == 1 && $lead->callback()->is_qualified == 0 ) {$type = "Priority Callback";}
        if($lead->callback()->is_qualified == 1 && $lead->callback()->is_priority == 0 ) {$type = "QA Callback";}
        if($lead->callback()->is_qualified == 1 && $lead->callback()->is_priority == 1) {$type = "Priority QA Callback";}

        return [
            $lead->id,
            $lead->first_name,
            $lead->last_name,
            $lead->phone_number,
            $lead->callback()->callback_time,
            $type
        ];
    }
}
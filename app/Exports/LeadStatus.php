<?php
/**
 * Created by PhpStorm.
 * User: robertlemm
 * Date: 20/07/2018
 * Time: 09:16
 */

namespace App\Exports;
use App\Lead;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;

class LeadStatus implements FromCollection, WithMapping, WithHeadings
{
    use Exportable;

    private $startTime;
    private $endTime;

    public function __construct($start , $end )
    {
        $this->startTime = $start;
        $this->endTime = $end;
    }

    /**
     * @return Collection
     */
    public function collection()
    {
        return Lead::whereBetween('created_at',[$this->startTime,$this->endTime])->get();
    }

    /**
     * @return array
     */
    public function headings(): array
    {
        return [
            'Unique ID',
            'First Name',
            'Last Name',
            'Email Address',
            'Telephone Number',
            'Status',
            'Category',

        ];
    }

    /**
     * @param mixed $row
     *
     * @return array
     */
    public function map($lead): array
    {
        return [
            $lead->id,
            $lead->first_name,
            $lead->last_name,
            $lead->email_address,
            $lead->phone_number,
            $lead->leadstatus->title,
            $lead->leadstatus->category,
        ];
    }
}
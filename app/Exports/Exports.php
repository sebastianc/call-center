<?php
/**
 * Created by PhpStorm.
 * User: robertlemm
 * Date: 20/07/2018
 * Time: 11:08
 */

namespace App\Exports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;


class Exports implements FromCollection, WithMapping, WithHeadings
{
    use Exportable;

    /**
     * Build the eloquent queries in this function
     *
     * @return Collection
     */
    public function collection()
    {
        // TODO: Implement collection() method.
        // return
    }

    /**
     * Setup the array of the headings which are put into the excel file
     *
     * @return array
     */
    public function headings(): array
    {
        return [

        ];
    }

    /**
     *  Put in logic for the rows which will output, length of array must be the same as @headings
     *
     * @param mixed $row <--- Name of the object from the collection e.g. lead , user etc
     *
     * @return array
     */
    public function map($row): array
    {
        return [

        ];
    }
}
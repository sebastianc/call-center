<?php

namespace App\Listeners;

use App\Allocation;
use Illuminate\Database\QueryException;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Events\UserCreated as UserCreatedEvent;
use App\Network;

use Log;

class UserCreatedListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object $event
     * @return void
     */
    public function handle(UserCreatedEvent $event)
    {
        // Create Allocation for everything
        $networks = Network::all();

        try {
            foreach ($networks as $network) {
                $allocation = new Allocation;
                $allocation->user_id = $event->user->id;
                $allocation->network_id = $network->id;
                $allocation->allocated = 0;
                $allocation->save();

            }
        } catch (QueryException $queryException) {
        }
    }

    public function failed(UserCreatedEvent $event, $exception)
    {
        Log::debug($exception);
    }
}

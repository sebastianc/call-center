<?php

namespace App\Listeners;

use App\Events\CallbackDeleted;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Artisan;

class CallbackDeletedListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  CallbackDeleted  $event
     * @return void
     */
    public function handle(CallbackDeleted $event)
    {
        /**
         * This could be queued if necessary
         */
        $exitCode = Artisan::call('leads:cache', [
            'id' => $event->callback->lead_id
        ]);

    }
}

<?php

namespace App\Listeners;

use App\Events\CallSaved;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Artisan;

class CallSavedListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  CallSaved  $event
     * @return void
     */
    public function handle(CallSaved $event)
    {
        /**
         * This could be queued if necessary
         */
        $exitCode = Artisan::call('leads:cache', [
            'id' => $event->call->lead_id
        ]);
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $fillable = ['amount','paid','type','reference','unique_order_id'];


    /*
     * Needed for reporting
     */
    public function user()
    {
        return $this->belongsTo("App\User","user_id","id");
    }

    public function interests()
    {
        return $this->hasMany("App\Interest");
    }

    public function lead(){
        return $this->belongsTo('App\Lead','lead_id','id');
    }


}

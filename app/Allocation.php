<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Allocation extends Model
{
    protected $fillable = ['user_id','network_id','allocation','allocated','enabled'];

    public function users()
    {
        return $this->belongsToMany("App\User");
    }

    public function networks()
    {
        return $this->belongsTo("App\Network","network_id");
    }
}

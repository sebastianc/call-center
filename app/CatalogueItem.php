<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CatalogueItem extends Model
{
    use SoftDeletes;
    protected $table = "catalogue_items";
    protected $primaryKey = "id";

    protected $fillable = ['title','type','price'];
    protected $guarded = ['created_at','updated_at'];
    protected $hidden = [];
    protected $dates = ['deleted_at'];

    public function interests()
    {
        //
    }
}

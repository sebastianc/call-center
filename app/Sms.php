<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sms extends Model
{
    protected $fillable = ['message','user_id','lead_id','phone_number','inbound'];


    public function user()
    {
        return $this->belongsTo("App\User");
    }

    public function lead()
    {
        return$this->belongsTo("App\Lead");
    }
}

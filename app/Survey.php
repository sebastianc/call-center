<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use DB;

class Survey extends Model
{
    protected $guarded = ['id'];

    /**
     * Get all possible values of an ENUM (or SET) field
     * from https://stackoverflow.com/a/37602466/1463965
     *
     * @param string $column
     * @return void
     */
    public static function getPossibleEnumValues($column)
    {
        // Create an instance of the model to be able to get the table name
        $instance = new static;

        // Pulls column string from DB
        $enumField = DB::select(DB::raw('SHOW COLUMNS FROM '.$instance->getTable().' WHERE Field = "'.$column.'"'));

        if (!\is_array($enumField) || empty($enumField)) {
            \trigger_error("The column `".$instance->getTable().".$column` is not an ENUM or SET field");
            return [];
        }
        else {
            $enumStr = $enumField[0]->Type;

            // Parse string
            preg_match_all("/'([^']+)'/", $enumStr, $matches);

            // Return matches
            return isset($matches[1]) ? $matches[1] : [];
        }
    }

        /**
     * Return an array of fields and questions that drives the questions on the member survey
     *
     * @return array
     */
    protected function getQuestions()
    {
        return [
            'job_title'                => 'What is your current job title/position?',
            'motivation'               => 'What is your motivation for change?',
            'sector'                   => 'Sector',
            'industries'               => 'Industries',
            'expertise'                => 'Expertise',
            'markets'                  => 'Markets',
            'where_in_career'          => 'Where Would You Say You Are In Your Career?',
            'require_first_role'       => 'When Do You Require Your First Role?',
            'expected_salary'          => 'Expected Salary?',
            'expected_day_rate'        => 'Expected Day Rate?',
            'consecutive_roles'        => 'How Many Consecutive Roles Could You Handle?',
            'money_to_invest'          => 'Do You Have Money You Are Looking to Invest In a Business?',
            'days_per_month'           => 'How Many Days, On Average, Can You Work A Month?',
            'currently_in_role'        => 'Are You Currently In A Role?',
            'vip'                      => 'VIP?',
            'networks_interested_in'   => 'Which Networks Are You Interested In?',
            'type_of_role'             => 'Which Type Of Role Do You Require?',
            'training_and_development' => 'Are You Interested In Training and Development?',
        ];
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: robertlemm
 * Date: 06/08/2018
 * Time: 12:53
 */

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class ValidTelephoneNumberRule implements Rule
{
    private static $messageBird;
    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     *
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $regex = '/^(((\+44\s?\d{4}|\(?0\d{4}\)?)\s?\d{3}\s?\d{3})|((\+44\s?\d{3}|\(?0\d{3}\)?)\s?\d{3}\s?\d{4})|((\+44\s?\d{2}|\(?0\d{2}\)?)\s?\d{4}\s?\d{4}))(\s?\#(\d{4}|\d{3}))?$/';
        return (preg_match($regex,$value));
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'This :attribute isn’t a valid UK phone number';
    }

}
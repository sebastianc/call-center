<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    protected $fillable = ['name'];

    /**
     * Return the users that belong to this team
     */
    public function users()
    {
        return $this->belongsToMany('App\User');
    }

    /**
     * Return the team leader for this team
     * Note that we return the relationship here, so we don't include ->get()
     *
     * @return void
     */
    public function teamLeaders() {
        return $this->users()->whereHas('roles', function ($query) {
            $query->where('uri', 'team-leader');
        });
    }

    public function teamLeader() {
        return $this->teamLeaders()->first();
    }

    /**
     * Return the account managers for this team
     *
     * @return void
     */
    public function accountManagers() {
        return $this->users()->whereHas('roles', function ($query) {
            $query->where('uri', 'account-manager');
        });
    }

    /**
     * Return the product managers for this team
     *
     * @return void
     */
    public function productManagers() {
        return $this->users()->whereHas('roles', function ($query) {
            $query->where('uri', 'product-manager');
        });
    }

    /**
     * Return the membership managers for this team
     *
     * @return void
     */
    public function membershipManagers() {
        return $this->users()->whereHas('roles', function ($query) {
            $query->where('uri', 'membership-manager');
        });
    }

    /**
     * Return the renewals managers for this team
     *
     * @return void
     */
    public function renewalsManagers() {
        return $this->users()->whereHas('roles', function ($query) {
            $query->where('uri', 'renewals-manager');
        });
    }

    /**
     * Return the leads belonging to all users in this team
     * Based on https://stackoverflow.com/a/38143587/1463965
     * Using getLeads() rather than leads() as this isn't an Eloquent relationship
     * Note that we're not currently using this but it's left here for reference
     *
     * @return void
     */
    public function getLeads() {
        $this->load('users.leads');
        $leads = $this->users->pluck('leads')->collapse();
        return $leads;
    }

}

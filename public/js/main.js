$(function () {
    initialiseTooltips();
    initialiseTextareaCounts();
});
function initialiseTooltips() {
    $('[data-toggle="tooltip"]').tooltip()
}
function initialiseTextareaCounts() {
    $('form').on('keyup', 'textarea[maxlength]', function() {
        var max_length = $(this).attr('maxlength');
        var text_length = $(this).val().length;
        var text_remaining = max_length - text_length;
        $(this).next('.form-text').find('span').html(text_remaining + ' characters remaining');
    });
}
@extends('re-usables.basic_layout')
<!-- Calls header.blade -->
<!-- Changes title in header -->
@section('title','Audting Leads')

@section('page_content')

    <!-- Content Start -->
    <h1>Lead [{{$lead->id}}] Audit</h1>

    <div class="row">
        @foreach($audits as $audit)
            @php($changes = $audit->getModified())
            @php($meta = $audit->getMetadata())
            <div class="col-sm-6">
                <div class="card">
                    <div class="card-header bg-secondary text-white "><h5>Audit - {{$meta['audit_id']}}
                            - {{ date("jS F Y H:ia",strtotime($meta['audit_created_at'])) }}</h5></div>
                    <div class="card-body">
                        @foreach($changes as $key => $change)
                            <strong><b>{{strtoupper($key)}} : </b></strong>
                            <br>
                            Original : {{$change['old']}}
                            <br>
                            New : {{$change['new']}}
                            <hr>
                        @endforeach
                    </div>
                    <div class="card-footer bg-secondary text-white ">
                        Called via : {{$meta['audit_url']}} @ {{$meta['audit_ip_address']}}<br>
                        By User ID : {{$meta['user_id']}}
                    </div>
                </div>
            </div>
        @endforeach
    </div>
    <!-- Content End -->
@endsection
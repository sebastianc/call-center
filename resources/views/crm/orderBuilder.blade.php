@extends('layouts.app')

@section('content')
    <div class="container">

        <div class="row" id="main" align="">
            <div class="col-md-8 offset-2">
                <div class="card" align="center">

                    <div class="card-header">
                        <h2>Payment Setup Options</h2>
                    </div>

                    <div class="card-body">
                        <p>Chose how you want to take a payment</p>

                        <button id="st_reg" class="btn btn-outline-success btn-block">Secure Trading</button>

                    </div>

                </div>

            </div>

        </div>

        <div class="row" id="regular" style="display: none">
                <div class="col-sm-8 offset-2">
                    <div class="alert" id="st-message"></div>
                    <div class="alert alert-info" role="alert">
                        {{env("SECURETRADING_ACCOUNT")}}
                    </div>
                    <form id="st-payment" class="form" action="/payment/process/auth" method="post">
                        <fieldset>
                            <!--Ensure all payment details use the data-st-field attribute.-->
                            <legend>Card Details</legend>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="pan">Card Number</label>
                                        <input type="text" class="form-control" id="pan" data-st-field="pan"
                                               autocomplete="off" maxlength="17"/>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-4 col-md-4">
                                    <div class="form-group">
                                        <label for="expirymonth">Expiry Month</label>
                                        <input type="text" class="form-control" id="expirymonth"
                                               data-st-field="expirymonth"
                                               autocomplete="off" maxlength="2" minlength="2"/>
                                    </div>
                                </div>

                                <div class="col-sm-4 col-md-4">
                                    <div class="form-group">
                                        <label for="expiryyear">Expiry Year</label>
                                        <input type="text" class="form-control" id="expiryyear"
                                               data-st-field="expiryyear"
                                               autocomplete="off" maxlength="4" minlength="4"/>
                                    </div>
                                </div>

                                <div class="col-sm-4 col-md-4">
                                    <div class="form-group">
                                        <label for="expiryyear"> Security Code</label>
                                        <input type="text" class="form-control" id="securitycode"
                                               data-st-field="securitycode"
                                               autocomplete="off" minlength="3" maxlength="4"/>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="card_issuer">Card Issuer</label>
                                        <select name="card_issuer" class="form-control" id="card_issuer">
                                            <option value="VISA">VISA</option>
                                            <option value="MASTERCARD">MasterCard</option>
                                            <option value="AMEX">American Express</option>
                                            <option value="MAESTRO">Maestro</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <legend>Customer Details</legend>
                            <div class="row">
                                <div class="col-sm-12">
                                    {{csrf_field()}}
                                    <div class="form-group">
                                        <label for="first_name">First Name</label>
                                        <input type="text" name="first_name" id="first_name"
                                               value="{{$lead->first_name}}"
                                               class="form-control" readonly required minlength="2">
                                    </div>

                                    <div class="form-group">
                                        <label for="last_name">Last Name</label>
                                        <input type="text" name="last_name" id="last_name"
                                               value="{{$lead->last_name}}"
                                               class="form-control" readonly required minlength="2">
                                    </div>

                                    <div class="form-group">
                                        <label for="email_address"><i class="fa fa-at"></i>Email Address</label>
                                        <input type="email" name="email" id="email_address"
                                               value="{{$lead->email_address}}"
                                               class="form-control" readonly required minlength="2">
                                    </div>
                                    <div class="form-group">
                                        <label for="phone_number"><i class="fa fa-phone"></i>Telephone</label>
                                        <input type="text" name="phone_number" id="phone_number"
                                               value="{{str_replace(array(" ","(",")"),"",$lead->phone_number)}}"
                                               class="form-control" readonly required minlength="6">
                                    </div>
                                </div>
                            </div>
                            <legend>Chargable Information</legend>
                            <div class="row">

                                <div class="col-md-12">
                                    <label>Order Reference</label>
                                    <input class="form-control" name="order_reference" type="text"
                                           value="{{$lead->id.time()."ITN"}}" readonly required>
                                </div>
                                <input type="hidden" name="lead_id" id="lead_id" value="{{$lead->id}}">
                                <div class="col-md-6">
                                    <label for="amount">Amount (Ex V.A.T)</label>
                                    <input class="form-control" type="text" id="amount" required value="{{ number_format($lead->calculateCost(),2) }}">
                                </div>
                                <div class="chargable">
                                    <label for="amount">Chargeable Amount (Inc V.A.T)</label>
                                    <input class="form-control" type="text" name="amount" id="chargable" value="{{ number_format($lead->calculateCost() * 1.2,2) }}" readonly>
                                </div>
                            </div>
                        </fieldset>
                        <br>
                        <button type="submit" class="btn btn-block btn-success">Make Payment</button>
                    </form>

                </div>
            </div>
        </div>
@endsection
@section('js')
    <script src="https://webservices.securetrading.net/js/st.js"></script>
    <script>
        $(document).ready(function () {
            $("#st_reg").click(function () {
                $("#regular").toggle();
            })
        });

        new SecureTrading.Standard({
            sitereference: "{{env("SECURETRADING_ACCOUNT")}}", locale: "en_gb"
        });
    </script>
@endsection



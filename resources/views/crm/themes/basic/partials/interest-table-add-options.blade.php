<form method="post" action="{{ route('crm.update-interest') }}" class="form-inline flex-nowrap interest-table-add-options">
    @csrf
    <input type="hidden" name="interest_id" value="{{ $interest->id }}">
    <select id="discounting" class="form-control-sm mr-1" name="discount"  @if($interest->in_basket && $interest->outstanding_value > 1) disabled @endif>

        <option value="">&pound;{{ number_format($interest->calculateCost(), 2) }}</option>
        {{-- Hardcoding the increment here isn't ideal, this should probably be a database-driven config value --}}
        @for($p = 5; $p < $max_discount + 1; $p = $p+5)
            <option value="{{ $p }}" @if ($interest->discount_percentage == $p) selected @endif>&pound;{{ number_format($interest->calculateCost(true, $p),2) }} (-{{ $p }}%)</option>
        @endfor
        {{--@foreach ([5,10,15,20] as $p)--}}
        {{--<option value="{{ $p }}" @if ($interest->discount_percentage == $p) selected @endif>&pound;{{ number_format($interest->calculateCost(true, $p),2) }} (-{{ $p }}%)</option>--}}
        {{--@endforeach--}}


    </select>
    @if ($interest->in_basket && $interest->outstanding_value < 1)
    <button class="btn btn-sm btn-success refresh" name="action" value="refresh"><i class="fa fa-sync-alt"></i></button>
    <button class="btn btn-sm btn-danger ml-1" type="submit" name="action" value="remove"><i class="fa fa-times-circle"></i></button>
    @elseif ($interest->in_basket && $interest->outstanding_value > 1)
        <button class="btn btn-sm btn-success refresh" disabled="disabled"><i class="fa fa-sync-alt"></i></button>
        <button class="btn btn-sm btn-danger ml-1" disabled="disabled"><i class="fa fa-times-circle"></i></button>
    @else
    <button class="btn btn-sm btn-success add" name="action" value="add"><i class="fa fa-shopping-basket"></i> Add</button>
    @endif
</form>

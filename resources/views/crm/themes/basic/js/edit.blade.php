<script>

    /**
     * Check that the user is still logged in and still owns the lead,
     * whenever they click anywhere on the page.
     * Loosely based on https://stackoverflow.com/a/338685/1463965
     */
    var blocking_modal_open = false;
    function gotBlockingModal(responseText, statusText, xhr) {
        //console.log(responseText);
        if (responseText.blocked) {
            /**
             * Flash up a message saying "Lead taken", block screen, force redirect
             **/
            blocking_modal_open = true;
            $('#blockingModal').find('.modal-content').html(responseText.modalContent);
            $('#blockingModal').modal({
                backdrop: 'static',
                keyboard: false
            });
        }
    }

    var modalTimeout;

    function checkBlockingModal() {
        if (!blocking_modal_open) {
            $.getJSON('{{ route('crm.get-blocking-modal') }}', { lead_id: {{ $lead->id }} }, gotBlockingModal);
        }

        clearTimeout(modalTimeout);
        modalTimeout = setTimeout(function(){ checkBlockingModal(); }, 10100);
    }

    /**
    * This polls the server repeatedly and checks to see if the user is logged in, and still owns this lead.
    * Note that this can make testing difficult, as we have a thousand hits in the Chrome Network log.
    * I'm also concerned about server load, so I've made it easy to disable this feature
    **/

    @if (env("ENABLE_SERVER_POLLING_FEATURE"))
    $(document).ready(function() {
        //$(document).ajaxStop(checkBlockingModal);
        checkBlockingModal();
    });
    @endif

    var table;
    var editor;

    /**
     * This needs breaking up a bit:
     **/
    $(document).ready(function() {

        /**
        * This avoids having to pass the csrf_token to every Ajax call (eg, DataTables Editor)
        **/
        $.ajaxSetup({
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
        });

        var columns = {!! json_encode($columns) !!};
        /**
         * Add a render() argument to some columns; it's hard to do this from the
         * PHP Controller, because the function() is wrapped in "" by json_encode
         **/
        $(columns).each(function(index, column) {
            if (column.data == 'interest_percentage') {
                columns[index]['render'] = function ( data, type, row, meta ) {
                    return '<i class="fa fa-edit"></i> '+data+'%';
                }
            }
            else if (column.data == 'sale_options') {
                /**
                 * Allow us to render HTML in this column,
                 * from https://stackoverflow.com/a/12323523/1463965
                 **/
                /**
                 * TODO: just check for HTML tags here rather than hardcocding the check for column name?
                 **/
                columns[index]['render'] = function ( data, type, row, meta ) {
                    return $("<div/>").html(data).text();
                }
            }
        });

        editor = new $.fn.dataTable.Editor( {
            ajax: '{!! route('crm.update',['list'=>'interests']) !!}',
            table: '#interestTable',
            fields: {!! json_encode($editable_fields) !!},
        });

        /**
         * This is a slightly clunky workaround to a datatables inline editing quirk
         * which is alluded to here: https://datatables.net/forums/discussion/41501/dependent-change-event-triggered-on-editor-open
         * Essentially, we want to submit the inline "percentage" select field on change (ie, when a user
         * selects a new percentage), but when we attach the on('change') event, then it triggers as soon
         * as we display the select field on click. So, we use inline_edit_open to see whether we've
         * already rendered the select, so that we only submit the form on the second change.
         * However -- this same code triggers when we change the percentage select in the "create" modal,
         * which we don't want. So, we have to track whether the modal is open too, via modal_edit_open
         * This is all a bit messy and could do with being revisited.
         **/
        let inline_edit_open = false;
        let modal_edit_open = false;
        $('#interestTable').on( 'click', 'tbody td.editable', function (e) {
            editor.inline(this, {
                onBlur: 'submit'
            });
        } );
        editor.on( 'open', function (event, mode, action) {
            if (mode == 'main') { /* See: https://editor.datatables.net/reference/event/open */
                modal_edit_open = true;
            }
        } );
        editor.on( 'close', function () {
            modal_edit_open = false;
        } );

        editor.field( 'interest_percentage' ).input().on( 'change', function () {
            if (modal_edit_open) {
                return; /* No need to treat this field differently within the modal */
            }
            else if (inline_edit_open) {
                editor.submit();
                inline_edit_open = false;
            }
            else {
                inline_edit_open = true;
            }
        } );
        editor.add( {
            type:    "hidden",
            name:    "lead_id",
            default: {{ $lead->id}}
        } );
        table = $('#interestTable').DataTable({
            pageLength: 100,
            processing: true,
            serverSide: true,
            ajax: '{!! route('crm.dataTableData', ['list'=>'interests','parent_id'=>$lead->id]) !!}',
            columns: columns,
            dom: "Brt",
            ordering: false,
            /**
            * Add the "Editor" button for bulk updating
            **/
            buttons: [
                {
                    extend: "create",
                    editor: editor,
                    text: "<i class='fa fa-edit'></i> Register new interest",
                    className: 'btn-warning btn-sm'
                },
                {
                    name: 'increaseDiscounts',
                    text: '<i class="fas fa-arrow-up"></i> Increase discounts',
                    className: 'btn-info btn-sm'
                }
            ],
            /**
            * Add some data- values to the "Increase discount" button, to trigger the generic modal
            **/
            "initComplete": function(settings, json) {
                table.button( 'increaseDiscounts:name' )
                .nodes()
                .attr({
                    'data-toggle': 'modal',
                    'data-target': '#ajaxModal',
                    'data-url': '{{ route('crm.ajax-block', ['block'=>'increase-discounts','id'=>$lead->id])}}'
                });
            }
        });

    });

    $(document).ready(function() {
        refreshPaymentCard();
        initInterestTableForms();
    });
    function refreshPaymentCard() {
        $.post("{{ route('crm.ajax-block', ['block'=>'payment-card']) }}", { lead_id: {{$lead->id}} })
            .done(function( data ) {
                $("#payment_card").html( data );
                initStripeForm();
        });
    }
    function initInterestTableForms() {
        $('#interestTable').on('click','form.interest-table-add-options :submit',function(event) {
            /**
             * Establish what data we're posting, and what button was clicked (add, refresh, remove)
             **/
            let $button = $(this);
            let $form   = $(this).parent();

            let action = $button.val();
            let data   = $form.serializeArray();

            data.push({name: 'action', value: action});
            $.post($form.attr('action'), data)
                .done(function( data ) {
                    /**
                     * Redraw the datatable
                     **/
                    table.draw();
                    /**
                     * Refresh the payment panel
                     **/
                        refreshPaymentCard();
                });

            event.preventDefault();
        });
    }

    /**
    * Load the "callback" panel dynamically:
    **/
    $(document).ready(function() {
        /**
        * Load the panel on load
        **/
        loadNextCallbackPanel();

    });
    function loadNextCallbackPanel() {
        url = '{{ route('crm.ajax-block',['block'=>'next-callback','id'=>$lead->id]) }}';
        $('#options-card .card-footer').load(url,function(result) {
            // Nothing to do on load I don't think?
        });
    }

    /**
    * Load the communications cards dynamically:
    **/
    $(document).ready(function() {
        $('#communication-cards a[data-toggle=card]').click(function (e) {
            e.preventDefault();
            loadCommunicationTab($(this).attr('id'));
        });
        /**
        * Load the first card tab
        **/
        loadCommunicationTab();

    });
    function loadCommunicationTab(id = null) {
        if (!id) { // In the absence of an ID, load the active tab
            $('#communication-cards .card-body').load($('#communication-cards a.active').attr("data-url"), function(result) {
                initialiseTooltips();
            });
        }
        else {
            var link = $('#'+id);
            var url = link.attr("data-url");
            $('#communication-cards .card-body').load(url,function(result) {
                // Select the right tab
                $('#communication-cards a').removeClass('active');
                link.addClass('active');
                initialiseTooltips();
            });
        }

    }

    function loadCommunicationTabManually(url)
    {
        console.log(url);

        $('#communication-cards .card-body').load(url, function(result) {
            initialiseTooltips();
        });
    }

    /**
    * Modals:
    */
    $('#ajaxModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget); // Button that triggered the modal
        var url = button.data('url'); // Extract info from data-* attributes
        $(this).find('.modal-content').load(url,function(result) {
                // Modal loaded
                refreshDateTimePicker();
                initialiseTextareaCounts();
        });
    });

    /**
    * Initiate call:
    **/
    $('a[data-action=init_call]').click(function(e) {

        e.preventDefault(e); /* Otherwise we follow the '#' link */

        if ($(this).hasClass('on-call')) {
            /**
            * If we're already on the call, "hang up"
            **/
            $('#init_call_options').addClass('d-none');
            /**
            * Hardcoding text here not ideal
            **/
            $(this).html('<i class="fa fa-phone"></i> Initiate Call');
            $(this).addClass('btn-primary');
            $(this).removeClass('btn-danger');
            $(this).removeClass('on-call');
            /**
            * Revert the href="tel":
            **/
            $(this).attr('href', $(this).attr('rel'));

            /**
             * Mark the current call as "ended":
             **/
            $('#init_call_options').find('input[name=ended]').val("1");

        }
        else {
            $('#init_call_options').removeClass('d-none');
            $(this).html('<i class="fa fa-ban"></i> End call');
            $(this).removeClass('btn-primary');
            $(this).addClass('btn-danger');
            $(this).addClass('on-call');
            $(this).click(function() {
                window.location.reload(true);
            });

            /**
            * Temporarily remove the href="tel":
            **/
            $(this).attr('rel',$(this).attr('href'));
            $(this).attr('href','#'); /* using removeAttr() causes styling issues in Boostrap */
        }
        $('#init_call_options').submit();
    });

    $('#init_call_options').on('change','input', function() {
        initOnCallOptions();
        var form = $('#init_call_options');
        form.submit();
    });
    $('#init_call_options').on('submit', function(e) {
        e.preventDefault(e); /** Only submit this form via Ajax **/
        var form = $(this);
        $.ajax({
            type:form.attr('method'),
            url:form.attr('action'),
            data:form.serialize(),
            dataType: 'json',
            success: function(responseText, statusText, xhr)  {
                let id = responseText.id;

                if (id) {
                    form.find('input[name=id]').val(id);
                }
                else {
                    /**
                    * Reset the form itself, so that a subsequent click creates a new call record:
                    **/
                    $('#init_call_options').find('input[name=id]').val('');
                    $('#init_call_options').find('input[name=pitched]').prop('checked', false);
                    $('#init_call_options').find('input[name=answered]').prop('checked', false);
                    $('#init_call_options').find('input[name=ended]').val('');
                }
                loadCommunicationTab('communication-cards-calls');
            },
            error: function(jqXHR, textStatus, errorThrown ) {
                // I don't think this form can "error" as such either
            },
            complete: function(jqXHR, textStatus) {
                // Not relevant either
            }
        });
    });
    /**
    * Only show the "on call" options if we've ticked the "answered" box:
    **/
    $(function() {
        initOnCallOptions();
    });
    function initOnCallOptions() {
        if ($('#answered').is(':checked')) {
            $('#on-call-options').slideDown('fast');
            $(this).ready(function() {
                $('a').click(function(e){
                    e.preventDefault();
                });
            });
        }
        else {
            $('#on-call-options').slideUp('fast');
            $('#on-call-options').find('input[type=checkbox]').prop('checked', false);
        }
    }

    /**
    * Generic form post from within a modal
    **/
    $('div.modal').on('submit','form.ajax',function(e) {

        e.preventDefault(e);

        var form = $(this);
        var modal = $(this).parents('.modal');

        var submit_button = form.find('button[type=submit][data-loading-text]');
        if (submit_button.length > 0) {
            var submit_button_original_text = submit_button.html();
            submit_button.html(submit_button.attr('data-loading-text'));
        }

        $.ajax({
            type:form.attr('method'),
            url:form.attr('action'),
            data:form.serialize(),
            dataType: 'json',
            success: function(responseText, statusText, xhr)  {
                if (responseText.callback) {
                    handleAjaxCallback(responseText.callback);
                }
                modal.modal('hide');
            },
            error: function(jqXHR, textStatus, errorThrown ) {

                if (jqXHR.status == 422 || jqXHR.status == 500) {
                        // 422 is the status code Laravel returns when validation fails
                    if (jqXHR.status == 422) { // Validation error
                        response = $.parseJSON(jqXHR.responseText);
                        $.each(response.errors, function(field, error) {
                            /**
                             * Highlight the field as invalid, using Bootstrap markup
                             **/
                            $('[name="'+field+'"]').addClass('is-invalid');
                            /**
                             * Add the actual validation error to the is-invalid block
                             **/
                            $('[name="'+field+'"]').next('div.invalid-feedback').html(error);
                        });
                    }
                    else if (jqXHR.status == 500) { // Server error
                        console.log(jqXHR.responseText);
                        // error_messages.push('An error has occurred; please contact support.');
                    }
                }
            },
            complete: function(jqXHR, textStatus) {
                if (submit_button.length > 0) {
                    submit_button.html(submit_button_original_text);
                }
            }
        });
    });

    /**
    * Generic form post from within a card
    **/
    $('div.card').on('submit','form.ajax',function(e) {
        var form = $(this);
        var card = $(this).parents('.card'); /** Not currently used **/

        var submit_button = form.find('button[type=submit][data-loading-text]');
        if (submit_button.length > 0) {
            var submit_button_original_text = submit_button.html();
            submit_button.html(submit_button.attr('data-loading-text'));
        }

        e.preventDefault(e);

        $.ajax({
            type:form.attr('method'),
            url:form.attr('action'),
            data:form.serialize(),
            dataType: 'json',
            success: function(responseText, statusText, xhr)  {
                /**
                * Reset all errors:
                */
                form.each(function() {
                    $(this).find(':input').removeClass('is-invalid');
                });
                new PNotify({
                    text: 'Lead updated', // As per modals; how to customise the callback here?
                    type: 'success',
                    delay: 3000,
                    styling: 'bootstrap3',
                    icons: 'bootstrap3'
                });
                if ($('#init_call_options').hasClass('d-none')) {
                    window.location.reload(true);
                }
            },
            error: function(jqXHR, textStatus, errorThrown ) {

                if (jqXHR.status == 422 || jqXHR.status == 500) {
                    // 422 is the status code Laravel returns when validation fails
                    if (jqXHR.status == 422) { // Validation error
                        response = $.parseJSON(jqXHR.responseText);
                        $.each(response.errors, function(field, error) {
                            $('[name="'+field+'"]').addClass('is-invalid');
                            $('[name="'+field+'"]').next('.invalid-feedback').text(error);
                        });
                    }
                    else if (jqXHR.status == 500) { // Server error
                        new PNotify({
                            text: jqXHR.responseText,
                            type: 'error',
                            delay: 3000,
                            styling: 'bootstrap3',
                            icons: 'bootstrap3'
                        });
                    }
                }
            },
            complete: function(jqXHR, textStatus) {
                if (submit_button.length > 0) {
                    submit_button.html(submit_button_original_text);
                }
            }
        });
    });

    /**
     * Sumit the payment form via Ajax, from https://stackoverflow.com/a/34099169/1463965
    **/
    function initStripeForm() {
        // $('form#stripe-ajax').get(0).submit = function() {
        var form = document.getElementById('stripeAjax');
        form.submit = function() {
            console.log('form.stripe-ajax');
            var form = $(this);
            var data = $(this).serializeArray();

            $.ajax({
                type:form.attr('method'),
                url:form.attr('action'),
                data:data,
                dataType: 'json',
                success: function(responseText, statusText, xhr)  {
                    new PNotify({
                        text: 'Payment taken',
                        type: 'success',
                        delay: 3000,
                        styling: 'bootstrap3',
                        icons: 'bootstrap3'
                    });
                },
                error: function(jqXHR, textStatus, errorThrown ) {
                    if (jqXHR.status == 422 || jqXHR.status == 500) {
                        // 422 is the status code Laravel returns when validation fails
                        if (jqXHR.status == 422) { // Validation error
                            response = $.parseJSON(jqXHR.responseText);
                            $.each(response.errors, function(field, error) {
                                new PNotify({
                                    text: error,
                                    type: 'error',
                                    delay: 3000,
                                    styling: 'bootstrap3',
                                    icons: 'bootstrap3'
                                });
                            });
                        }
                        else if (jqXHR.status == 500) { // Server error
                            new PNotify({
                                text: jqXHR.responseText,
                                type: 'error',
                                delay: 3000,
                                styling: 'bootstrap3',
                                icons: 'bootstrap3'
                            });
                        }
                    }
                }
            });

            return false;
        }
    }

    /**
     * Generic "Ajax link" code, currently used for the "Cancel callback" link
     */
        $('body').on('click','a.ajax',function(e) {
        var link = $(this);
        e.preventDefault(e);
        $.ajax({
            type:'get',
            url:link.attr('href'),
            dataType: 'json',
            success: function(responseText, statusText, xhr)  {
                if (responseText.callback) {
                    handleAjaxCallback(responseText.callback);
                }
            },
            error: function(jqXHR, textStatus, errorThrown ) {
            },
            complete: function(jqXHR, textStatus) {
            }
        });
    });

    $("#lead-status").on("change", function (event) {
        let value = $(this).val();
        $.ajax({
            type: "post",
            url: "{{ route('crm.update-status', ['id'=>$lead->id]) }}",
            data: {status_id: value},
            success: function (data) {
                new PNotify({
                    text: 'Lead status updated',
                    type: 'success',
                    delay: 3000,
                    styling: 'bootstrap3',
                    icons: 'bootstrap3'
                });
            },
            error: function (error) {
                new PNotify({
                    text: 'Lead status failed',
                    type: 'danger',
                    delay: 3000,
                    styling: 'bootstrap3',
                    icons: 'bootstrap3'
                });
            }
        });
    });
    $("#lead-assigned").click(function(){
        let alerted = localStorage.getItem('alerted') || '';
        if (alerted !== 'yes') {
            alert('You will lose access to this lead if you re-assign');
            localStorage.setItem('alerted', 'yes');
        } else {
            $("#lead-assigned").on("change", function (event) {
                let value = $(this).val();
                window.localStorage.clear();
                $.ajax({
                    type: "post",
                    url: "{{ route('crm.update-owner', ['id'=>$lead->id]) }}",
                    data: {user_id: value},
                    success: function (data) {
                        new PNotify({
                            text: 'Lead ownership updated',
                            type: 'success',
                            delay: 3000,
                            styling: 'bootstrap3',
                            icons: 'bootstrap3'
                        });
                        window.setTimeout(function() {
                            location.replace('/crm/table/leads')
                        }, 10);
                    },
                    error: function (error) {
                        new PNotify({
                            text: 'Lead ownership failed',
                            type: 'danger',
                            delay: 3000,
                            styling: 'bootstrap3',
                            icons: 'bootstrap3'
                        });
                    }
                });
            });
        }
    });
    /**
     * TODO: get all this JS into a central file:
     **/
    $(function () {
        $('#memberSurvey').on('shown.bs.collapse', function () {
           document.getElementById('memberSurvey').scrollIntoView();
        })
    });

    function refreshDateTimePicker() {

        var dateFormat = "DD-MM-YYYY";
        var MinDate = "{{ date('d-m-Y') }}";
        dateMin = moment(MinDate, dateFormat);

        $("#datetimepicker").datetimepicker({
            sideBySide: true,
            format: 'DD/MM/YYYY HH:mm',
            /**
            * This works (in that it disables weekend days in the date picker)
            * but gives the datepicker a random future date. I believe this is
            * a bug in the Tempus Dominus datepicker (currently v5.0.1). We do
            * validate for weekends though so restricting the picker isn't essential.
            daysOfWeekDisabled: [0, 6],
            **/
            minDate: dateMin,
            disabledTimeIntervals: [[moment({ h: 0 }), moment({ h: 8 })], [moment({ h: 18 }), moment({ h: 24 })]],
            buttons: {
                showToday: true,
                showClear: true,
                showClose: true
            },
            /**
             * Update the defaults so that we use Font Awesome 5, not 4
             **/
            icons: {
                today: 'far fa-calendar-check',
                clear: 'fas fa-minus-circle',
                close: 'fas fa-check'
            }
        });
    }

    /**
    * Handle a range of callbacks after we've posted an Ajax form
    **/
    function handleAjaxCallback(callback) {
        if (callback == 'callback-updated') {
            loadNextCallbackPanel();
            new PNotify({
                text: 'Callback scheduled',
                type: 'success',
                delay: 3000,
                styling: 'bootstrap3',
                icons: 'bootstrap3'
            });
        }
        else if (callback == 'callback-deleted') {
            loadNextCallbackPanel();
            new PNotify({
                text: 'Callback removed',
                type: 'success',
                delay: 3000,
                styling: 'bootstrap3',
                icons: 'bootstrap3'
            });
        }
        else if (callback == 'note-added') {
            loadCommunicationTab('communication-cards-notes');
            new PNotify({
                text: 'Note added',
                type: 'success',
                delay: 3000,
                styling: 'bootstrap3',
                icons: 'bootstrap3'
            });
        }
        else if (callback == 'sms-sent') {
            loadCommunicationTab('communication-cards-sms');
            new PNotify({
                text: 'SMS sent',
                type: 'success',
                delay: 3000,
                styling: 'bootstrap3',
                icons: 'bootstrap3'
            });
        }
        else if (callback == 'sms-failure') {
            loadCommunicationTab('communication-cards-sms');
            new PNotify({
                title: "Error sending message",
                text: 'SMS unable to send message',
                type: 'error',
                delay: 3000,
                styling: 'bootstrap3',
                icons: 'bootstrap3'
            });
        }
        else if (callback == 'sms-non-verified') {
            loadCommunicationTab('communication-cards-sms');
            new PNotify({
                title: "Error sending message",
                text: 'SMS unable to send to unverified number',
                type: 'error',
                delay: 3000,
                styling: 'bootstrap3',
                icons: 'bootstrap3'
            });
        }
        else if (callback == 'discounts-increased') {
            /**
            * Currently we reload the page here; ideally, we'd refresh the discount dropdown
            * without reloading the page for a better user-experience, but this is a Phase 2 job:
            **/
            location.reload();
        }
        else if (callback == 'survey-updated') {
            /* Close the member survey panel here? */
            new PNotify({
                text: 'Survey updated',
                type: 'success',
                delay: 3000,
                styling: 'bootstrap3',
                icons: 'bootstrap3'
            });
        }
        else {
            console.log("Unhandled callback "+callback);
        }
    }

    $(document).ready(function () {
        console.log("ready fired");
        $('body').on('click','#part_payment_button', function () {
            let amount = parseFloat(prompt("How much is the partial payment? \n(£400.00 / 40000p)"));
            if(isNaN(amount)){
                alert("Please Enter a valid number");
                return;
            }

            var maxval = parseFloat($('#max-cost').text());

            if(amount > maxval) {
                alert("Amount entered is higher than the Product Price");
                return;
            }

            if(amount < 1.0)
            {
                alert("Cannot take less than a £1 as part payment");
                return;
            }
            // decimals to pennies
            amount = (amount * 100).toFixed(0);

            $("#part_payment").val(amount);
            $("#part_payment_form").append('<script src="https://checkout.stripe.com/checkout.js" class="stripe-button"data-amount="'+ (amount) +'" data-key="pk_test_PGSkoImV8KgyA9EFGTFH8bYG" data-name="NewCo. Networks Ltd" data-description="Sales Partial payment" data-image="https://s3-eu-west-1.amazonaws.com/NewCo.-media/images/Sales-Module/default.png" data-locale="auto" data-currency="gbp" data-label="Part Payment" data-email="{{$lead->email_address}}" data-allow-remember-me="true"><<\/script>');
            $("#part_payment_button").hide();
            $("#part_payment_form").show();

        });
    });

    $(document).ready(function () {
        $('[data-toggle="tooltip"]').tooltip()
    });

</script>
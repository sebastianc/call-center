@extends('crm.themes.'.config('crm.theme').'.layouts.app')

@section('content')

    <div class="container">
        <div class="row">

            <div class="col-md-6 offset-md-3 col-sm-12" align="center">
                @if( strlen(Auth::user()->api_token) < 1  )
                    <div class="alert alert-danger" role="alert">
                        <p>No readable API-token associated with this users, unable to create leads until a suitable API-token is added</p>
                    </div>
                @endif
                <div class="alert alert-success" id="success_alert" role="alert" style="display: none">
                    <p>Lead Created</p>
                    <ul id="success">
                    </ul>
                </div>
                <div class="alert alert-danger" id="error_alert" role="alert" style="display: none">
                    <p>An error has appeared whilst creating lead</p>
                    <ul id="error">
                    </ul>
                </div>
                <div class="card">
                    <div class="card-header">
                        <h2>Create new lead</h2>
                    </div>

                    <div class="card-body">

                        <form class="form" id="new_lead">

                            <div class="form-group">
                                <label>Phone Number</label>
                                <input class=form-control type="text" name="phone_number" required minlength="6">
                            </div>

                            <div class="form-group">
                                <label>First Name</label>
                                <input class="form-control" type="text" name="first_name" required>
                            </div>

                            <div class="form-group">
                                <label>Last Name</label>
                                <input class="form-control" type="text" name="last_name" required>
                            </div>

                            <div class="form-group">
                                <label>Email Address</label>
                                <input class="form-control" type="email" name="email_address" required>
                            </div>

                            <div class="form-group">
                                <label>Network</label>
                                <select class="form-control" type="text" name="network_id">
                                    @foreach($networks as $network)
                                        <option value="{{$network->id}}">{{$network->name}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label>Allocate To :</label>
                                <select class="form-control" type="text" name="user_id">
                                    <option value="0">Run through distributor</option>
                                    @foreach($users as $user)
                                        <option value="{{$user->id}}">{{$user->name}}</option>
                                    @endforeach
                                </select>
                            </div>

                        </form>
                        @if( strlen(Auth::user()->api_token) < 1  )
                            <button class="btn btn-danger">Obtain API Token From Team Leader !</button>
                        @else
                            <button class="btn btn-success" id="fireNewLead">Create New Lead</button>
                        @endif
                    </div>

                </div>

            </div>
        </div>
    </div>

@endsection

@section('js')
    <script>
        $(document).ready(function () {
            $("#fireNewLead").click(function () {
                let data = $("#new_lead").serialize();

                $.ajax({
                    type: "POST",
                    url: "/api/lead/precapture?api_token={{\Illuminate\Support\Facades\Auth::user()->api_token}}",
                    data: data,
                    success: function (data) {
                        if (data.response_code === 202) {
                            $("#success_alert").show();
                            $("#error_alert").hide();
                            $("#success").empty().append("<li> Lead ID : " + data.data[0].id + "</li>");
                        }
                    },
                    error: function (error) {
                        console.log(error);
                        $("#success_alert").hide();
                        $("#error_alert").show();
                        $("#error").empty().append("<li> " + error.responseText + "</li>");

                    }
                })
            });
        });
    </script>
@endsection

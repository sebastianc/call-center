@extends('crm.themes.'.config('crm.theme').'.layouts.app')
@section('content')
    <div class="col-sm-12 col-md-10 offset-md-1">
        <h1><code>Internal Reports</code></h1>
        <div class="row">

            <table class="table table-striped table-bordered table-hover">
                <thead>
                <tr>
                    <th>Report Name</th>
                    <th>Report URI</th>
                    <th>Report Params</th>
                    <th>Report Desc</th>
                    <th>Report Sample Link</th>
                </tr>
                </thead>
                <tbody>

                <tr>
                    <td>Lead Status Report</td>
                    <td>/report/leadStatus/{startPoint}/{endPoint}</td>
                    <td><code>startPoint | endPoint</code> = (Y-m-d) date</td>
                    <td>Returns report on the lead and all the status they currently have set against them</td>
                    <td><a class="btn btn-outline-success"
                           href="/report/leadStatus/{{\Carbon\Carbon::now()->subDay()->format('Y-m-d')}}/{{\Carbon\Carbon::now()->format('Y-m-d')}}">Sample
                            Report</a></td>
                </tr>
                <tr>
                    <td>Lead Call Stats Report</td>
                    <td>/report/leadCallStats/{startPoint}/{endPoint}</td>
                    <td><code>startPoint | endPoint</code> = (Y-m-d) date</td>
                    <td>Returns report on the users and their call stats for the period selected</td>
                    <td><a class="btn btn-outline-success"
                           href="/report/leadCallStats/{{\Carbon\Carbon::now()->subDay()->format('Y-m-d')}}/{{\Carbon\Carbon::now()->format('Y-m-d')}}">Sample
                            Report</a></td>
                </tr>
                <tr>
                    <td>Lead callbacks Report</td>
                    <td>/report/leadCallbacks</td>
                    <td><code>N/A</code></td>
                    <td>Returns report on the current callbacks set</td>
                    <td><a class="btn btn-outline-success"
                           href="/report/leadCallbacks">Sample
                            Report</a></td>
                </tr>
                <tr>
                    <td>User sales Report</td>
                    <td>/report/userSales/{id}</td>
                    <td><code>id</code> - User ID e.g. Andrew Nelson = 9</td>
                    <td>Returns report on the sales the person has made for the day</td>
                    <td><a class="btn btn-outline-success"
                           href="/report/userSales/9">Sample
                            Report</a></td>
                </tr>
                <tr>
                    <td>Lead source report</td>
                    <td>/report/leadSources/{length?}</td>
                    <td><code>length</code> - number of months you want to search through</td>
                    <td>Returns each lead and source it came in on</td>
                    <td><a class="btn btn-outline-success"
                           href="/report/leadSources">Sample
                            Report</a></td>
                </tr>
                <tr>
                    <td>Team sales Report</td>
                    <td>/report/userSales/{teamID}</td>
                    <td><code>teamID</code> - Team ID e.g. Team X = 1</td>
                    <td>Returns report on the sales for a given team for the day broken down by each individual user on
                        the team
                    </td>
                    <td><a class="btn btn-outline-success"
                           href="/report/teamSales/1">Sample
                            Report</a></td>
                </tr>

                </tbody>
            </table>
        </div>
    </div>

@endsection
@extends('crm.themes.'.config('crm.theme').'.layouts.app')

@section('content')


    <div class="container">

        <div class="row">

            <div class="card">
                @if (session()->has('success'))
                    <div class="alert alert-success">{{ session('success') }}</div>
                @endif
                <div class="card-header bg-dark text-light">
                    <h3>Catalogue Items</h3>
                </div>

                <div class="card-body">
                    <div class="card-title">
                        <a class="btn btn-outline-dark "  href="/crm/catalogue/view/new">New Catalogue Item <i
                                    class="fas fa-plus-square"></i></a>
                    </div>
                    <div class="col-sm-12">
                        <table class="table table-striped table-hover table-bordered">
                            <thead class="bg-warning bg-dark text-light">
                            <tr>
                                <th>Catalogue Item ID</th>
                                <th>Catalogue Item Title</th>
                                <th>Catalogue Item RRP</th>
                                <th>Catalogue Item Created On</th>
                                <th>Catalogue Item Deleted On</th>
                                <th>Catalogue Item Maximum Discount</th>
                                <th>Edit</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($catalogueItems as $catalogueItem)
                                <tr @if(!is_null($catalogueItem->deleted_at)) class="bg-danger text-light" @endif >
                                    <td>{{$catalogueItem->id}}</td>
                                    <td>{{$catalogueItem->title}}</td>
                                    <td>£{{number_format($catalogueItem->price,2)}}</td>
                                    <td>{{$catalogueItem->created_at}}</td>
                                    <td>@if(is_null($catalogueItem->deleted_at))
                                            N/A @else {{$catalogueItem->deleted_at}}@endif</td>
                                    <td>{{$catalogueItem->max_discount}}% - (£{{number_format($catalogueItem->price - (($catalogueItem->price / 100) * $catalogueItem->max_discount),2) }})</td>
                                    <td>
                                        <div class="btn-group">
                                            @if(!is_null($catalogueItem->deleted_at))
                                                <a class="btn btn-light btn-block"  href="/crm/catalogue/reactivate/{{$catalogueItem->id}}">Re-enable</a>
                                            @else
                                                <a class="btn btn-outline-warning btn-sm"  href="/crm/catalogue/view/edit/{{$catalogueItem->id}}">Edit Item</a>
                                                <a class="btn btn-outline-danger btn-sm"  href="/crm/catalogue/delete/{{$catalogueItem->id}}">Delete Item</a>
                                            @endif

                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>


        </div>

    </div>


@endsection
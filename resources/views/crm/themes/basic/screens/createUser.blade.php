@extends('crm.themes.'.config('crm.theme').'.layouts.app')

@section('content')

    <div class="container">
        <div class="row">

            <div class="col-md-6 offset-md-3 col-sm-12">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="card">
                    <div class="card-header">
                        <h2>Create new user</h2>
                    </div>

                    <div class="card-body">

                        <form class="form" id="user" action="{{route('crm.create-user')}}" method="post">
                            {{csrf_field()}}
                            <div class="form-group">
                                <label for="first_name">First Name</label>
                                <input class="form-control" id="first_name" name="first_name" type="text" required
                                       minlength="2" value="{{ old('first_name') }}">
                            </div>

                            <div class="form-group">
                                <label for="last_name">Last Name</label>
                                <input class="form-control" id="last_name" name="last_name" type="text" required
                                       minlength="2" value="{{ old('last_name') }}">
                            </div>

                            <div class="form-group">
                                <label for="email_address">Email Address</label>
                                <div class="input-group mb-3">
                                    <input class="form-control" id="email_address" name="email_address" type="text" required
                                           minlength="2"
                                           aria-label="Recipient's username" aria-describedby="basic-addon2" value="{{ old('email_address') }}" autocomplete="new-password"> {{-- new-password stops Chrome from trying to autocomplete: https://stackoverflow.com/questions/15738259/disabling-chrome-autofill --}}
                                    <div class="input-group-append">
                                        <span class="input-group-text" id="basic-addon2">@NewCo.networks.com</span>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="password">Password</label>
                                <input class="form-control" id="password" name="password" type="password" required
                                       minlength="8" maxlength="32" autocomplete="new-password">
                            </div>

                            <div class="form-group">
                                <label>What team(s) should this user belong to?</label>
                                <br>
                            @foreach($teams as $team)
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" name="teams[]" type="checkbox" id="team-{{$team->id}}" value="{{$team->id}}">
                                <label class="form-check-label" for="team-{{$team->id}}">{{$team->name}}</label>
                            </div>
                            @endforeach
                            </div>

                            <div class="form-group">
                                <label>What role(s) should this user have?</label>
                                <br>

                                @foreach($roles as $role)
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" name="roles[]" type="checkbox" id="role-{{$role->id}}" value="{{$role->id}}">
                                        <label class="form-check-label" for="role-{{$role->id}}">{{$role->title}}</label>
                                    </div>
                                @endforeach
                            </div>

                            <div class="form-group">
                                <label>Network / Product Allocation</label>
                                <br>
                                @foreach($networks as $network)
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" name="networks[]" type="checkbox" id="network-{{$network->id}}" value="{{$network->id}}">
                                        <label class="form-check-label" for="network-{{$network->id}}">{{$network->abbreviation}}</label>
                                    </div>
                                @endforeach
                            </div>

                            <div class="form-group">
                                <div class="form-check form-check-inline">
                                <input class="form-check-input" name="api_token" id="api_token" type="checkbox" value="1">
                                    <label class="form-check-label" for="api_token">Allowed to create own leads?</label>
                                </div>
                            </div>


                            {{--<div class="form-group">--}}
                                {{--<label for="api_token">API Token</label>--}}
                                {{--<input class="form-control" id="api_token" name="api_token" type="password"--}}
                                       {{--minlength="8" maxlength="32" value="{{md5(microtime())}}" placeholder="Enter a random 8-32 characters here if the user is going to create leads">--}}
                            {{--</div>--}}

                            <div class="form-group">
                                <button class="btn btn-primary" type="submit">Create User</button>
                            </div>

                        </form>
                    </div>

                </div>

            </div>
        </div>
    </div>

@endsection


@extends('crm.themes.'.config('crm.theme').'.layouts.app')

@section('content')
<main role="main" class="container">


        @if(session()->has('success'))
        <div class="alert alert-success">
            {{ session()->get('success') }}
        </div>
            @elseif(session()->has('unauth'))
            <div class="alert alert-danger">
                {{ session()->get('unauth') }}
            </div>
        @endif


      <div class="card mb-4">
          <div class="card-header">
            NewCo. Networks: Sales Module
          </div>
          <div class="card-body">
            <p class="card-text">You are logged in as <strong>{{ Auth::user()->name }}</strong>. Choose an option below.</p>
          </div>
        </div>

      <div class="card-columns">
          @if (Auth::user()->isTeamLeader())
          <div class="card">
            <div class="card-body d-flex flex-column">
              <h5 class="card-title">Users</h5>
              <p class="card-text">Use this screen to view @if (Auth::user()->isTeamLeader()) and create @endif <strong>users</strong>.</p>

              @if (Auth::user()->isTeamLeader())
              <div class="btn-group  d-flex mt-auto">
                    <a href="{{ route('crm.list',['list'=>'users']) }}" class="btn btn-primary w-100"><i class="fas fa-user"></i> View</a>
                    <a href="{{ route('crm.create',['class'=>'user'])}}" class="btn btn-outline-primary w-100"><i class="fas fa-plus"></i> Create</a>
              </div>
              @else
                <a href="{{ route('crm.list',['list'=>'users']) }}" class="btn btn-primary mt-auto"><i class="fas fa-user"></i> View users</a>
              @endif

            </div>
            <div class="card-footer">
                <small class="text-muted">We have <strong>{{ $counts['users'] }}</strong> total users</small>
              </div>
          </div>
          @endif

                @if (Auth::user()->isTeamLeader())
                <div class="card">
                  <div class="card-body d-flex flex-column">
                    <h5 class="card-title">Teams</h5>
                    <p class="card-text">Use this screen to view and create <strong>teams</strong>.</p>

                    <div class="btn-group  d-flex mt-auto">
                          <a href="{{ route('crm.list',['list'=>'teams']) }}" class="btn btn-primary w-100"><i class="fas fa-users"></i> View</a>
                          <a href="{{ route('crm.edit',['class'=>'team'])}}" class="btn btn-outline-primary w-100"><i class="fas fa-plus"></i> Create</a>
                    </div>

                  </div>
                  <div class="card-footer">
                      <small class="text-muted">We have <strong>{{ $counts['teams'] }}</strong> total teams</small>
                    </div>
                </div>
                @endif


              @if (Auth::user()->isTeamLeader() )
                  <div class="card">
                      <div class="card-body d-flex flex-column">
                          <h5 class="card-title">Catalogue Items</h5>
                          <p class="card-text">Use this screen to view <strong>catalogue items</strong>.</p>
                          <a href="{{ route('crm.catalogue-items',['action'=>'all'])}}" class="btn btn-primary mt-auto"><i class="fas fa-book"></i> View Catalogue Items</a>

                      </div>
                      <div class="card-footer">
                          <small class="text-muted">We have <strong>{{ $counts['catalogue-items'] }}</strong> total catalogue items</small>
                      </div>
                  </div>
              @endif

          <div class="card">
              <div class="card-body d-flex flex-column">
                <h5 class="card-title">Leads</h5>
                <p class="card-text">Use this screen to view <strong>leads</strong>.</p>
                  <div class="btn-group  d-flex mt-auto">
                  <a href="{{ route('crm.list',['list'=>'leads']) }}" class="btn btn-primary w-100"><i class="fas fa-headset"></i> View leads</a>
                      @if (Auth::user()->isTeamLeader() )
                      <a href="{{ route('crm.create',['class'=>'lead']) }}" class="btn btn-outline-primary w-100"><i class="fas fa-plus"></i> Create lead</a>
                      @endif
                  </div>
              </div>
              <div class="card-footer">
                  <small class="text-muted">We have <strong>{{ $counts['leads'] }}</strong> total leads</small>
                </div>
            </div>

            <div class="card">
                <div class="card-body d-flex flex-column">
                  <h5 class="card-title">The open pot</h5>
                  <p class="card-text">Use this screen to view <strong>the open pot</strong>.</p>
                  <a href="{{ route('crm.list',['list'=>'open-pot']) }}" class="btn btn-primary mt-auto"><i class="fas fa-box-open"></i> View the open pot</a>

                </div>
                <div class="card-footer">
                    <small class="text-muted">We have <strong>{{ $counts['open-pot'] }}</strong> leads in the open pot</small>
                  </div>
              </div>

        </div>
  </main>
@endsection
@extends('crm.themes.'.config('crm.theme').'.layouts.app')

@section('content')

    <div class="container">

        <div class="row">
            <div class="col-sm-12">

                <div class="card" align="center">
                    <div class="card-header border-dark">
                        @if (session()->has('success'))
                            <div class="alert alert-success">{{ session('success') }}</div>
                        @endif
                        <div class="text-center">
                            <figure class="figure" data-toggle="modal" data-target="#imageModal" style="cursor: pointer">
                                <img src="{{ $user->getAvatar() }}" class="figure-img rounded img-fluid" alt="user_image" id="image" >
                                <figcaption class="figure-caption text-center">Click to edit</figcaption>
                            </figure>
                        </div>
                        <hr>
                        <h2>{{$user->name}}</h2>
                    </div>
                </div>

            </div>
        </div>
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="row">
            <div class="col-sm-12 col-md-8 offset-md-2" style="margin-top: 20px;">
                <div class="card h-100" align="center">
                    <div class="card-header bg-dark border-light text-light">
                        <h3>User Information</h3>
                    </div>

                    <div class="card-body border-success">
                        <form class="form" action="{{route('crm.update-user',['id'=>$user->id])}}" method="POST" >

                            {{csrf_field()}}

                            <div class="form-group">
                                <div class="form-check form-check-inline">
                                    <input @if ($user->active) checked="checked" @endif class="form-check-input" name="active" type="checkbox" value="1" id="form-active">
                                    <label class="form-check-label" for="form-active">Active?</label>
                                </div>
                                <small id="emailHelp" class="form-text text-muted">If a user is not active, they will not be able to log in.</small>
                            </div>

                            <div class="form-group">
                                <label for="id">Unique ID Number</label>
                                <input class="form-control" id="id" readonly value="{{$user->id}}">
                            </div>

                            <div class="form-group">
                                <label for="user_name">Name</label>
                                <input class="form-control" type="text" name="user_name" id="user_name"
                                       value="{{$user->name}}" required>
                            </div>


                            <div class="form-group">
                                <label for="email_address">Email Address</label>
                                <input class="form-control" id="email_address" name="email_address" type="email" required
                                       minlength="2" value="{{$user->email}}">
                            </div>

                            <div class="form-group">
                                <label for="password">Password</label>
                                <input class="form-control" id="password" name="password" type="password"
                                       minlength="8" maxlength="32"
                                       placeholder="Enter password to change password otherwise leave blank">
                            </div>

                            <hr>

                            <div class="form-group">
                                <h3>Enable / Disable Lead Allocation</h3>
                                <table class="table table-striped" style="text-align: center">
                                    <thead>
                                    <tr>
                                        <th>Network / Product</th>
                                        <th>Enabled</th>
                                        <th>Allocation</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @php($total = 0)
                                @foreach($allocations as $allocaton)
                                        <tr>
                                            <td>{{$allocaton->networks->abbreviation}}</td>
                                            <td>
                                                <input class="form-check-input" name="enabled[]" type="checkbox" @if($allocaton->enabled == 1)checked @endif id="{{$allocaton->id}}" value="{{$allocaton->network_id}}"></td>
                                            <td><input type="hidden" name="allocation[]" value="{{$allocaton->id}}">
                                                <input  type="number" name="count[]" class="form-control" value="{{$allocaton->allocation}}"></td>
                                        </tr>
                                    @if($allocaton->enabled == 1)
                                    @php($total += $allocaton->allocation)
                                    @endif
                                @endforeach
                                    </tbody>
                                </table>

                                <h4>Total Enable Lead Cap : {{$total}}</h4>
                            </div>

                            <hr>
                            <div class="form-group">
                                <h3>Enable / Disable Team</h3>
                                <table class="table table-striped" style="text-align: center">
                                    <thead>
                                    <tr>
                                        <th>Team</th>
                                        <th>Status</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($teams as $team)
                                        <tr>
                                            <td>{{$team->name}}</td>
                                            <td><div class="btn-group btn-group-toggle" data-toggle="buttons">
                                                    <label class="btn btn-outline-success @if(in_array($team->id,$current_teams)) active @endif">
                                                        <input type="radio" name="teams[{{$team->id}}]" value="on"   @if(in_array($team->id,$current_teams)) checked @endif> Active
                                                    </label>
                                                    <label class="btn btn-outline-danger @if(!in_array($team->id,$current_teams)) active @endif">
                                                        <input type="radio" name="teams[{{$team->id}}]" value="off"  @if(!in_array($team->id,$current_teams)) checked @endif> Disabled
                                                    </label>
                                                </div></td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>

                            </div>

                            <hr>
                            <div class="form-group">
                                <h3>Enable / Disable Roles</h3>
                                <table class="table table-striped" style="text-align: center">
                                    <thead>
                                    <tr>
                                        <th>Team</th>
                                        <th>Status</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($roles as $role)
                                        <tr>
                                            <td>{{$role->title}}</td>
                                            <td><div class="btn-group btn-group-toggle" data-toggle="buttons">
                                                    <label class="btn btn-outline-success @if(in_array($role->id,$current_roles)) active @endif">
                                                        <input type="radio" name="roles[{{$role->id}}]" value="on"   @if(in_array($role->id,$current_roles)) checked @endif> Active
                                                    </label>
                                                    <label class="btn btn-outline-danger @if(!in_array($role->id,$current_roles)) active @endif">
                                                        <input type="radio" name="roles[{{$role->id}}]" value="off"  @if(!in_array($role->id,$current_roles)) checked @endif> Disabled
                                                    </label>
                                                </div></td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>

                            </div>

                            <div class="form-group">
                                <div class="form-check form-check-inline">
                                    <input @if(strlen($user->api_token )> 0) checked="checked" @endif class="form-check-input" name="api_token" type="checkbox" value="1">
                                    <label class="form-check-label">Allowed to create own leads?</label>
                                </div>
                            </div>

                            <div class="form-group">
                                <button class="btn btn-success" type="submit">Save Changes</button>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-sm-12" style="margin-top: 20px;">

                <div class="card">
                    <div class="card-header bg-dark  border-light text-light" align="center">
                        <h3>Available Action</h3>
                    </div>

                    <div class="card-body border-success">
                        <a href="/crm/impersonate/start/{{$user->id}}" class="btn btn-lg btn-outline-primary">Login as
                            user</a>
                        {{--<button class="btn btn-lg btn-outline-danger">Disable User</button>--}}
                    </div>
                </div>

            </div>
        </div>

    </div>

    <!---Change Image---->
    <div class="modal fade" id="imageModal" tabindex="-1" role="dialog" aria-labelledby="imageModal" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Change User Image</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="container" align="center">
                        <form action="{{route("crm.update-image",['id'=>$user->id])}}" method="POST" enctype="multipart/form-data">
                            <h2>Upload New Image</h2>
                            {{csrf_field()}}
                            <img style="height: 120px; width: 128px;" src="{{$user->getAvatar()}}" class="rounded img-fluid" alt="user_image" id="preview_image">
                            <hr>
                            <input type="file" name="image" id="new_image" onchange="updatePreview(this)">
                            <hr>
                            <button class="btn btn-dark" type="submit">Proceed</button>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>

@endsection

@section('js')
    <script>
        function updatePreview(input)
        {
            if (input.files && input.files[0]) {
                let reader = new FileReader();

                reader.onload = function (e) {
                    $('#preview_image')
                        .attr('src', e.target.result);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }



    </script>
@endsection

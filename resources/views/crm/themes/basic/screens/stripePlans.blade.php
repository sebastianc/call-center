@extends('crm.themes.'.config('crm.theme').'.layouts.app')

@section('content')

    <div class="container">

        <h1></h1>

        <div class="col-sm-12">

            <div class="card">
                <div class="card-header bg-dark text-light">
                    <h3>Plans Available</h3>
                </div>

                <div class="card-body">

                    <table class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Plan Identifier <br><small>use this for product association</small></th>
                            <th>Plan Stripe ID</th>
                            <th>Plan Recurring Amount</th>
                            <th>Plan Currency</th>
                            <th>Plan Recurring Point</th>
                            <th>Plan Active</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($stripe_data->data->items as $stripe_datum)
                        <tr>
                            <td>{{$stripe_datum->id}}</td>
                            <td>{{$stripe_datum->stripe_id}}</td>
                            <td>{{$stripe_datum->amount}}</td>
                            <td>{{$stripe_datum->currency}}</td>
                            <td>{{$stripe_datum->interval}}</td>
                            <td>{{$stripe_datum->active}}</td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>

                </div>

            </div>

        </div>

@endsection
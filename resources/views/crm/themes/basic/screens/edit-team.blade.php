@extends('crm.themes.'.config('crm.theme').'.layouts.app')

@section('content')
    <div class="container">
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        @if ($team->exists)
                        Edit team
                        @else
                        Create team
                        @endif
                    </div>

                    <div class="card-body border-success">

                            <form _class="form" action="{{ route('crm.update-team',['id'=>$team->id]) }}" method="POST">
                                @csrf
                                <div class="form-group">
                                    <label for="name">Team name</label>
                                    <input class="form-control" type="text" name="name" id="name" value="{{$team->name}}" required>
                                </div>
                                <a href="{{ route('crm.dashboard') }}" class="btn btn-default">Cancel</a>
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </form>

                    </div>

                </div>
            </div>

        </div>


    </div>

@endsection


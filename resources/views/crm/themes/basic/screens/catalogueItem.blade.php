@extends('crm.themes.'.config('crm.theme').'.layouts.app')

@section('content')


    <div class="container">
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="row">

        @if($edit === 1)
            <!--Edit form-->
                <div class="col-sm-12">

                    <div class="card">
                        <div class="card-header bg-dark text-light">
                            <h3>Edit Catalogue Items : {{$catalogueItem->title}}</h3>
                        </div>

                        <div class="card-body">

                            <a class="btn btn-outline-dark" href="/crm/catalogue/view/all"><i
                                        class="fas fa-angle-double-left"></i>Go Back</a>

                            <form class="form" action="{{route('crm.update-catalogue-item',['id'=>$catalogueItem->id])}}" method="post">
                                {{csrf_field()}}
                                <div class="form-group">
                                    <label>Catalogue Item Title</label>
                                    <input class="form-control" name="catalogue_item_title" id="catalogue_item_title"
                                           value="{{$catalogueItem->title}}" type="text">
                                </div>

                                <div class="form-group">
                                    <label>Catalogue Item RRP</label>
                                    <input class="form-control" name="catalogue_item_rrp" id="catalogue_item_rrp"
                                           value="{{$catalogueItem->price}}" type="text">
                                </div>

                                <div class="form-group">
                                    <label>Catalogue Item Type</label>
                                    <select class="form-control" name="catalogue_item_type" id="catalogue_item_type">
                                        <option @if($catalogueItem->type == 1)selected @endif value="1">Singular
                                        </option>
                                        <option @if($catalogueItem->type == 2)selected @endif value="2">Subscription
                                        </option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label>Catalogue Legacy Type</label>
                                    <select class="form-control" name="catalogue_legacy_type" id="catalogue_item_type">
                                        <option @if($catalogueItem->legacy_type == 1)selected @endif value="1">Package
                                        </option>
                                        <option @if($catalogueItem->legacy_type == 2)selected @endif value="2">Product
                                        </option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label>Catalogue Item Max Discount %</label>
                                    <select class="form-control" name="catalogue_item_discount"
                                            id="catalogue_item_discount">
                                        @for($p = 5; $p < 101; $p = $p+5)
                                            <option @if($p == $catalogueItem->max_discount) selected
                                                    @endif value="{{$p}}">{{$p}}</option>
                                        @endfor
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label>Associated Stripe Plans <a href="{{route('crm.catalogue-items',['action'=>'plans'])}}" target="_blank">see plans here !</a></label>
                                    <input class="form-control" name="catalogue_stripe_plan" id="catalogue_item_rrp"
                                           value="{{$catalogueItem->stripe_plan_id}}" type="text">
                                </div>


                                <div class="form-group">
                                    <button type="submit" class="btn btn-outline-warning">Update</button>
                                </div>


                            </form>
                        </div>
                    </div>


                    @else
                        <div class="col-sm-12">

                            <div class="card">
                                <div class="card-header bg-dark text-light">
                                    <h3>Create Catalogue Item</h3>
                                </div>

                                <div class="card-body">

                                    <a class="btn btn-outline-dark" href="/crm/catalogue/view/all"><i
                                                class="fas fa-angle-double-left"></i>Go Back</a>

                                    <form class="form" action="/crm/catalogue/create" method="post">
                                        {{csrf_field()}}
                                        <div class="form-group">
                                            <label>Catalogue Item Title</label>
                                            <input class="form-control" name="catalogue_item_title"
                                                   id="catalogue_item_title" type="text">
                                        </div>

                                        <div class="form-group">
                                            <label>Catalogue Item RRP</label>
                                            <input class="form-control" name="catalogue_item_rrp"
                                                   id="catalogue_item_rrp" type="text">
                                        </div>

                                        <div class="form-group">
                                            <label>Catalogue Item Type</label>
                                            <select class="form-control" name="catalogue_item_type"
                                                    id="catalogue_item_type">
                                                <option value="1">Singular
                                                </option>
                                                <option value="2">Subscription</option>
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label>Catalogue Legacy Type</label>
                                            <select class="form-control" name="catalogue_legacy_type" id="catalogue_item_type">
                                                <option value="1">Package
                                                </option>
                                                <option value="2">Product
                                                </option>
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label>Catalogue Item Max Discount %</label>
                                            <select class="form-control" name="catalogue_item_discount"
                                                    id="catalogue_item_discount">
                                                @for($p = 5; $p < 101; $p = $p+5)
                                                    <option value="{{$p}}">{{$p}}</option>
                                                @endfor
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label>Associated Stripe Plans <a href="{{route('crm.catalogue-items',['action'=>'plans'])}}" target="_blank">see plans here !</a></label>
                                            <input class="form-control" name="catalogue_stripe_plan" id="catalogue_item_rrp"
                                                    type="text">
                                        </div>


                                        <div class="form-group">
                                            <button type="submit" class="btn btn-outline-warning">Create</button>
                                        </div>

                                    </form>
                                </div>
                            </div>

                            @endif

                        </div>

                </div>



@endsection
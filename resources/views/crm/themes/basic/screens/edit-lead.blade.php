@extends('crm.themes.'.config('crm.theme').'.layouts.app')
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-8">
            <div class="card mb-3">
                <div class="card-header bg-info text-white">
                    Lead information for <strong>{{ $lead->first_name }} {{ $lead->last_name }}</strong>, who first registered an interest on <strong>{{ Carbon\Carbon::parse($lead->created_at)->format('jS F Y') }}</strong>.
                </div>
                @if($lead->timezone !== "Europe/London")
                    <div class="card-header bg-danger text-white">
                       Different timezone GMT({{\Carbon\Carbon::now($lead->timezone)->format("P")}}) Hours , Their current time is : <strong>{{\Carbon\Carbon::now($lead->timezone)->format('H:iA - l jS F (e)')}}  </strong>
                    </div>
                @endif
                <div class="card-body">
                    <div class="mb-4">
                        <table id="interestTable" class="dt-responsive table table-striped table-bordered">
                            <thead>
                                <tr>
                                    @foreach ($columns as $column)
                                    <th>{{ $column->label }}</th>
                                    @endforeach
                                </tr>
                            </thead>
                        </table>
                    </div>

                    <div>
                        <div class="card" id="communication-cards">
                            <div class="card-header">
                                <ul class="nav nav-tabs card-header-tabs">
                                    <li class="nav-item">
                                        <a class="nav-link active" id="communication-cards-notes" href="#"
                                           data-toggle="card"
                                           data-url="{{ route('crm.ajax-block',['block'=>'lead-notes','id'=>$lead->id])}}"><i
                                                    class="fas fa-file-alt"></i> Notes</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="communication-cards-calls" href="#" data-toggle="card"
                                           data-url="{{ route('crm.ajax-block',['block'=>'lead-calls','id'=>$lead->id])}}"><i
                                                    class="fas fa-phone-square"></i> Calls</a>
                                    </li>

                                    <li class="nav-item">
                                        <a class="nav-link" id="communication-cards-sms" href="#" data-toggle="card"
                                           data-url="{{ route('crm.ajax-block',['block'=>'lead-sms','id'=>$lead->id])}}"><i
                                                    class="far fa-comment-alt"></i> SMS
                                            @if ($lead->hasUnreadSMS())
                                                <span class="badge badge-danger">{{ $lead->getUnreadSMSCount() }}</span>
                                            @endif
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="communication-cards-audit" href="#" data-toggle="card"
                                           data-url="{{ route('crm.ajax-block',['block'=>'lead-audit','id'=>$lead->id])}}"><i
                                                    class="fas fa-clipboard-list"></i> Audit</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="communication-cards-engagement" href="#" data-toggle="card"
                                           data-url="{{ route('crm.ajax-block',['block'=>'lead-engagement','id'=>$lead->id])}}"><i
                                                    class="fas fa-eye"></i> Engagement</a>
                                    </li>
                                    @if($lead->isUkMobile())
                                    <li class="nav-item ml-auto">
                                        <a class="nav-link" href="#" data-toggle="modal" data-target="#ajaxModal"
                                           data-url="{{ route('crm.ajax-block',['block'=>'send-sms','id'=>$lead->id])}}"><i
                                                    class="fas fa-comment-alt"></i> Send SMS</a>
                                    </li>
                                        @else
                                        <li class="nav-item ml-auto" data-toggle="tooltip" data-placement="top" data-html="true" title="“This lead does not have a valid <em><b>UK</b></em> mobile number">
                                            <button disabled="disabled" class="nav-link" href="#"  data-toggle="modal" data-target="#ajaxModal"
                                               data-url="{{ route('crm.ajax-block',['block'=>'send-sms','id'=>$lead->id])}}"><i
                                                        class="fas fa-comment-alt"></i><strike>Send SMS</strike></button>
                                        </li>

                                    @endif
                                    <li class="nav-item">
                                        <a class="nav-link" href="#" data-toggle="modal" data-target="#ajaxModal"
                                           data-url="{{ route('crm.ajax-block',['block'=>'add-note','id'=>$lead->id])}}"><i
                                                    class="fa fa-edit"></i> Add new note</a>

                                </li>
                                </ul>
                            </div>
                            <div class="card-body">
                                <!-- The content of this card is loaded via Ajax -->
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            {{-- Member survey --}}
            {{-- Using an include isn't ideal here, we end up having pass parameters through various nested views --}}
            {{-- I'll refactor this to load this panel via Ajax --}}
            @include('crm.themes.'.config('crm.theme').'.cards.member-survey',['survey'=>$survey,'member_survey_questions'=>$member_survey_questions])

            {{-- / Member survey --}}
        </div>
        <div class="col">
            <div class="sticky-top" style="top: 70px">
                    <div class="card mb-2" id="options-card">
                        <div class="card-header">
                            <ul class="nav nav-tabs card-header-tabs">
                                <li class="nav-item">
                                    <a class="nav-link active" id="call-options-tab" data-toggle="tab" href="#call-options">Options</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="lead-details-tab" data-toggle="tab" href="#lead-details">Edit lead</a>
                                </li>
                                <li class="nav-item ml-auto">
                                    <a class="nav-link" href="#memberSurvey" data-toggle="collapse" data-target="#memberSurvey"><i class="far fa-clipboard"></i> Member Survey</a>
                                </li>
                            </ul>
                        </div>
                        <div class="card-body">
                            <div class="tab-content" id="myTabContent">
                                <div class="tab-pane show active" id="call-options">
                                    <form class="mb-3 d-none" id="init_call_options" method="post" action="{{ route('crm.update-call')}}">
                                        @csrf
                                        <input type="hidden" name="id" value="">
                                        <input type="hidden" name="ended" value="0">
                                        <input type="hidden" name="lead_id" value="{{ $lead->id }}">
                                        <div class="form-group">
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value="1" name="answered" id="answered">
                                                <label class="form-check-label" for="answered">
                                                    Call answered?
                                                </label>
                                            </div>
                                            <div id="on-call-options">
                                                <div class="form-check">
                                                    <input class="form-check-input" type="checkbox" value="1" name="pitched" id="pitched">
                                                    <label class="form-check-label" for="pitched">
                                                        Pitched?
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </form>

                                    <div class="mb-2">
                                        <a class="btn btn-primary btn-block" href="tel:{{$lead->phone_number}}" data-action="init_call"><i class="fa fa-phone"></i> Initiate Call</a>
                                        <a class="btn btn-primary btn-block"  href="#" data-action="callback"  data-toggle="modal" data-target="#ajaxModal" data-url="{{ route('crm.ajax-block',['block'=>'reschedule-callback','id'=>$lead->id])}}"><i class="fa fa-calendar-alt"></i>
                                            @if (is_null($lead->callback()))
                                                Schedule Callback
                                            @else
                                                Reschedule Callback
                                            @endif

                                        </a>
                                    </div>

                                    <form>
                                        <div class="form-row">
                                            <div class="col">
                                                <label for="lead-status" class="small">Status</label>
                                                <select class="form-control" id="lead-status">
                                                    @foreach($status as $state)
                                                    <option @if($state->id == $lead->lead_status_id) selected @endif value="{{$state->id}}">{{$state->title}} - {{$state->category}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col">
                                                <label for="lead-assigned" class="small">Assigned to...</label>
                                                <select class="form-control" id="lead-assigned">
                                                  @foreach($users as $user)
                                                      <option @if($user->id == $lead->user_id) selected @endif value="{{$user->id}}">{{$user->name}} @if($user->id == Auth::user()->id) (You)@endif</option>
                                                    @endforeach
                                                </select>
                                            </div>

                                        </div>
                                    </form>


                                </div>
                                <div class="tab-pane" id="lead-details">
                                        <form class="form ajax" action="{{ route('crm.update-lead',['lead_id'=>$lead->id]) }}" method="post">
                                            {{csrf_field()}}

                                            <div class="form-row mb-3">
                                                    <div class="col">
                                                        <input type="text" name="first_name" id="first_name" value="{{$lead->first_name}}" class="form-control" required
                                                            minlength="2">
                                                        <div class="invalid-feedback">
                                                            <!-- This is populated via Ajax -->
                                                        </div>
                                                    </div>
                                                    <div class="col">
                                                            <input type="text" name="last_name" id="last_name" value="{{$lead->last_name}}" class="form-control" required minlength="2">
                                                            <div class="invalid-feedback">
                                                                    <!-- This is populated via Ajax -->
                                                                </div>
                                                    </div>

                                                </div>


                                            <div class="form-group">

                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text"><i class="fa fa-at"></i></span>
                                                    </div>
                                                    <input type="email" name="email_address" id="email_address" value="{{$lead->email_address}}" class="form-control" required>
                                                    <div class="invalid-feedback">
                                                            <!-- This is populated via Ajax -->
                                                        </div>
                                                </div>

                                            </div>
                                            @if( Auth::user()->isTeamLeader())
                                            <div class="form-group">
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text"><i class="fa fa-phone"></i></span>
                                                    </div>
                                                    <input type="text" name="phone_number" id="phone_number" value="{{$lead->phone_number}}" class="form-control" required
                                                        minlength="6">
                                                        <div class="invalid-feedback">
                                                                <!-- This is populated via Ajax -->
                                                            </div>
                                                </div>
                                            </div>
                                            @else
                                                {{-- Potentially not the best approach --}}
                                                <div class="form-group">
                                                    <div class="input-group mb-3">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text"><i class="fa fa-phone"></i></span>
                                                        </div>
                                                        <input type="text" name="phone_number" id="phone_number" value="{{$lead->phone_number}}" class="form-control" required
                                                               minlength="6">
                                                        <div class="invalid-feedback">
                                                            <!-- This is populated via Ajax -->
                                                        </div>
                                                    </div>
                                                </div>
                                            @endif

                                            <div class="form-group">

                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text"><i class="fa fa-globe"></i></span>
                                                    </div>
                                                    <select name="timezone" id="timezone" class="form-control">
                                                        <optgroup label="Europe">
                                                            @foreach(DateTimeZone::listIdentifiers(DateTimeZone::EUROPE) as $zones);
                                                            <option value="{{$zones}}" @if($zones == $lead->timezone) selected @endif>{{$zones}}</option>
                                                            @endforeach
                                                        </optgroup>
                                                        <optgroup label="America">
                                                            @foreach(DateTimeZone::listIdentifiers(DateTimeZone::AMERICA) as $zones);
                                                            <option value="{{$zones}}" @if($zones == $lead->timezone) selected @endif >{{$zones}}</option>
                                                            @endforeach
                                                        </optgroup>
                                                        <optgroup label="Asia">
                                                            @foreach(DateTimeZone::listIdentifiers(DateTimeZone::ASIA) as $zones);
                                                            <option value="{{$zones}}" @if($zones == $lead->timezone) selected @endif >{{$zones}}</option>
                                                            @endforeach
                                                        </optgroup>
                                                    </select>
                                                    <div class="invalid-feedback">
                                                        <!-- This is populated via Ajax -->
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="form-group">
                                                <button type="submit" class="btn btn-block btn-success" @if($impersonated) disabled @endif data-loading-text="Saving...">@if($impersonated)Unable to update @else Update @endif
                                                </button>
                                            </div>
                                        </form>

                                </div>
                            </div>
                        </div>
                        <div class="card-footer text-muted" id="nextCallbackPanel">
                            <!-- The content of this card is loaded via Ajax -->
                        </div>
                    </div>


                <div class="card mb-2" id="payment_card">
                    <!-- The content of this card is loaded via Ajax -->
                </div>


            </div>

        </div>
    </div>
</div>

<div class="modal fade" id="ajaxModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
<!-- The contents of this modal are loaded via Ajax -->
        </div>
    </div>
</div>
{{-- This is the "Access denied" modal --}}
<div class="modal fade" id="blockingModal" tabindex="-1">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <!-- The contents of this modal are loaded via Ajax -->
        </div>
    </div>
</div>


@endsection

@section('css')
<link href="//cdn.datatables.net/1.10.16/css/dataTables.bootstrap4.min.css" rel="stylesheet">
<link href="//cdn.datatables.net/buttons/1.5.1/css/buttons.bootstrap4.min.css" rel="stylesheet">
<link href="//cdn.datatables.net/select/1.2.5/css/select.bootstrap4.min.css" rel="stylesheet">

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.1/css/tempusdominus-bootstrap-4.min.css" />

<link rel="stylesheet" href="{{ asset('vendor/Editor-PHP-1.7.3/css/editor.bootstrap4.min.css') }}" />

<link href="{{ asset('css/datatables.css') }}" rel="stylesheet">
<link href="{{ asset('css/edit.css') }}" rel="stylesheet">
@endsection

@section('js')
<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.1/moment.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.1/locale/en-gb.js"></script>
<script src="{{ asset('vendor/moment/moment-round.min.js') }}"></script>

<script src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="//cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
<script src="//cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script src="//cdn.datatables.net/buttons/1.5.1/js/buttons.bootstrap4.min.js"></script>

<script src="//cdn.datatables.net/select/1.2.5/js/dataTables.select.min.js"></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.1/js/tempusdominus-bootstrap-4.min.js"></script>

<script src="{{ asset('vendor/Editor-PHP-1.7.3/js/dataTables.editor.min.js') }}"></script>
<script src="{{ asset('vendor/Editor-PHP-1.7.3/js/editor.bootstrap4.min.js') }}"></script>

<script type="text/javascript" src="{{ asset('vendor/pnotify/pnotify.custom.min.js') }}"></script>
<link href="{{ asset('vendor/pnotify/pnotify.custom.min.css') }}" media="all" rel="stylesheet" type="text/css" />

@include('crm.themes.'.config('crm.theme').'.js.edit',['lead'=>$lead,'columns'=>$columns])

@endsection

@if (count($calls))
<ul class="list-unstyled">
    @foreach ($calls as $call)
    <li class="media">

        <figure class="figure mr-3">
            <a data-toggle="tooltip" title="{{ $call->user->name }}" data-placement="bottom">
                <img src="{{ $call->user->getAvatar() }}" class="avatar figure-img img-fluid rounded-circle mb-1">
            </a>
            {{-- We could do this but in fact a tooltip might be more practical
            <figcaption class="figure-caption text-center">{{ $call->user->getInitials() }}</figcaption>
            --}}
        </figure>

        <div class="media-body">
            <p>
                Called on <strong>{{ date('jS F Y',strtotime($call->created_at) )}}</strong> at <strong>{{ date('H:i',strtotime($call->created_at) )}}</strong> by <strong>{{ $call->user->name}}</strong>.
                @if ($call->answered = 1 && !empty($call->answered_at))
                    Call lasted for <strong>{{ $call->getDurationDescription() }}</strong>.
                @else
                    Call not answered.
                @endif
            <br>
                @if ($call->answered)
                    <span class="badge badge-success">Answered</span>
                @else
                    <span class="badge badge-warning">Not answered</span>
                @endif

                @if ($call->pitched)
                    <span class="badge badge-success">Pitched</span>
                @else
                    <span class="badge badge-warning">Not pitched</span>
                @endif
            </p>
        </div>
    </li>
    @endforeach
</ul>
@else
<div class="alert alert-warning mb-0" role="alert">
    No calls have been made to this lead.
  </div>
  @endif
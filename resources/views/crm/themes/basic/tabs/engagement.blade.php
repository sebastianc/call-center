@if (!is_null($engagement_history))

    <div class="card-group" align="center">
        <div class="card">
            <div class="card-body">
                <h5 class="card-title">Job Applications</h5>
                <p class="card-text">{{$engagement_history->data->{'Job Applications'} }}</p>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <h5 class="card-title">Job Views</h5>
                <p class="card-text">{{$engagement_history->data->{'Jobs Viewed'} }}</p>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <h5 class="card-title">Recent Logins</h5>
                <p class="card-text">{{$engagement_history->data->{'Recent Logins'} }}</p>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <h5 class="card-title">Threads Started</h5>
                <p class="card-text">{{$engagement_history->data->{'Threads Started'} }}</p>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <h5 class="card-title">Forum Comments</h5>
                <p class="card-text">{{$engagement_history->data->{'Forum Comments'} }}</p>
            </div>
        </div>

    </div>

@else
    <div class="alert alert-warning mb-0" role="alert">
        No Engagement History Available
    </div>
@endif
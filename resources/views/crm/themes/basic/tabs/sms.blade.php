@if (count($sms))
 <ul class="list-unstyled">
    @foreach ($sms as $sm)
    <li class="media">

        <figure class="figure mr-3">
                <a data-toggle="tooltip" title="{{ $sm->user->name }}" data-placement="bottom">
                    <img src="{{ $sm->user->getAvatar() }}" class="avatar figure-img img-fluid rounded-circle mb-1">
                </a>
                {{-- We could do this but in fact a tooltip might be more practical
                <figcaption class="figure-caption text-center">{{ $call->user->getInitials() }}</figcaption>
                --}}
            </figure>
        <div class="media-body">
            <blockquote class="blockquote" style="font-size: 90%">
                @if (!$sm->read)
                <span class="badge badge-danger">New</span>
                @endif
                {{$sm->message}}
                <footer class="blockquote-footer">
                @if ($sm->inbound)
                Received from {{ $sm->phone_number }} on {{ date('jS F Y H:i',strtotime($sm->created_at) )}}
                @else
                Sent to {{ $sm->phone_number }} on {{ date('jS F Y H:i',strtotime($sm->created_at) )}}
                @endif
                </footer>
            </blockquote>
        </div>
    </li>
    @endforeach
</ul>
@else
<div class="alert alert-warning mb-0" role="alert">
    No SMS have been sent to this lead.
  </div>
  @endif
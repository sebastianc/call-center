@if (count($notes))
 <ul class="list-unstyled">
    @foreach ($notes as $note)
    <li class="media">

        <figure class="figure mr-3">
                <a data-toggle="tooltip" title="{{ isset($note->user->name)? $note->user->name : '' }}" data-placement="bottom">
                    <img src="{{ isset($note->user)? $note->user->getAvatar(): '' }}" class="avatar figure-img img-fluid rounded-circle mb-1">
                </a>
                {{-- We could do this but in fact a tooltip might be more practical
                <figcaption class="figure-caption text-center">{{ $call->user->getInitials() }}</figcaption>
                --}}
            </figure>

        <div class="media-body">
            <blockquote class="blockquote" style="font-size: 90%">
                {{$note->text}}
                <footer class="blockquote-footer">{{ date('jS F Y H:i',strtotime($note->created_at) )}}</footer>
            </blockquote>
        </div>
    </li>
    @endforeach
</ul>
@else
<div class="alert alert-warning mb-0" role="alert">
    No notes have been made for this lead.
  </div>
  @endif
@if (count($audits))
    @foreach ($audits as $audit)
        <p align="left"><b><u>{{$audit->getUser()->name}}
                    -- {{date('d/m/Y',strtotime($audit->getMetadata()['audit_created_at']) ) }}
                    at {{date('H:i',strtotime($audit->getMetadata()['audit_created_at']) )}}</u></b></p>
        <ul class="list-unstyled">
            @if($audit->getAuditType() === 'Interest')
                {{--Interest Logic--}}
                @foreach($audit->getModified() as $key => $value )
                    <li style="padding-left: 20px;">
                        @php
                            $old = $value['old'] === NULL ? 0 : $value['old'];
                            if($value['old'] === false) {
                                $old = 'false';
                            } elseif($value['old'] === true) {
                                $old = 'true';
                            }

                            $new = $value['new'] === NULL ? 0 : $value['new'];

                            if($value['new'] === false) {
                                $new = 'false';
                            } elseif($value['new'] === true) {
                                $new = 'true';
                            }

                        @endphp
                        @if ($old === $new)
                            @continue
                        @endif
                        Interest [<em><code style="color: dodgerblue">{{$audit->getInterest()}}</code></em>] -
                        <code>{{ strtoupper(str_replace("_"," ",$key)) }}</code> was changed from
                        <code>{{ $old }}</code> to
                        <code>{{ $new }}</code>

                    </li>
                @endforeach
            @else
                {{--Lead Logic--}}
                @foreach($audit->getModified() as $key => $value )
                    <li style="padding-left: 20px;">
                        @if($key === 'lead_status_id')
                            Leads <code>Lead Status</code> was changed from
                            <code>{{ \App\LeadStatus::find($value['old'])->title }}</code> to
                            <code>{{ \App\LeadStatus::find($value['new'])->title }}</code>
                        @elseif($key === 'user_id')
                            Leads <code>Assigned User</code> was changed from
                            <code>{{ \App\User::find($value['old'])->name }}</code> to
                            <code>{{ \App\User::find($value['new'])->name }}</code>
                        @else
                            Leads <code>{{ strtoupper(str_replace("_"," ",$key)) }}</code> field was changed from
                            <code>{{ $value['old'] }}</code> to <code>{{ $value['new'] }}</code>
                        @endif
                    </li>
                @endforeach
            @endif
        </ul>
        <hr>
    @endforeach
    <!-- Custom pagination code -->
    <div class="btn-group" role="group" aria-label="Basic example">
        <button class="btn btn-light" href="#" data-toggle="card"
                onclick="loadCommunicationTabManually('{{ route('crm.ajax-block', ['block'=>'lead-audit','id'=>$lead->id]) }}?page=1')"><i
                    class="fa fa-fast-backward"></i></button>
        @if($current_page > 1)
            <button class="btn btn-light" href="#" data-toggle="card"
                    onclick="loadCommunicationTabManually('{{ route('crm.ajax-block', ['block'=>'lead-audit','id'=>$lead->id]) }}?page={{$current_page - 1}}')">
                <i class="fa fa-backward"></i></button>
        @endif
        @for($i = 1; $i < ($total_pages + 1); $i++ )
            @if($i == $current_page)
                <button class="btn btn-light active" aria-pressed="true" href="#" data-toggle="card"
                        onclick="loadCommunicationTabManually('{{ route('crm.ajax-block', ['block'=>'lead-audit','id'=>$lead->id]) }}?page={{$i}}')">{{$i}}</button>
            @else
                <button class="btn btn-light" href="#" data-toggle="card"
                        onclick="loadCommunicationTabManually('{{ route('crm.ajax-block', ['block'=>'lead-audit','id'=>$lead->id]) }}?page={{$i}}')">{{$i}}</button>
            @endif
        @endfor
        @if($current_page < $total_pages)
            <button class="btn btn-light" href="#" data-toggle="card"
                    onclick="loadCommunicationTabManually('{{ route('crm.ajax-block', ['block'=>'lead-audit','id'=>$lead->id]) }}?page={{$current_page + 1}}')">
                <i class="fa fa-forward"></i></button>
        @endif
        <button class="btn btn-light" href="#" data-toggle="card"
                onclick="loadCommunicationTabManually('{{ route('crm.ajax-block', ['block'=>'lead-audit','id'=>$lead->id]) }}?page={{$total_pages}}')">
            <i class="fa fa-fast-forward"></i></button>
    </div>
    <!-- End pagination code -->
@else
    <div class="alert alert-warning mb-0" role="alert">
        No changes have been made to this lead
    </div>
@endif
<div class="modal-header">
    <h5 class="modal-title">
        @if (is_null($lead->callback()))
            Schedule callback
        @else
            Reschedule callback
        @endif
    </h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<div class="modal-body">
    <div class="container">
        <form class="form ajax" action="{{ route('crm.update-callback',['id'=>$lead->id]) }}" method="post">
            {{csrf_field()}}
            @if(!is_null($lead->callback()))
                <p>Current callback:
                    <strong>{{ date('jS F \a\t H:i',strtotime($lead->callback()->callback_time)) }}</strong>.</p>
                @if($lead->timezone !== "Europe/London")
                    <p><span class="badge badge-danger">Local time call back
                            {{\Carbon\Carbon::createFromTimeString($lead->callback()->callback_time)->timezone($lead->timezone)->format('jS F \a\t H:i')}}</span>
                    </p>
                @endif
            @endif
            <div class="form-group">
                @if(!is_null($lead->callback()))
                    <label for="defer">Defer callback by...</label>
                @else
                    <label for="defer">Call this lead in...</label>
                @endif
                <select class="form-control" name="defer" id="defer">
                    <option></option>
                    <option value="1">1 hour</option>
                    <option value="4">4 hours</option>
                    <option value="12">12 hours</option>
                    <option value="24">24 hours</option>
                </select>
                <div class="invalid-feedback">
                    Please select an option
                </div>
            </div>

            <div class="form-group">
                <label for="datetimepicker">
                    ... or select a day/time:
                </label>
                {{-- WIP:
                    <div class="input-group date" id="datetimepicker" data-target-input="nearest">
                        <input type="text" name="callback" id="callback" class="form-control datetimepicker" data-target="#datetimepicker"/>
                        <div class="input-group-append" data-target="#datetimepicker" data-toggle="datetimepicker">
                            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                        </div>
                    </div>
                --}}
                <input type="text" name="callback" class="form-control datetimepicker-input" id="datetimepicker"
                       data-toggle="datetimepicker" data-target="#datetimepicker" autocomplete="off"/>
                <div class="invalid-feedback">
                    Please select a date
                </div>
            </div>
            <div class="form-group">
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" value="1" name="is_priority" id="priority_callback">
                    <label class="form-check-label" for="priority_callback">
                        Is this a priority callback?
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" value="1" name="is_qualified"
                           id="qualified_callback">
                    <label class="form-check-label" for="qualified_callback">
                        Is this a QA callback?
                    </label>
                </div>
            </div>

            <button type="submit" class="btn btn-block btn-warning">Set callback</button>
        </form>
    </div>
</div>
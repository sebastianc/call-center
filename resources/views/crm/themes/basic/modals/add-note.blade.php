<div class="modal-header">
    <h5 class="modal-title" id="addNoteModalTitle">Add note</h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<form class="ajax" method="post" action="{{ route('crm.add-note',['lead_id'=>$lead->id]) }}">
    <div class="modal-body">
        <div class="form-group">
            <label for="note">Enter your note here</label>
            <textarea class="form-control" name="note_text" id="note" rows="3"></textarea>
            <div class="invalid-feedback">
                Please enter a note
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button type="submit" class="btn btn-primary" data-loading-text="Saving...">Add note</button>
    </div>
</form>
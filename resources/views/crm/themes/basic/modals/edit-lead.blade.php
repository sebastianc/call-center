<!-- modal for interests -->

<div class="modal fade" id="interestsModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Register new interests</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="container">
                    <form class="form" action="/crm/update/lead/{{$lead->id}}/interest/add" method="post">
                        <div class="col-sm-12">
                            {{csrf_field()}}
                            <div class="col-sm-12 form-group">
                                <label for="catalogue_items">Catalogue Item</label>
                                <select id="catalogue_items" name="catalogue_items" class="form-control">
                                    <optgroup label="Memberships">
                                        @foreach($catalogueItems as $catalogueItem)
                                            @if($catalogueItem->type == 1)
                                                <option value="{{$catalogueItem->id}}">{{$catalogueItem->title}} --
                                                    RRP: £{{$catalogueItem->price}}</option>
                                            @endif
                                        @endforeach
                                    </optgroup>
                                    <optgroup label="Products">
                                        @foreach($catalogueItems as $catalogueItem)
                                            @if($catalogueItem->type == 2)
                                                <option value="{{$catalogueItem->id}}">{{$catalogueItem->title}} --
                                                    RRP: £{{$catalogueItem->price}}</option>
                                            @endif
                                        @endforeach
                                    </optgroup>

                                </select>
                            </div>

                            <div class="col-sm-12 form-group">
                                <label for="catalogue_interest">Interest</label>
                                <select id="catalogue_interest" name="catalogue_interest" class="form-control">
                                    <option value="25">25% - Partially Interest</option>
                                    <option value="50">50% - Interested</option>
                                    <option value="75">75% - Very Interested</option>
                                    <option value="100">100% - Practically Bought</option>
                                </select>
                            </div>

                            <div class="col-sm-12 form-group">
                                <label for="catalogue_value">Potential Value</label>
                                <input type="number" step="0.01" name="catalogue_value" class="form-control"
                                       required>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-block btn-success">Add Interest</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- modal for notes -->

<div class="modal fade" id="addNotes" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Create new note</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="container">
                    <form class="form" action="/crm/update/lead/{{$lead->id}}/note/add" method="post">
                        <div class="col-sm-12">
                            {{csrf_field()}}

                            <div class="form-group">
                                <label for="note_text">Note content</label>
                                <textarea class="form-control" rows="8" name="note_text" id="note_text"
                                          required></textarea>
                            </div>

                            <div class="form-group">
                                <button class="btn btn-block btn-success" type="submit">Add Note</button>
                            </div>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!--- Modal for call backs -->
<div class="modal fade" id="callbackModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Call Back</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="container">
                    @if(!is_null($lead->callback))
                        <a href="/crm/update/lead/{{$lead->id}}/callback/update/1"
                           class="btn btn-block btn-outline-warning ">Defer
                            1 Hour</a>
                        <a href="/crm/update/lead/{{$lead->id}}/callback/update/4"
                           class="btn btn-block btn-outline-warning ">Defer
                            4 Hours</a>
                        <a href="/crm/update/lead/{{$lead->id}}/callback/update/12"
                           class="btn btn-block btn-outline-warning ">Defer
                            12 Hours</a>
                        <a href="/crm/update/lead/{{$lead->id}}/callback/update/24"
                           class="btn btn-block btn-outline-warning ">Defer
                            24 Hours</a>
                    @endif
                    <form class="form" action="/crm/update/lead/{{$lead->id}}/callback/update" method="post">
                        {{csrf_field()}}
                        <div class="form-group">
                            <label for="callback">User Defined</label>
                            <input type="text" name="callback" id="callback" class="form-control" placeholder="set time"
                                   value="{{date("Y-m-d H:i:s",time() + 3600)}}">
                        </div>

                        <button type="submit" class="btn btn-block btn-warning">Set Call Back</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


<!--- Modal for Payments -->
<div class="modal fade" id="paymentModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Payment</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

            </div>
        </div>
    </div>
</div>
<div class="modal-header">
    <h5 class="modal-title">Lead no longer available</h5>
</div>
<div class="modal-body">
    <p>This lead has been taken by someone else.</p>
    {{-- TODO: can we reliably use back() here? The user may want to return to their list
    of leads here, or they might want to go back to the open pot, depending on where they were --}}
    <p>Please <a href="{{ route('crm.dashboard') }}">go back and pick another</a>.</p>
</div>
<div class="modal-footer">
    <a href="{{ route('crm.dashboard') }}" class="btn btn-primary">OK</a>
</div>
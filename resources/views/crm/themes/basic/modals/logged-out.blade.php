<div class="modal-header">
    <h5 class="modal-title">Logged out</h5>
</div>
<div class="modal-body">
    <p>Your session has expired.</p>
    <p>Please <a href="{{ route('login') }}">login again</a>.</p>
</div>
<div class="modal-footer">
    <a href="{{ route('login') }}" class="btn btn-primary">OK</a>
</div>
<div class="modal-header">
    <h5 class="modal-title" id="addNoteModalTitle">Send an SMS (UK mobiles only)</h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<form class="ajax" method="post" action="{{ route('crm.send-sms',['lead_id'=>$lead->id]) }}">
    <input type="hidden" name="phone_number" value="{{ $lead->phone_number }}">
    <div class="modal-body">
        <div class="form-group">
            <label for="message">Enter the text of your SMS here</label>
            <textarea class="form-control" name="message" id="message" rows="3" maxlength="140"></textarea>
            <small class="form-text text-muted">This text will be sent to {{ $lead->phone_number }}. <span
                        class="count">140 characters remaining</span></small>
            <div class="invalid-feedback">
                Please enter a message
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        @if($lead->isMobileNumber())
            <button type="submit" class="btn btn-primary" data-loading-text="Sending...">Send Message</button>
        @else
            <button type="submit" class="btn btn-default"  disabled data-loading-text="Sending..."><strike>Send Message</strike></button>
        @endif
    </div>
</form>

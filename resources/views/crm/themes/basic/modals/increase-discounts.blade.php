<div class="modal-header">
    <h5 class="modal-title" id="addNoteModalTitle">Increase the discount limit</h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<form class="ajax" method="post" action="{{ route('crm.increase-discounts',['lead_id'=>$lead->id]) }}">
    <div class="modal-body">
        <div class="form-group">
            <input type="password" class="form-control" id="password" name="password" placeholder="Enter the manager's password">
            <small class="form-text text-muted">This will enable higher discounts for this lead for ten minutes.</small>
            <div class="invalid-feedback">
                Incorrect password
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button type="submit" class="btn btn-primary" data-loading-text="Checking...">Submit</button>
    </div>
</form>
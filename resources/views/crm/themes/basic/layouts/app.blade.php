<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>NewCo. Networks: CRM</title>

    <!-- Bootstrap core CSS -->
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">

    <!-- Custom styles for this template -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css"
          integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Heebo:400,700" rel="stylesheet">
    <style>
        body {
            min-height: 75rem;
            padding-top: 4.5rem;
            font-family: 'Heebo', sans-serif;
        }
    </style>
    @yield('css')
</head>

<body>

<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
    <a class="navbar-brand" href="{{ route('crm.dashboard') }}"><img
                src="{{ asset('images/NewCo.-logo-white.png') }}" alt="NewCo. Networks" height="30"></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse"
            aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarCollapse">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="{{ route('crm.dashboard') }}">Dashboard</a>
            </li>
            @if (Auth::user()->isTeamLeader())
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('crm.list',['list'=>'users']) }}">Users</a>
                </li>
            @endif
            @if (Auth::user()->isTeamLeader())
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('crm.list',['list'=>'teams']) }}">Teams</a>
                </li>
            @endif
            <li class="nav-item">
                <a class="nav-link" href="{{ route('crm.list',['list'=>'leads']) }}">Leads</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('crm.list',['list'=>'open-pot']) }}">Open Pot</a>
            </li>
            @if(Auth::user()->onEngagementTeam() || Auth::user()->isTeamLeader() || Auth::user()->onRenewalsTeam())
            <li class="nav-item">
                <a class="nav-link" href="{{ route('crm.list',['list'=>'engagement-pot']) }}">Engagement Pot</a>
            </li>
            @endif
            @if(Auth::user()->onRenewalsTeam() || Auth::user()->isTeamLeader())
            <li class="nav-item">
                <a class="nav-link" href="{{ route('crm.list',['list'=>'renewals-pot']) }}">Renewals Pot</a>
            </li>
            @endif
            @if(Auth::user()->isTeamLeader())

                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown"
                       aria-haspopup="true" aria-expanded="false">
                        Reporting
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                        <a class="dropdown-item" href="{{route('report.exportable')}}">Internal</a>
                        <a class="dropdown-item" target="_blank" href="https://reports.NewCo.networks.com">External</a>
                    </div>
                </li>
            @endif
        </ul>
        <ul class="navbar-nav ml-auto">

            @impersonating
            <li class="nav-item">
                <a class="nav-link" href="{{ route('crm.stop-impersonation') }}">Stop Impersonation</a>
            </li>
            @endImpersonating
            <li class="nav-item">
                <a class="nav-link" href="{{ route('logout') }}"
                   onclick="event.preventDefault();
                                  document.getElementById('logout-form').submit();" data-toggle="tooltip"
                   title="{{ Auth::user()->name }}">
                    {{ __('Logout') }}
                </a>
            </li>
            </li>
        </ul>
        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            @csrf
        </form>
    </div>
</nav>


@yield('content')

<!-- Bootstrap core JavaScript
    ================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
        integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"
        integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T"
        crossorigin="anonymous"></script>
<script src="{{ asset('js/main.js') }}"></script>

@yield('js')
</body>
</html>




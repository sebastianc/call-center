@if (!is_null($lead->callback()))
Next callback:
    <strong>{{ date('D jS M H:i',strtotime($lead->callback()->callback_time)) }}</strong>
    <a href="{{ route('crm.delete-callback',['id'=>$lead->id, 'callbackID' => $lead->callback()->id]) }}" class="float-right text-danger ajax"><i class="fas fa-times-circle"></i> Cancel</a>
    @if ($lead->callback()->is_priority)
    <span class="badge badge-warning">Priority</span>
    @endif
    @if ($lead->callback()->is_qualified)
    <span class="badge badge-warning">QA</span>
    @endif
    @if($lead->timezone !== "Europe/London")
        <span class="badge badge-danger">{{\Carbon\Carbon::createFromTimeString($lead->callback()->callback_time)->timezone($lead->timezone)->format('D jS M H:i')}} Local Time</span>
    @endif
@else
 <i>No callback set.</i>
@endif
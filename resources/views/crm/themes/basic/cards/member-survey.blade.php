<div class="collapse card" id="memberSurvey">
    <div class="card-header text-white bg-success">
        Member survey
    </div>
    <div class="card-body">
        <form class="form ajax" action="{{ route('crm.member-survey') }}" method="post">
            @csrf
            <input type="hidden" name="lead_id" value="{{ $lead->id }}">
            <div class="form-row">

                @foreach ($member_survey_questions as $field=>$question)

                @if (!property_exists($survey, $field) && false)
                    <div class="alert">{{ $field }} is not a property</div>
                @else
                    <div class="form-group col-md-6">
                        <label for="{{ $field }}" class="small">{{ $question }}</label>

                        @if (in_array($field, ['job_title'])) {{-- Standard text input --}}
                        <input type="text" class="form-control form-control-sm" id="{{ $field }}" name="{{ $field }}" value="{{ $survey->$field }}">
                        @elseif (in_array($field, ['expertise', 'industries', 'markets'])) {{-- Multiple selects --}}
                            <select multiple class="form-control select2" id="{{ $field }}" name="{{ $field }}[]">
                                <option selected value="." style="display:none"> </option>
                                @foreach (App\Survey::getPossibleEnumValues($field) as $value)
                                <option value="{{ $value }}" @if (in_array($value, explode(',',$survey->$field))) selected @endif>{{ $value }}</option>
                                @endforeach
                            </select>
                        @else
                        <select class="form-control select2" id="{{ $field }}" name="{{ $field }}">
                            <option value="">Select</option>
                            @foreach (App\Survey::getPossibleEnumValues($field) as $value)
                            <option value="{{ $value }}" @if ($survey->$field == $value) selected @endif>{{ $value }}</option>
                            @endforeach
                        </select>
                        @endif
                    </div>
                @endif

                @endforeach

            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
</div>

@section('js')

    @parent

    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-theme/0.1.0-beta.10/select2-bootstrap.min.css"  rel="stylesheet">
    <script>
    $(document).ready(function() {
        $('.select2').select2({
            theme: "bootstrap",
            allowClear: true,
            multiple: true
        });
        $('button [type="submit"]').click(function(){
            if($('#expertise').val()==''){
                $('#expertise').val('.').trigger('change.select2');
            }
            if($('#industries').val()==''){
                $('#industries').val('.').trigger('change.select2');
            }
            if($('#markets').val()==''){
                $('#markets').val('.').trigger('change.select2');
            }

        })
    });
    </script>
@endsection
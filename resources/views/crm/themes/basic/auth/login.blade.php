<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>NewCo. Networks: CRM: Log in</title>

    <!-- Bootstrap core CSS -->
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">

    <!-- Custom styles for this template -->
    <link href="https://fonts.googleapis.com/css?family=Heebo" rel="stylesheet">
    <link href="{{ asset('css/login.css') }}" rel="stylesheet">
  </head>

  <body class="text-center">
    <form class="form-signin"method="POST" action="{{ route('login') }}">
        @csrf
        <img src="{{ asset('images/NewCo.-logo.png') }}" alt="NewCo. Networks" class="img-fluid mb-4" >
        <h1 class="h3 mb-3 font-weight-normal">Please sign in</h1>

        @if($errors->any())
        <div class="alert alert-danger" role="alert">
        @foreach ($errors->all() as $error)
              {{ $error }}<br>
        @endforeach
        </div>
        @endif

        <label for="inputEmail" class="sr-only">Email address</label>
        <input type="email" name="email" id="inputEmail" class="form-control" placeholder="Email address" required autofocus value="{{ old('email') }}">
        <label for="inputPassword" class="sr-only">Password</label>
        <input type="password" name="password" id="inputPassword" class="form-control" placeholder="Password" required>
        {{-- Are we allowing "Remember me"? ML to confirm
        <div class="checkbox mb-3">
            <label>
            <input type="checkbox" value="remember-me"> Remember me
            </label>
        </div>
         --}}
        <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>

        <p class="mt-5 mb-3 text-muted">&copy; NewCo. Networks {{ date('Y') }}</p>

    </form>

  </body>
</html>

@extends('layouts.app')

@section('js')
    {{-- Required for inline editing of dates in Editor --}}
    <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.1/moment.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.1/locale/en-gb.js"></script>

    <script src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="//cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js"></script>

    <script src="//cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
    <script src="//cdn.datatables.net/buttons/1.5.1/js/buttons.bootstrap4.min.js"></script>

    <script src="//cdn.datatables.net/select/1.2.5/js/dataTables.select.min.js"></script>

    <script src="{{ asset('vendor/Editor-PHP-1.7.3/js/dataTables.editor.min.js') }}"></script>
    <script src="{{ asset('vendor/Editor-PHP-1.7.3/js/editor.bootstrap4.min.js') }}"></script>

    <script>

        var table;
        var editor;

        $(document).ready(function() {

            /**
            * This avoids having to pass the csrf_token to every Ajax call (eg, DataTables Editor)
            **/
            $.ajaxSetup({
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
            });

            var columns = {!! json_encode($columns) !!};
            columns.push({
                mRender: function (data, type, row) {
                    return '<a class="table-edit" href="'+row.edit_link+'"><i class="far fa-edit"></i></a>'
                },
                searchable: false,
                orderable: false
            });

            editor = new $.fn.dataTable.Editor( {
                ajax: '{!! route('crm.update',['list'=>$list]) !!}',
                table: '#dataTable',
                fields: {!! json_encode($editable_fields) !!}
            });

            $('#dataTable').on( 'click', 'tbody td.editable', function (e) {
                /**
                * Inline editing:
                editor.inline( this, {
                    buttons: { label: '<i class="fa fa-check"></i>', fn: function () { this.submit(); } }
                } );
                **/

                /**
                * Bubble editing: this works better than inline, IMHO
                **/
                editor.bubble(this);
            } );

            table = $('#dataTable').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{!! route('crm.dataTableData',['list'=>$list]) !!}',
                columns: columns,
                /**
                * From https://datatables.net/reference/option/stateSave, means we retain search terms and current page when returning to the table
                **/
                stateSave: true,
                /**
                * Control layout, see https://datatables.net/reference/option/dom
                **/
                /* old version
                dom: "<'row'<'col-sm-4'l><'col-sm-6'f><'col-sm-2'B>>" +
                     "<'row'<'col-sm-12'tr>>" +
                     "<'row'<'col-sm-5'i><'col-sm-7'p>>",
                     */
                     dom: "<'row'<'col-sm-6'l><'col-sm-6'Bf>>" +
                     "<'row'<'col-sm-12'tr>>" +
                     "<'row'<'col-sm-5'i><'col-sm-7'p>>",
                /**
                * Add the "Editor" button for bulk updating
                **/
                buttons: [
                    {
                        extend: "edit",
                        editor: editor,
                        text: "<i class='fa fa-wrench'></i> Edit",
                        className: 'btn-info btn-sm'
                    },
                ],
                /**
                 * Allow rows to be selected, see https://datatables.net/reference/option/select.style
                 */
                select: {
                    style: 'multi+shift',
                },

                /**
                 * Add column filters; see https://datatables.net/examples/api/multi_filter_select.html
                 **/
                initComplete: function () {
                    @foreach ($filters as $column_key=>$filter_values)
                    this.api().columns('{{ $column_key }}').every( function () {

                        var column = this;

                        select_html = '<select><option value="">All '+$(column.header()).text()+'s</option></select>';

                        var select = $(select_html)
                            .appendTo( $(column.header()).empty() )
                            .on( 'change', function () {
                                /**
                                * Removed now that we're using IDs in dropdown filters:
                                *
                                var val = $.fn.dataTable.util.escapeRegex(
                                    $(this).val()
                                );
                                **/
                                var search = $(this).val();
                                if (search) {
                                    search = "^" + search + "$";
                                }
                                column
                                    .search(search, true, false ) // input, regex, smart : see https://datatables.net/reference/api/column().search()
                                    .draw();

                            });
                        var filters = {!! json_encode($filter_values) !!};
                        $.each(filters, function ( id, name ) {
                            /**
                            * If we've filtered by column, and saved state using stateSave,
                            * make sure we pre-select the column option:
                            **/
                            /**
                            * Note that we're switching to using 'id' for a value here;
                            * this makes filtering hasMany values (eg user->roles()) easier
                            * but may require custom filters in the Laravel Datatables query
                            **/
                            /**
                            * The conversion back to a regex here for comparison is a bit clunky
                            **/
                            if (column.search() == '^'+id+'$') {
                                select.append( '<option value="'+id+'" selected="selected">'+name+'</option>' )
                            } else {
                                select.append( '<option value="'+id+'">'+name+'</option>' )
                            }
                        } );
                    });
                    @endforeach
                }
            });
        } );
    </script>
@endsection

@section('css')
    <link href="//cdn.datatables.net/1.10.16/css/dataTables.bootstrap4.min.css" rel="stylesheet">
    {{-- Not sure why dataTables.bootstrap4.min.css sets border-collapse: separate, it looks odd --}}
    <style>
        table.dataTable {
            border-collapse: collapse !important;
        }
        table.dataTable thead th {
            background-color: #fff;
        }
        /**
        * Temporary styling of search boxes, buttons:
        **/
        #dataTable_wrapper #dataTable_filter {
            float: right;
        }
        #dataTable_wrapper .dt-buttons {
            margin-left: 1em;
            float: right;
        }
    </style>

    <link href="//cdn.datatables.net/buttons/1.5.1/css/buttons.bootstrap4.min.css" rel="stylesheet">
    <link href="//cdn.datatables.net/select/1.2.5/css/select.bootstrap4.min.css" rel="stylesheet">

    <link rel="stylesheet" href="{{ asset('vendor/Editor-PHP-1.7.3/css/editor.bootstrap4.min.css') }}"/>

@endsection


@section('content')
<div class="container">

    <div class="row">
        <div class="col-md">
            <div class="alert alert-primary" role="alert">
                {{ $page_header }}
            </div>
            @forelse ($warnings as $warning)
            <div class="alert alert-warning" role="alert">
                {{ $warning }}
            </div>
            @empty
            {{-- No warnings --}}
            @endforelse


        </div>
    </div>


    <table id="dataTable" class="dt-responsive table table-striped table-bordered">
        <thead>
            <tr>
                @foreach ($columns as $column)
                    <th>{{ $column->label }}</th>
                @endforeach
                <th>&nbsp;</th>
            </tr>
        </thead>
    </table>
    <hr>
    <p><a href="#" onclick="table.state.clear(); window.location.reload();">Clear filter</a></p>

</div>
@endsection
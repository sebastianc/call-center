@extends('layouts.app')

@section('content')
@section('css')
    <link href="//cdn.datatables.net/1.10.16/css/dataTables.bootstrap4.min.css" rel="stylesheet">
    {{-- Not sure why dataTables.bootstrap4.min.css sets border-collapse: separate, it looks odd --}}
    <style>
        table.dataTable {
            border-collapse: collapse !important;
        }

        table.dataTable thead th {
            background-color: #fff;
        }

        /**
        * Temporary styling of search boxes, buttons:
        **/
        #dataTable_wrapper #dataTable_filter {
            float: right;
        }

        #dataTable_wrapper .dt-buttons {
            margin-left: 1em;
            float: right;
        }
    </style>

    <link href="//cdn.datatables.net/buttons/1.5.1/css/buttons.bootstrap4.min.css" rel="stylesheet">
    <link href="//cdn.datatables.net/select/1.2.5/css/select.bootstrap4.min.css" rel="stylesheet">

    <link rel="stylesheet" href="{{ asset('vendor/Editor-PHP-1.7.3/css/editor.bootstrap4.min.css') }}"/>

@endsection
@php
    $team = \App\Team::find($lead->team_id);
    $user = \App\User::find($lead->user_id);
@endphp
<div class="container">
    <div class="col-sm-6 offset-sm-3">
        <div class="card" align="center">
            <div class="card-header border-info">
                <h1>Lead Record : [ {{ $lead->id }} ] </h1>
                <h4>{{$team->name}} - {{$user->name}} </h4>
                {{--<h4>Called count : {{count($lead->callHistory)}}</h4>--}}
                @if(!is_null($lead->callback))
                    <h5>Callback set : {{ date('dS M Y H:ia',strtotime($lead->callback)) }}</h5>
                @endif

                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                @if(session()->has('success'))
                    <div class="alert alert-success">
                        {{ session()->get('success') }}
                    </div>
                @endif
            </div>
        </div>
    </div>

    <div class="col-sm-12" style="margin-top: 20px;">
        <div class="card border-info mb-3">
            <div class="card-header text-white bg-info">
                <h4 align="center">Actions</h4>
            </div>

            <div class="card-body">
                <button class="btn btn-lg btn-outline-primary ActionIcon" data-action="init_call">Initiate Call <i
                            class="fa fa-phone"></i></button>
                <button class="btn btn-lg btn-outline-warning ActionIcon" data-action="callback" data-toggle="modal"
                        data-target="#callbackModal">Reschedule Call
                    Back <i class="fa fa-calendar-alt"></i></button>
                <a class="btn btn-lg btn-outline-success ActionIcon" href="/payment/build/{{$lead->id}}" data-action="payment">Take Payment <i
                            class="fa fa-credit-card"></i></a>
                <button class="btn btn-lg btn-outline-danger ActionIcon" data-action="close_lead">Close Lead <i
                            class="fa fa-trash-alt"></i></button>
                <a href="/crm/history/lead/{{$lead->id}}" target="_blank" class="btn btn-lg btn-outline-info">View
                    History <i class="fa fa-history"></i></a>
            </div>
        </div>
    </div>


    <div class="card-group" style="margin-top: 20px;">

        <div class="col-sm-4">
            <div class="card border-info mb-3">
                <div class="card-header text-white bg-info">
                    <h4 align="center">Info</h4>
                </div>
                <div class="card-body">

                    <form class="form" id="lead_info" action="/crm/update/lead/{{$lead->id}}" method="post">
                        {{csrf_field()}}
                        <div class="form-group">
                            <label for="first_name">First Name</label>
                            <input type="text" name="first_name" id="first_name" value="{{$lead->first_name}}"
                                   class="form-control" readonly required minlength="2">
                        </div>

                        <div class="form-group">
                            <label for="last_name">Last Name</label>
                            <input type="text" name="last_name" id="last_name" value="{{$lead->last_name}}"
                                   class="form-control" readonly required minlength="2">
                        </div>

                        <div class="form-group">
                                <label for="email_address">Email Address</label>

                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="fa fa-at"></i></span>
                                    </div>
                            <input type="email" name="email_address" id="email_address"
                                   value="{{$lead->email_address}}"
                                   class="form-control" readonly required minlength="2">
                                </div>

                        </div>
                        @if( Auth::user()->isTeamLeader() || Auth::user()->isAccountManager())
                            <div class="form-group">
                                    <label for="phone_number">Telephone</label>

                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="fa fa-phone"></i></span>
                                        </div>
                                <input type="text" name="phone_number" id="phone_number"
                                       value="{{$lead->phone_number}}"
                                       class="form-control" readonly required minlength="6">
                                    </div>


                            </div>
                        @else
                            {{-- I don't see the purpose of these hidden blocks...? --}}
                            <div class="form-group" style="display: none" id="hidden_number">
                                    <label for="phone_number"><i class="fa fa-phone"></i> Telephone</label>
                                <input type="text" name="phone_number" id="phone_number"
                                       value="{{$lead->phone_number}}"
                                       class="form-control" readonly required minlength="6">
                            </div>

                            <div class="form-group" style="display: block" id="dummy_phone_number">
                                <label for="phone_number"><i class="fa fa-phone"></i>Telephone</label>
                                <input type="password" name="dummy_phone_number"
                                       value="1234567891011"
                                       class="form-control">
                            </div>
                        @endif
                        <div class="form-group">
                            <button type="submit" class="btn btn-block btn-success" id="submit_info" disabled
                                    style="display: none">Update Lead
                            </button>
                        </div>
                        <!--Hidden a tel for triggering call -->
                        <a href="tel:{{$lead->phone_number}}" style="display: none" id="call_trigger"></a>
                    </form>
                </div>

                @if( Auth::user()->isTeamLeader() || Auth::user()->isAccountManager())
                    <div class="card-footer">
                        <button class="btn btn-block btn-info" id="edit_info">Edit Lead</button>
                        <button class="btn btn-block btn-danger" id="edit_cancel" style="display: none">Cancel
                        </button>
                    </div>
                @endif

            </div>
        </div>
        <div class="col-sm-8 ">
            <div class="card border-info mb-3">
                <div class="card-header text-white bg-info">
                    <h4 align="center">Notes</h4>
                </div>
                <div class="card-body" style="max-height: 425px;overflow-y: scroll">
                    <div align="right">
                        <button class="btn btn-sm btn-info"><i class="fa fa-plus" data-toggle="modal"
                                                                   data-target="#addNotes"></i> Add new note
                        </button>
                    </div>
                    <br>
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>Note ID</th>
                            <th>Note Message</th>
                            <th>Recorded at</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($lead->notes as $note)
                            <tr>
                                <td>{{$note->id}}</td>
                                <td>{{$note->text}}</td>
                                <td>{{$note->created_at}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!--- interests row -->
        <div class="col-sm-12" style="margin-top: 20px;">
            <div class="card border-info mb-3">
                <div class="card-header text-white bg-info">
                    <h4 align="center">Interests</h4>
                </div>

                <div class="card-body .table-responsive">
                    <div class="card-body .table-responsive">
                        <table id="interestTable" class="dt-responsive table table-striped table-bordered">
                            <thead>
                            <tr>
                                <th>UID</th>
                                <th>Catalogue Item</th>
                                <th>Interest %</th>
                                <th>Sold</th>
                                <th>Sales Value £</th>
                                <th>Interest Taken</th>
                                <th>Time Elapsed</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($interests as $interest)
                                <tr>
                                    @php($catalogue = $interest->catalogueItem)
                                    <td>{{ $interest->id }}</td>
                                    <td class="editable">{{ $catalogue->title }}</td>
                                    <td class="editable">{{ $interest->interest_percentage }}</td>
                                    @if($interest->sold > 0)
                                        <td class="editable">Yes</td>
                                    @else
                                        <td class="editable">No</td>
                                    @endif
                                    @if($interest->sale_value > 0)
                                        <td class="editable">{{ $interest->sale_value }}</td>
                                    @else
                                        <td class="editable">0</td>
                                    @endif
                                    <td class="editable">{{date('dS M Y H:ia',strtotime($interest->created_at))}}</td>
                                    <?php $original = \Carbon\Carbon::createFromTimestamp(strtotime($interest->created_at)); $now = \Carbon\Carbon::now(); $diff = $original->diffInHours($now) ?>
                                    <td>{{$diff}} Hours</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>

</div>

@include('crm.editLeadModal')

@endsection

<?php

$catalogue = json_encode($catalogueItems->toArray());

?>

@section('js')
    <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.1/moment.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.1/locale/en-gb.js"></script>

    <script src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="//cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js"></script>

    <script src="//cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
    <script src="//cdn.datatables.net/buttons/1.5.1/js/buttons.bootstrap4.min.js"></script>

    <script src="//cdn.datatables.net/select/1.2.5/js/dataTables.select.min.js"></script>

    <script src="{{ asset('vendor/Editor-PHP-1.7.3/js/dataTables.editor.min.js') }}"></script>
    <script src="{{ asset('vendor/Editor-PHP-1.7.3/js/editor.bootstrap4.min.js') }}"></script>
    <script>
        $(document).ready(function () {

            $(document).ready(function () {
                $("#amount").keyup(function () {
                    let amount = $("#amount").val();
                    let total = Number(amount) * 1.2;
                    total = Math.round(Number(total)).toFixed(2);
                    $("#chargable").val(total);
                });
            });

            $.ajaxSetup({
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
            });

            // Options for catalogue
            let options = {!! $catalogue !!};
            let selectopts = [];
            let tmp = {};
            $.each(options, function (i, e) {
                tmp.label = e.title;
                tmp.value = e.id;
                selectopts.push(tmp);
                tmp = {};
            });

            let soldOpts = [{label: "Yes", value: "1"}, {label: "No", value: "0"}];


            // editor code TODO ajax saving route
            let editor; // global
            editor = new $.fn.dataTable.Editor({
                table: "#interestTable",
                ajax: {
                    url: "/crm/update/lead/{{$lead->id}}/interest", complete: function (data) {
                        $.each(data, function (i, e) {
                            console.log(e);
                        })
                    }
                },
                idSrc: 'id',
                fields: [
                    {label: "Catalogue Item", name: "catalogue_item_id", type: "select", options: selectopts},
                    {label: "Interest", name: "interest_percentage"},
                    {label: "Sold", name: "sold", type: "select", options: soldOpts},
                    {label: "Sales Value", name: "sale_value"},
                ]
            });
            // Trigger call
            $('#interestTable').on('click', 'tbody td.editable', function (e) {
                // editor.bubble(this);
            });

            $("#interestTable").DataTable({
                dom: "Bfrtip",
                searching: false,
                "bInfo": false,
                bLengthChange: false,
                "ordering": false,
                columns: [
                    {data: "id"},
                    {data: "catalogue_item_id"},
                    {data: "interest_percentage"},
                    {data: "sold"},
                    {data: "sale_value"},
                    {data: "created_at"},
                    {data: "elapsed"},
                ],
                select: true,
                buttons: [
                    {extend: "create", editor: editor},
                    {extend: "edit", editor: editor},
                ]
            });
            /*
            This for action bars
             */
            $("#edit_info").on("click", function () {
                $("#lead_info *").filter(":input").each(function () {
                    $(this).attr("readonly", false);
                });
                $("#edit_cancel").show();
                $("#edit_info").hide();
                $("#submit_info").show().attr("disabled", false);
            });

            $("#edit_cancel").on("click", function () {
                $("#lead_info *").filter(":input").each(function () {
                    $(this).attr("readonly", true);
                });
                $("#edit_cancel").hide();
                $("#edit_info").show();
                $("#submit_info").hide().attr("disabled", true);
            })

            $(".ActionIcon").on("click", function () {
                let action = $(this).data('action');

                switch (action) {
                    case "init_call":
                        let call = confirm("Call this number?");
                        if (call) {
                            $("#dummy_phone_number").hide();
                            $("#hidden_number").show();
                            let trigger = $("#call_trigger").attr('href');
                            location.href = trigger;
                            $.get('/crm/update/lead/{{$lead->id}}/calls/create', function (data, status) {
                                console.log(status + " call logged")
                            })
                        }
                        break;
                    case "payment":
                        //TODO SOMETHING
                        break;
                    default:
                        console.log("no-action fired");
                        break;
                }
            })
        });
    </script>
@endsection


@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <p>You are logged in!</p>
                    <p>You have the following roles assigned:</p>
                    <p>
                    @forelse (Auth::user()->roles as $role)
                        <i>{{ $role->title }}</i><br>
                    @empty
                        <i>None</i>
                    @endforelse
                    </p>
                    <p>You can view the following tables:</p>
                    <ul>
                        @if (Auth::user()->isTeamLeader())
                        <li><a href="{{ route('crm.list',['list'=>'users']) }}">All Users in all teams</a></li>
                        <li><a href="{{ route('crm.list',['list'=>'leads']) }}">All Leads for your teams</a></li>
                        @elseif (Auth::user()->isAccountManager())
                        <li><a href="{{ route('crm.list',['list'=>'users']) }}">All Users in your team</a></li>
                        <li><a href="{{ route('crm.list',['list'=>'leads']) }}">All Leads for your team</a></li>
                        @else
                        <li><a href="{{ route('crm.list',['list'=>'leads']) }}">All your Leads</a></li>
                        @endif
                        <li><a href="{{ route('crm.list',['list'=>'open-pot']) }}">The open pot</a></li>
                    </ul>


                </div>
            </div>
        </div>
    </div>
</div>
@endsection

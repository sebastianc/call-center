@extends('layouts.app')

@section('content')


    <div class="container">

        @if( count($audits) > 0 )

            @foreach( $audits as $audit )
                @php($meta = $audit->getMetadata())
                <div class="card border-info mb-3">
                    <div class="card-header bg-info">
                        <h2 align="center">[{{$meta['audit_id']}}]
                            - {{date('dS M Y H:ia',strtotime($meta['audit_created_at'])) }}</h2>
                    </div>
                    <div class="card-body">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Key</th>
                                <th>Old Value</th>
                                <th>New Value</th>
                            </tr>
                            </thead>
                            <tbody>
                            @php( $modded = $audit->getModified() )
                            @foreach($modded as $key => $value )
                                <tr>
                                    <td><b>{{strtoupper($key)}}</b></td>
                                    <td><b>{{$value['old']}}</b></td>
                                    <td><b>{{$value['new']}}</b></td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="card-footer ">
                        <div align="center"><strong>{{$meta['user_name']}} via {{$meta['audit_url']}}
                                on {{$meta['audit_ip_address']}}</strong></div>
                    </div>
                </div>
            @endforeach
        @else
            <div align="center">
                <h2>No audit history available</h2>
            </div>
        @endif
    </div>



@endsection
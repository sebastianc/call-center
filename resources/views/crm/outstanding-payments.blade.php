@extends('crm.themes.'.config('crm.theme').'.layouts.app')

@section('content')

    <div class="container">

        <div class="row">

            <div class="col-sm-12">

                <div class="card">
                    @if(session()->has('success'))
                        <div class="alert alert-success">
                            {{ session()->get('success') }}
                        </div>
                    @endif
                        @if(session()->has('error'))
                            <div class="alert alert-danger">
                                {{ session()->get('error') }}
                            </div>
                        @endif
                    <div class="card-header bg-dark text-light">
                        <div class="card-title">
                            <h2>Outstanding Payments</h2>
                        </div>


                    </div>

                    <div class="card-body">
                        <table class="table table-striped table-hover table-bordered" style="text-align: center;">
                            <thead>
                            <tr>
                                <th>Lead ID</th>
                                <th>Item</th>
                                <th>Original Sale Value (excl VAT)</th>
                                <th>Original Sale Value (incl VAT)</th>
                                <th>Initial Payment (incl VAT)</th>
                                <th>Original Payment ID</th>
                                <th>Outstanding Amount (incl VAT)</th>
                                <th>Original Payment Date</th>
                                <th>Options</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($outstanding_interests as $interest)
                                <tr>
                                    <td>({{$interest->lead_id}})
                                        {{$interest->lead->first_name ." " . $interest->lead->last_name}}</td>
                                    <td>{{$interest->catalogueItem->title}}</td>
                                    <td>£{{number_format($interest->calculateCost(),2)}}</td>
                                    <td>£{{number_format($interest->sale_value,2)}}</td>
                                    <td>£{{number_format($interest->payment->amount,2)}}</td>
                                    <td>{{$interest->payment_id}}</td>
                                    <td>£{{number_format($interest->outstanding_value,2)}}</td>
                                    <td>{{date("d/m/Y g:ia",strtotime($interest->sold_on))}}</td>
                                    <td>
                                        <button class="btn btn-sm btn-dark text-light trigger_payment"
                                                data-lead-id="{{$interest->lead_id}}"
                                                data-payment-id="{{$interest->payment_id}}"
                                                data-lead-name="{{$interest->lead->first_name ." " . $interest->lead->last_name}}"
                                                data-outstanding-value="{{$interest->outstanding_value}}"
                                                data-interest-id="{{$interest->id}}">Update Payment
                                        </button>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                    </div>
                </div>

            </div>

        </div>

    </div>

    <!--payment modal--->
    <div class="modal fade" id="paymentModal" tabindex="-1" role="dialog" aria-labelledby="paymentModal"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Create Additional Payment</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="col-sm-12">
                        <form class="form" id="payment_form" action="{{route('payment.additional-payment')}}" method="POST">
                            {{csrf_field()}}
                            <div class="form-group">
                                <label for="paymentID">Original Payment ID</label>
                                <input class="form-control" type="text" id="paymentID" name="original_payment_id" value="" readonly>
                            </div>

                            <div class="form-group">
                                <label for="leadID">Lead ID</label>
                                <input class="form-control"  type="text" id="leadID" name="lead_id" value="" readonly="">
                            </div>

                            <div class="form-group">
                                <label for="lead-name">Lead Name</label>
                                <input class="form-control"  type="text" id="lead-name" name="lead-name" value="" readonly="">
                            </div>
                            <div class="form-group" style="display:none;">
                                <label for="old_user_id">Old User Id</label>
                                {{--<select class="form-control" id="user_id" name="user_id">
                                    @foreach($users as $user)
                                    <option value="{{$user->id}}">{{$user->name}}</option>
                                    @endforeach
                                </select>--}}
                                <input class="form-control" type="text" id="old_user_id" name="old_user_id" value="" readonly="">
                            </div>
                            <div class="form-group" style="display:none;">
                                <label for="interest_id">Interest Id</label>
                                <input class="form-control" type="text" id="interest_id" name="interest_id" value="" readonly="">
                            </div>

                            <div class="form-group">
                                <label for="paymentMethod">Payment Type</label>
                                <select class="form-control" id="paymentMethod" name="payment_method">
                                    @foreach($payment_types as $payment_type)
                                    <option value="{{$payment_type->id}}">{{$payment_type->title}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="amount">New Payment Amount £</label>
                                <input class="form-control" type="text" id="amount" name="amount" placeholder="Enter £££ Here" readonly="" required>
                            </div>

                            <div class="form-group" style="display:none;">
                                <label for="user_id">User performing current outstanding payment registration</label>
                                <input class="form-control" type="text" id="user_id" name="user_id" readonly="" required>
                            </div>

                            <div class="form-group">
                                <button class="btn btn-dark text-light btn-block">Process Additional Payment</button>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')
    <script>
        $(document).ready(function () {
            $(".trigger_payment").on('click', function () {
                let paymentID = $(this).data('payment-id');
                let leadID = $(this).data('lead-id');
                let leadName = $(this).data('lead-name');
                let outstanding = $(this).data('outstanding-value');
                let interestID = $(this).data('interest-id');
                let currentUserID = {{ $current_user }};
                $("#paymentID").val(paymentID);
                $("#leadID").val(leadID);
                $("#lead-name").val(leadName);
                $("#amount").val(outstanding);
                $("#interest_id").val(interestID);
                $("#user_id").val(currentUserID);
                $("#paymentModal").modal();
            });
        });
    </script>
@endsection
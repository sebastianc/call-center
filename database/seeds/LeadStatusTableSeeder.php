<?php

use Illuminate\Database\Seeder;

class LeadStatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table("lead_status")->truncate();
        DB::table("lead_status")->insert([
                [
                    'title' => 'Fresh',
                    'category' => 'Open',
                    'created_at' => Carbon\Carbon::now()->toDateTimeString()
                ],
                [
                    'title' => 'Recapture',
                    'category' => 'Open',
                    'created_at' => Carbon\Carbon::now()->toDateTimeString()
                ],
                [
                    'title' => 'Not Interested',
                    'category' => 'Delay',
                    'created_at' => Carbon\Carbon::now()->toDateTimeString()
                ],
                [
                    'title' => 'Too Junior',
                    'category' => 'Delay',
                    'created_at' => Carbon\Carbon::now()->toDateTimeString()
                ],
                [
                    'title' => 'Unwilling to talk',
                    'category' => 'Delay',
                    'created_at' => Carbon\Carbon::now()->toDateTimeString()
                ],
                [
                    'title' => 'Won\'t Pay',
                    'category' => 'Delay',
                    'created_at' => Carbon\Carbon::now()->toDateTimeString()
                ],
                [
                    'title' => 'Too Early',
                    'category' => 'Delay',
                    'created_at' => Carbon\Carbon::now()->toDateTimeString()
                ],
                [
                    'title' => 'Did not see value',
                    'category' => 'Delay',
                    'created_at' => Carbon\Carbon::now()->toDateTimeString()
                ],
                [
                    'title' => 'Not happy with service',
                    'category' => 'Delay',
                    'created_at' => Carbon\Carbon::now()->toDateTimeString()
                ],
                [
                    'title' => 'Put off by marketing',
                    'category' => 'Delay',
                    'created_at' => Carbon\Carbon::now()->toDateTimeString()
                ],
                [
                    'title' => 'Complaint',
                    'category' => 'Closed',
                    'created_at' => Carbon\Carbon::now()->toDateTimeString()
                ],
                [
                    'title' => 'Non Contactable',
                    'category' => 'Closed',
                    'created_at' => Carbon\Carbon::now()->toDateTimeString()
                ],
                [
                    'title' => 'Unsuitable Profession',
                    'category' => 'Closed',
                    'created_at' => Carbon\Carbon::now()->toDateTimeString()
                ],
                [
                    'title' => 'Retired',
                    'category' => 'Closed',
                    'created_at' => Carbon\Carbon::now()->toDateTimeString()
                ],
                [
                    'title' => 'Deceased',
                    'category' => 'Closed',
                    'created_at' => Carbon\Carbon::now()->toDateTimeString()
                ],
                [
                    'title' => 'Phone',
                    'category' => 'Complaint',
                    'created_at' => Carbon\Carbon::now()->toDateTimeString()
                ],
                [
                    'title' => 'Email',
                    'category' => 'Complaint',
                    'created_at' => Carbon\Carbon::now()->toDateTimeString()
                ],
                [
                    'title' => 'Other',
                    'category' => 'Complaint',
                    'created_at' => Carbon\Carbon::now()->toDateTimeString()
                ],
                [
                    'title' => 'Mis-sell',
                    'category' => 'Complaint',
                    'created_at' => Carbon\Carbon::now()->toDateTimeString()
                ],
                [
                    'title' => 'Job Pages',
                    'category' => 'Complaint',
                    'created_at' => Carbon\Carbon::now()->toDateTimeString()
                ],
                [
                    'title' => 'Website',
                    'category' => 'Complaint',
                    'created_at' => Carbon\Carbon::now()->toDateTimeString()
                ],
                [
                    'title' => 'Overpayment',
                    'category' => 'Complaint',
                    'created_at' => Carbon\Carbon::now()->toDateTimeString()
                ],
                [
                    'title' => 'Marketing',
                    'category' => 'Complaint',
                    'created_at' => Carbon\Carbon::now()->toDateTimeString()
                ],
                [
                    'title' => 'Online Reviews',
                    'category' => 'Complaint',
                    'created_at' => Carbon\Carbon::now()->toDateTimeString()
                ],
                [
                    'title' => 'Promised Refund',
                    'category' => 'Complaint',
                    'created_at' => Carbon\Carbon::now()->toDateTimeString()
                ],
                [
                    'title' => 'No Service',
                    'category' => 'Complaint',
                    'created_at' => Carbon\Carbon::now()->toDateTimeString()
                ],
                [
                    'title' => 'No Job',
                    'category' => 'Complaint',
                    'created_at' => Carbon\Carbon::now()->toDateTimeString()
                ],
                [
                    'title' => 'Changed Mind',
                    'category' => 'Complaint',
                    'created_at' => Carbon\Carbon::now()->toDateTimeString()
                ],
                [
                    'title' => 'Sales Calls',
                    'category' => 'Complaint',
                    'created_at' => Carbon\Carbon::now()->toDateTimeString()
                ],
                [
                    'title' => 'Chargeback',
                    'category' => 'Complaint',
                    'created_at' => Carbon\Carbon::now()->toDateTimeString()
                ],
                [
                    'title' => 'Profile',
                    'category' => 'Query',
                    'created_at' => Carbon\Carbon::now()->toDateTimeString()
                ],
                [
                    'title' => 'Website',
                    'category' => 'Query',
                    'created_at' => Carbon\Carbon::now()->toDateTimeString()
                ],
                [
                    'title' => 'Jobs',
                    'category' => 'Query',
                    'created_at' => Carbon\Carbon::now()->toDateTimeString()
                ],
                [
                    'title' => 'CV Service',
                    'category' => 'Query',
                    'created_at' => Carbon\Carbon::now()->toDateTimeString()
                ],
                [
                    'title' => 'Events',
                    'category' => 'Query',
                    'created_at' => Carbon\Carbon::now()->toDateTimeString()
                ],
                [
                    'title' => 'Profile',
                    'category' => 'Support Request',
                    'created_at' => Carbon\Carbon::now()->toDateTimeString()
                ],
                [
                    'title' => 'General',
                    'category' => 'Support Request',
                    'created_at' => Carbon\Carbon::now()->toDateTimeString()
                ],
                [
                    'title' => 'CV Service',
                    'category' => 'Support Request',
                    'created_at' => Carbon\Carbon::now()->toDateTimeString()
                ],
                [
                    'title' => 'Approve',
                    'category' => 'Refund Request',
                    'created_at' => Carbon\Carbon::now()->toDateTimeString()
                ],
                [
                    'title' => 'Decline',
                    'category' => 'Refund Request',
                    'created_at' => Carbon\Carbon::now()->toDateTimeString()
                ],
                [
                    'title' => '0-500',
                    'category' => 'Refund Request',
                    'created_at' => Carbon\Carbon::now()->toDateTimeString()
                ],
                [
                    'title' => '500-1000',
                    'category' => 'Refund Request',
                    'created_at' => Carbon\Carbon::now()->toDateTimeString()
                ],
                [
                    'title' => '> 1000',
                    'category' => 'Refund Request',
                    'created_at' => Carbon\Carbon::now()->toDateTimeString()
                ],
                [
                    'title' => 'Further Action',
                    'category' => 'Refund Request',
                    'created_at' => Carbon\Carbon::now()->toDateTimeString()
                ],
                [
                    'title' => 'Trust Pilot Requested',
                    'category' => 'Compliment',
                    'created_at' => Carbon\Carbon::now()->toDateTimeString()
                ],
                [
                    'title' => 'Trust Pilot Received',
                    'category' => 'Compliment',
                    'created_at' => Carbon\Carbon::now()->toDateTimeString()
                ],
                [
                    'title' => 'Sales',
                    'category' => 'Compliment',
                    'created_at' => Carbon\Carbon::now()->toDateTimeString()
                ],
                [
                    'title' => 'Customer Care',
                    'category' => 'Compliment',
                    'created_at' => Carbon\Carbon::now()->toDateTimeString()
                ],
                [
                    'title' => 'Website',
                    'category' => 'Compliment',
                    'created_at' => Carbon\Carbon::now()->toDateTimeString()
                ],
                [
                    'title' => 'NewCo. Membership',
                    'category' => 'Interest',
                    'created_at' => Carbon\Carbon::now()->toDateTimeString()
                ],
                [
                    'title' => 'CV Service',
                    'category' => 'Interest',
                    'created_at' => Carbon\Carbon::now()->toDateTimeString()
                ],
                [
                    'title' => 'Events',
                    'category' => 'Interest',
                    'created_at' => Carbon\Carbon::now()->toDateTimeString()
                ],
                [
                    'title' => 'Conference',
                    'category' => 'Interest',
                    'created_at' => Carbon\Carbon::now()->toDateTimeString()
                ],
                [
                    'title' => 'Webinars',
                    'category' => 'Interest',
                    'created_at' => Carbon\Carbon::now()->toDateTimeString()
                ],
                [
                    'title' => 'Psychometrics',
                    'category' => 'Interest',
                    'created_at' => Carbon\Carbon::now()->toDateTimeString()
                ],
                [
                    'title' => 'Call Back',
                    'category' => 'Action',
                    'created_at' => Carbon\Carbon::now()->toDateTimeString()
                ],
                [
                    'title' => 'Lead Passed - Engagement',
                    'category' => 'Action',
                    'created_at' => Carbon\Carbon::now()->toDateTimeString()
                ],
                [
                    'title' => 'Goodwill',
                    'category' => 'Action',
                    'created_at' => Carbon\Carbon::now()->toDateTimeString()
                ],
                [
                    'title' => 'Resolved - Happy',
                    'category' => 'Action',
                    'created_at' => Carbon\Carbon::now()->toDateTimeString()
                ],
                [
                    'title' => 'Resolved - Unhappy',
                    'category' => 'Action',
                    'created_at' => Carbon\Carbon::now()->toDateTimeString()
                ],
                [
                    'title' => 'Proactive Call',
                    'category' => 'Action',
                    'created_at' => Carbon\Carbon::now()->toDateTimeString()
                ],
                [
                    'title' => 'Unhappy - Lead Passed to Customer Care',
                    'category' => 'Delay',
                    'created_at' => Carbon\Carbon::now()->toDateTimeString()
                ],
                [
                    'title' => 'NewCo. Membership',
                    'category' => 'Not Interested',
                    'created_at' => Carbon\Carbon::now()->toDateTimeString()
                ],
                [
                    'title' => 'CV Service',
                    'category' => 'Not Interested',
                    'created_at' => Carbon\Carbon::now()->toDateTimeString()
                ],
                [
                    'title' => 'Events',
                    'category' => 'Not Interested',
                    'created_at' => Carbon\Carbon::now()->toDateTimeString()
                ],
                [
                    'title' => 'Conference',
                    'category' => 'Not Interested',
                    'created_at' => Carbon\Carbon::now()->toDateTimeString()
                ],
                [
                    'title' => 'Webinars',
                    'category' => 'Not Interested',
                    'created_at' => Carbon\Carbon::now()->toDateTimeString()
                ],
                [
                    'title' => 'Psychometrics',
                    'category' => 'Not Interested',
                    'created_at' => Carbon\Carbon::now()->toDateTimeString()
                ],
                [
                    'title' => 'Too Junior',
                    'category' => 'Unsuitable',
                    'created_at' => Carbon\Carbon::now()->toDateTimeString()
                ],
                [
                    'title' => 'Unsuitable Profession',
                    'category' => 'Unsuitable',
                    'created_at' => Carbon\Carbon::now()->toDateTimeString()
                ],
                [
                    'title' => 'Unwilling To Talk',
                    'category' => 'Unsuitable',
                    'created_at' => Carbon\Carbon::now()->toDateTimeString()
                ],
                [
                    'title' => 'TEST Record',
                    'category' => 'Unsuitable',
                    'created_at' => Carbon\Carbon::now()->toDateTimeString()
                ],
                [
                    'title' => 'Foreign Lead',
                    'category' => 'Uncontactable',
                    'created_at' => Carbon\Carbon::now()->toDateTimeString()
                ],
                [
                    'title' => 'Incorrect Number',
                    'category' => 'Uncontactable',
                    'created_at' => Carbon\Carbon::now()->toDateTimeString()
                ],
                [
                    'title' => 'No Longer Contactable',
                    'category' => 'Uncontactable',
                    'created_at' => Carbon\Carbon::now()->toDateTimeString()
                ],
                [
                    'title' => 'Won\'t Pay',
                    'category' => 'Renewals',
                    'created_at' => Carbon\Carbon::now()->toDateTimeString()
                ],
                [
                    'title' => 'Did Not See Value',
                    'category' => 'Renewals',
                    'created_at' => Carbon\Carbon::now()->toDateTimeString()
                ],
                [
                    'title' => 'Unhappy With Service',
                    'category' => 'Renewals',
                    'created_at' => Carbon\Carbon::now()->toDateTimeString()
                ],
                [
                    'title' => 'No Longer Applicable',
                    'category' => 'Renewals',
                    'created_at' => Carbon\Carbon::now()->toDateTimeString()
                ],
                [
                    'title' => 'Pursuing Different Career',
                    'category' => 'Renewals',
                    'created_at' => Carbon\Carbon::now()->toDateTimeString()
                ],
                [
                    'title' => 'Deceased',
                    'category' => 'Renewals',
                    'created_at' => Carbon\Carbon::now()->toDateTimeString()
                ],
                [
                    'title' => 'Retired',
                    'category' => 'Renewals',
                    'created_at' => Carbon\Carbon::now()->toDateTimeString()
                ],
                [
                    'title' => 'Successful - Not Needed',
                    'category' => 'Renewals',
                    'created_at' => Carbon\Carbon::now()->toDateTimeString()
                ],
                [
                    'title' => 'Conflict of Interest',
                    'category' => 'Renewals',
                    'created_at' => Carbon\Carbon::now()->toDateTimeString()
                ],
                [
                    'title' => 'Emigrated',
                    'category' => 'Renewals',
                    'created_at' => Carbon\Carbon::now()->toDateTimeString()
                ],
                [
                    'title' => 'Warmed Up',
                    'category' => 'Renewals',
                    'created_at' => Carbon\Carbon::now()->toDateTimeString()
                ],
                [
                    'title' => 'No Longer Contactable',
                    'category' => 'Renewals',
                    'created_at' => Carbon\Carbon::now()->toDateTimeString()
                ],
                [
                    'title' => 'Dead Lead',
                    'category' => 'Renewals',
                    'created_at' => Carbon\Carbon::now()->toDateTimeString()
                ],
                [
                    'title' => 'Discovery Call',
                    'category' => 'Ultimate Consultant',
                    'created_at' => Carbon\Carbon::now()->toDateTimeString()
                ],
                [
                    'title' => 'Proactive Call',
                    'category' => 'Ultimate Consultant',
                    'created_at' => Carbon\Carbon::now()->toDateTimeString()
                ],
                [
                    'title' => 'Profile Build',
                    'category' => 'Ultimate Consultant',
                    'created_at' => Carbon\Carbon::now()->toDateTimeString()
                ],
                [
                    'title' => 'CV Review',
                    'category' => 'Ultimate Consultant',
                    'created_at' => Carbon\Carbon::now()->toDateTimeString()
                ],
                [
                    'title' => 'Job Sent',
                    'category' => 'Ultimate Consultant',
                    'created_at' => Carbon\Carbon::now()->toDateTimeString()
                ],
                [
                    'title' => 'No status',
                    'category' => 'Open',
                    'created_at' => Carbon\Carbon::now()->toDateTimeString()
                ],
                [
                    'title' => 'Due for Renewal',
                    'category' => 'Renewals',
                    'created_at' => Carbon\Carbon::now()->toDateTimeString()
                ],
            ]
        );
    }
}

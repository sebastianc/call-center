<?php

use Illuminate\Database\Seeder;

class NetworkTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table("network")->truncate();
        DB::table("network")->insert([
                [
                    'name' => 'NewCo. Networks',
                    'abbreviation' => 'ITN'
                ],
                [
                    'name' => 'Products',
                    'abbreviation' => 'Product'
                ],[
                    'name' => 'Executive Coaching',
                    'abbreviation' => 'Exec-coaching'
                ],
            ]
        );
    }
}

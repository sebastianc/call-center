<?php

use Illuminate\Database\Seeder;

class CatalogueItemTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table("catalogue_items")->truncate();
        DB::table("catalogue_items")->insert([
                // Memberships
                [
                    'title' => 'NewCo. Networks 1 Year Membership',
                    'type' => 1,
                    'price' => 500.00,
                    'created_at' => date('Y-m-d H:i:s', time()),
                    'legacy_id' => 7,
                    'legacy_type' => 1,
                ],
                [
                    'title' => 'NewCo. Networks 3 Year Membership',
                    'type' => 1,
                    'price' => 1000.00,
                    'created_at' => date('Y-m-d H:i:s', time()),
                    'legacy_id' => 7,
                    'legacy_type' => 1,
                ],
                [
                    'title' => 'NewCo. Networks 1 Month Membership Subscription',
                    'type' => 2,
                    'price' => 60.00,
                    'created_at' => date('Y-m-d H:i:s', time()),
                    'legacy_id' => 7,
                    'legacy_type' => 1,
                ],
                // Products
                [
                    'title' => 'Evening Event',
                    'type' => 1,
                    'price' => 75.00,
                    'created_at' => date('Y-m-d H:i:s', time()),
                    'legacy_id' => 22,
                    'legacy_type' => 2,
                ],
                [
                    'title' => 'Conference',
                    'type' => 1,
                    'price' => 450.00,
                    'created_at' => date('Y-m-d H:i:s', time()),
                    'legacy_id' => 46,
                    'legacy_type' => 2,
                ],
                [
                    'title' => 'Covering letter	',
                    'type' => 1,
                    'price' => 80.00,
                    'created_at' => date('Y-m-d H:i:s', time()),
                    'legacy_id' => 0,
                    'legacy_type' => 2,
                ],
                [
                    'title' => 'CV review',
                    'type' => 1,
                    'price' => 250.00,
                    'created_at' => date('Y-m-d H:i:s', time()),
                    'legacy_id' => 1,
                    'legacy_type' => 2,
                ],
                [
                    'title' => 'CV re-write',
                    'type' => 1,
                    'price' => 650.00,
                    'created_at' => date('Y-m-d H:i:s', time()),
                    'legacy_id' => 27,
                    'legacy_type' => 2,
                ],
                [
                    'title' => 'ITN Profile Build',
                    'type' => 1,
                    'price' => 100.00,
                    'created_at' => date('Y-m-d H:i:s', time()),
                    'legacy_id' => 54,
                    'legacy_type' => 2,
                ],
                [
                    'title' => 'LinkedIn Profile Build	',
                    'type' => 1,
                    'price' => 450.00,
                    'created_at' => date('Y-m-d H:i:s', time()),
                    'legacy_id' => 48,
                    'legacy_type' => 2,
                ],
                [
                    'title' => 'Mentoring (non-member)',
                    'type' => 1,
                    'price' => 12995.00,
                    'created_at' => date('Y-m-d H:i:s', time()),
                    'legacy_id' => 53,
                    'legacy_type' => 2,
                ],
                [
                    'title' => 'Mentoring (member)',
                    'type' => 1,
                    'price' => 9995.00,
                    'created_at' => date('Y-m-d H:i:s', time()),
                    'legacy_id' => 53,
                    'legacy_type' => 2,
                ],
                [
                    'title' => 'Mentoring Lite (non-member)',
                    'type' => 1,
                    'price' => 7995.00,
                    'created_at' => date('Y-m-d H:i:s', time()),
                    'legacy_id' => 62,
                    'legacy_type' => 2,
                ],
                [
                    'title' => 'Mentoring Lite (member)',
                    'type' => 62,
                    'price' => 5995.00,
                    'created_at' => date('Y-m-d H:i:s', time()),
                    'legacy_id' => 1,
                    'legacy_type' => 2,
                ],
                [
                    'title' => 'Ahead of the Curve (non-member)',
                    'type' => 1,
                    'price' => 1449.00,
                    'created_at' => date('Y-m-d H:i:s', time()),
                    'legacy_id' => 56,
                    'legacy_type' => 2,
                ],
                [
                    'title' => 'Ahead of the Curve (member)',
                    'type' => 1,
                    'price' => 1225.00,
                    'created_at' => date('Y-m-d H:i:s', time()),
                    'legacy_id' => 56,
                    'legacy_type' => 2,
                ],
                [
                    'title' => 'Career Encore (non-member)',
                    'type' => 1,
                    'price' => 1000.00,
                    'created_at' => date('Y-m-d H:i:s', time()),
                    'legacy_id' => 50,
                    'legacy_type' => 2,
                ],
                [
                    'title' => 'Career Encore ( (member)',
                    'type' => 1,
                    'price' => 750.00,
                    'created_at' => date('Y-m-d H:i:s', time()),
                    'legacy_id' => 50,
                    'legacy_type' => 2,
                ],
                [
                    'title' => 'Confident Women Director (non-member)',
                    'type' => 1,
                    'price' => 650.00,
                    'created_at' => date('Y-m-d H:i:s', time()),
                    'legacy_id' => 52,
                    'legacy_type' => 2,
                ],
                [
                    'title' => 'Confident Women Director (member)',
                    'type' => 1,
                    'price' => 250.00,
                    'created_at' => date('Y-m-d H:i:s', time()),
                    'legacy_id' => 52,
                    'legacy_type' => 2,
                ],
                [
                    'title' => 'Behavioural Program (non-member)',
                    'type' => 1,
                    'price' => 500.00,
                    'created_at' => date('Y-m-d H:i:s', time()),
                    'legacy_id' => 0,
                    'legacy_type' => 2,
                ],
                [
                    'title' => 'Behavioural Program (member)',
                    'type' => 1,
                    'price' => 250.00,
                    'created_at' => date('Y-m-d H:i:s', time()),
                    'legacy_id' => 0,
                    'legacy_type' => 2,
                ]
            ]
        );
    }

}

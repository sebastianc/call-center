<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

use App\Team;
use App\Role;
use App\User;
use App\Lead;
use App\LeadStatus;
use App\CatalogueItem;
use App\Interest;
use App\Call;
use App\Note;

use Carbon\Carbon;

class RussTestFirstSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $account_manager    = Role::where('uri', 'account-manager')->firstOrFail();
        $product_manager    = Role::where('uri', 'product-manager')->firstOrFail();
        $membership_manager = Role::where('uri', 'membership-manager')->firstOrFail();
        $renewals_manager   = Role::where('uri', 'renewals-manager')->firstOrFail();
        $team_leader        = Role::where('uri', 'team-leader')->firstOrFail();

        $team = Team::updateOrCreate([
            'name'=>'UAT v1.0'
        ]);

        $russ = User::updateOrCreate([
            'email'  => 'russ.burt@NewCo.networks.com',
        ],[
            'name'     => 'Russ Burt',
            'active'   => 1,
            'password' => Hash::make('password')
        ]);
        $russ->roles()->sync($team_leader);
        $russ->teams()->sync($team);

        $rhys = User::updateOrCreate([
            'email'  => 'rhys.yates@NewCo.networks.com',
        ],[
            'name'     => 'Rhys Yates',
            'password' => Hash::make('password'),
            'active'   => 1
        ]);
        $rhys->roles()->sync($membership_manager);
        $rhys->teams()->sync($team);

        $jordan = User::updateOrCreate([
            'email'  => 'jordan.poole@NewCo.networks.com',
        ],[
            'name'     => 'Jordan Poole',
            'password' => Hash::make('password'),
            'active'   => 1
        ]);
        $jordan->roles()->sync($product_manager);
        $jordan->teams()->sync($team);

        $jonathan = User::updateOrCreate([
            'email'  => 'jonathan.halford@NewCo.networks.com',
        ],[
            'name'     => 'Jonathan Halford',
            'active'   => 1,
            'password' => Hash::make('password')
        ]);
        $jonathan->roles()->sync($renewals_manager);
        $jonathan->teams()->sync($team);

        /**
         * Now what? Give each user five leads...?
         * That works fine for MM and PM. What about the RM (Jonathan)?
         * Do we create some leads who have purchased a membership
         * twelve months or so earlier?
         */

        $statuses    = [];
        $statuses['fresh']     = LeadStatus::where('title', 'Fresh')->firstOrFail();
        $statuses['recapture'] = LeadStatus::where('title', 'Recapture')->firstOrFail();

        $catalogue_items = [];
        $catalogue_items['membership']    = CatalogueItem::where('title', 'NewCo. Networks 1 Year Membership')->firstOrFail();
        $catalogue_items['cv-review']     = CatalogueItem::where('title', 'CV review')->firstOrFail();

        /**
         * Rhys can have five Simpsons, all fresh, with a membership interest set for Homer
         */
        $lead = Lead::updateOrCreate([
            'email_address' => 'homer.simpson@thesimpsons.com',
        ], [
            'first_name'     => 'Homer',
            'last_name'      => 'Simpson',
            'phone_number'   => '07555 555 555',
            'team_id'        => $team->id,
            'user_id'        => $rhys->id,
            'lead_status_id' => $statuses['fresh']->id,
            'created_at'     => Carbon::now()->subHours(24)->toDateTimeString()
        ]);

        $interest = Interest::updateOrCreate([
            'lead_id' => $lead->id,
            'catalogue_item_id' => $catalogue_items['membership']->id
        ], [
            'interest_percentage' => 50,
            'created_at'          => $lead->created_at
        ]);

        $lead = Lead::updateOrCreate([
            'email_address' => 'marge.simpson@thesimpsons.com',
        ], [
            'first_name'     => 'Marge',
            'last_name'      => 'Simpson',
            'phone_number'   => '07555 555 555',
            'team_id'        => $team->id,
            'user_id'        => $rhys->id,
            'lead_status_id' => $statuses['fresh']->id,
            'created_at'     => Carbon::now()->subHours(20)->toDateTimeString()
        ]);
        $lead = Lead::updateOrCreate([
            'email_address' => 'bart.simpson@thesimpsons.com',
        ], [
            'first_name'     => 'Bart',
            'last_name'      => 'Simpson',
            'phone_number'   => '07555 555 555',
            'team_id'        => $team->id,
            'user_id'        => $rhys->id,
            'lead_status_id' => $statuses['fresh']->id,
            'created_at'     => Carbon::now()->subHours(16)->toDateTimeString()
        ]);
        $lead = Lead::updateOrCreate([
            'email_address' => 'lisa.simpson@thesimpsons.com',
        ], [
            'first_name'     => 'Lisa',
            'last_name'      => 'Simpson',
            'phone_number'   => '07555 555 555',
            'team_id'        => $team->id,
            'user_id'        => $rhys->id,
            'lead_status_id' => $statuses['fresh']->id,
            'created_at'     => Carbon::now()->subHours(12)->toDateTimeString()
        ]);
        $lead = Lead::updateOrCreate([
            'email_address' => 'maggie.simpson@thesimpsons.com',
        ], [
            'first_name'     => 'Maggie',
            'last_name'      => 'Simpson',
            'phone_number'   => '07555 555 555',
            'team_id'        => $team->id,
            'user_id'        => $rhys->id,
            'lead_status_id' => $statuses['fresh']->id,
            'created_at'     => Carbon::now()->subHours(8)->toDateTimeString()
        ]);

        /**
         * Jordan can have five Breaking Bad, all fresh, with a product interest set for Walter
         */
        $lead = Lead::updateOrCreate([
            'email_address' => 'walter.white@breakingbad.com',
        ], [
            'first_name'     => 'Walter',
            'last_name'      => 'White',
            'phone_number'   => '07555 555 555',
            'team_id'        => $team->id,
            'user_id'        => $jordan->id,
            'lead_status_id' => $statuses['fresh']->id,
            'created_at'     => Carbon::now()->subHours(24)->toDateTimeString()
        ]);

        $interest = Interest::updateOrCreate([
            'lead_id' => $lead->id,
            'catalogue_item_id' => $catalogue_items['membership']->id
        ], [
            'interest_percentage' => 50,
            'created_at'          => $lead->created_at
        ]);

        $lead = Lead::updateOrCreate([
            'email_address' => 'skyler.white@breakingbad.com',
        ], [
            'first_name'     => 'Skyler',
            'last_name'      => 'White',
            'phone_number'   => '07555 555 555',
            'team_id'        => $team->id,
            'user_id'        => $jordan->id,
            'lead_status_id' => $statuses['fresh']->id,
            'created_at'     => Carbon::now()->subHours(20)->toDateTimeString()
        ]);
        $lead = Lead::updateOrCreate([
            'email_address' => 'jesse.pinkman@breakingbad.com',
        ], [
            'first_name'     => 'Jesse',
            'last_name'      => 'Pinkman',
            'phone_number'   => '07555 555 555',
            'team_id'        => $team->id,
            'user_id'        => $jordan->id,
            'lead_status_id' => $statuses['fresh']->id,
            'created_at'     => Carbon::now()->subHours(16)->toDateTimeString()
        ]);
        $lead = Lead::updateOrCreate([
            'email_address' => 'hank.schrader@breakingbad.com',
        ], [
            'first_name'     => 'Hank',
            'last_name'      => 'Schrader',
            'phone_number'   => '07555 555 555',
            'team_id'        => $team->id,
            'user_id'        => $jordan->id,
            'lead_status_id' => $statuses['fresh']->id,
            'created_at'     => Carbon::now()->subHours(12)->toDateTimeString()
        ]);
        $lead = Lead::updateOrCreate([
            'email_address' => 'saul.goodman@breakingbad.com',
        ], [
            'first_name'     => 'Saul',
            'last_name'      => 'Goodman',
            'phone_number'   => '07555 555 555',
            'team_id'        => $team->id,
            'user_id'        => $jordan->id,
            'lead_status_id' => $statuses['fresh']->id,
            'created_at'     => Carbon::now()->subHours(8)->toDateTimeString()
        ]);

        /**
         * Now... Jonathan needs a couple GoT leads, but they're renewals so should have some history --
         * paid interests and ideally some calls and notes
         */
        $lead = Lead::updateOrCreate([
            'email_address' => 'cersei.lannister@gameofthrones.com',
        ], [
            'first_name'     => 'Cersei',
            'last_name'      => 'Lannister',
            'phone_number'   => '07555 555 555',
            'team_id'        => $team->id,
            'user_id'        => $jonathan->id,
            'lead_status_id' => $statuses['fresh']->id,
            'created_at'     => Carbon::now()->subHours(24)->toDateTimeString()
        ]);

        $interest = Interest::updateOrCreate([
            'lead_id' => $lead->id,
            'catalogue_item_id' => $catalogue_items['membership']->id
        ], [
            'interest_percentage' => 100,
            'sold'                => 1,
            'sale_value'          => $catalogue_items['membership']->price,
            'created_at'          => Carbon::now()->second(0)->subMonths(12)->toDateTimeString()
        ]);

        $call = Call::updateOrCreate([
            'user_id'    => $jonathan->id,
            'lead_id'    => $lead->id,
            'created_at' => Carbon::parse($interest->created_at)->subMinutes(10)->toDateTimeString(),
        ], [
            'ended_at'   => Carbon::parse($interest->ended_at)->subMinutes(6)->toDateTimeString(),
            'answered' => 1,
            'pitched' => 1,
        ]);

        $note = Note::updateOrCreate([
            'user_id' => $jonathan->id,
            'lead_id' => $lead->id,
            'created_at' => Carbon::parse($call->created_at)->addMinutes(1)->toDateTimeString(),
        ], [
            'text' => 'Customer is very keen to proceed with setting up a membership',
        ]);

        $lead = Lead::updateOrCreate([
            'email_address' => 'jaime.lannister@gameofthrones.com',
        ], [
            'first_name'     => 'Jaime',
            'last_name'      => 'Lannister',
            'phone_number'   => '07555 555 555',
            'team_id'        => $team->id,
            'user_id'        => $jonathan->id,
            'lead_status_id' => $statuses['fresh']->id,
            'created_at'     => Carbon::now()->subHours(18)->toDateTimeString()
        ]);

        $interest = Interest::updateOrCreate([
            'lead_id' => $lead->id,
            'catalogue_item_id' => $catalogue_items['membership']->id
        ], [
            'interest_percentage' => 100,
            'sold'                => 1,
            'sale_value'          => $catalogue_items['membership']->price,
            'created_at'          => Carbon::now()->second(0)->subMonths(10)->toDateTimeString()
        ]);

    }
}

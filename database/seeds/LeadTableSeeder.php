<?php
/**
 * Created by PhpStorm.
 * User: robertlemm
 * Date: 10/05/2018
 * Time: 08:47
 */

Use Illuminate\Database\Seeder;

class LeadTableSeeder extends Seeder
{

    public function run()
    {
        factory(\App\Lead::class,100)->create();
    }
}
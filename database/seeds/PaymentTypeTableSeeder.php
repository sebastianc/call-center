<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PaymentTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table("payment_type")->truncate();
        DB::table("payment_type")->insert([
            [
                'title' => 'Stripe',
                'created_at' => \Carbon\Carbon::now()->toDateTimeString()
            ],
            [
                'title' => 'BACS Payment',
                'created_at' => \Carbon\Carbon::now()->toDateTimeString()
            ],
            [
                'title' => 'Paypal',
                'created_at' => \Carbon\Carbon::now()->toDateTimeString()
            ],
        ]);
    }
}

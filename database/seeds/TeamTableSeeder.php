<?php
/**
 * Created by PhpStorm.
 * User: robertlemm
 * Date: 01/05/2018
 * Time: 10:09
 */

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TeamTableSeeder extends Seeder
{
    /**
     * Run the Team Seeder, extend to use each for longer function seeder
     *
     * @return void
     */
    public function run()
    {
        DB::table("teams")->truncate();
        DB::table("teams")->insert([
                [
                    'name' => 'Customer Care',
                ],
                [
                    'name' => 'Events',
                ],
                [
                    'name' => 'Finance',
                ],
                [
                    'name' => 'Jobs',
                ],
                [
                    'name' => 'Marketing',
                ],
                [
                    'name' => 'Sponsorship',
                ],
                [
                    'name' => 'Engagement',
                ],
                [
                    'name' => 'Sweeper',
                ],
                [
                    'name' => 'Renewals',
                ],
                [
                    'name' => 'TCH-IT',
                ],
                [
                    'name' => 'TCH-Management',
                ],
                [
                    'name' => 'TCH-Finance',
                ],
                [
                    'name' => 'TCH-Business',
                ],
                [
                    'name' => 'Office',
                ],
                [
                    'name' => 'USA',
                ],
                [
                    'name' => 'Admin',
                ],
                [
                    'name' => 'Development',
                ],

            ]
        );
    }
}
<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            RolesTableSeeder::class,
            TeamTableSeeder::class,
            // Truncates Users table, DO NOT USE !!!
            //UserTableSeeder::class,
            // Messes with Lead ids which are the real ones from live, DO NOT USE !!!
            //LeadTableSeeder::class,
            // Test data will not be used moving forward
            //NoteTableSeeder::class,
            NetworkTableSeeder::class,
            // Test data will not be used moving forward
            //InterestsTableSeeder::class,
            CatalogueItemTypeTableSeeder::class,
            CatalogueItemTableSeeder::class,
            AllocationTableSeeder::class,
            PaymentTypeTableSeeder::class,
            LeadStatusTableSeeder::class,
        ]);
    }
}

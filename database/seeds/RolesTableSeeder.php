<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table("roles")->truncate();
        DB::table("roles")->insert([
                [
                    'title' => 'Account Manager',
                    'uri'   => 'account-manager'
                ],
                [
                    'title' => 'Product Manager',
                    'uri'   => 'product-manager'
                ],
                [
                    'title' => 'Membership Manager',
                    'uri'   => 'membership-manager'
                ],
                [
                    'title' => 'Renewals Manager',
                    'uri'   => 'renewals-manager'
                ],
                [
                    'title' => 'Team Leader',
                    'uri'   => 'team-leader'
                ]
            ]
        );
    }
}

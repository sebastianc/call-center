<?php

use Illuminate\Database\Seeder;

class AllocationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Empty //
        \Illuminate\Support\Facades\DB::table("allocations")->truncate();

        $users = \App\User::all();
        $networks = \App\Network::all();
        foreach ($users as $user)
        {
            // make allocations for each
            foreach ($networks as $network)
            {
                \App\Allocation::create([
                    'user_id' => $user->id,
                    'network_id' => $network->id,
                    'allocation' => 5, // default
                    'allocated' => 0,
                    'enabled' => rand(0,1),
                ]);
            }
        }
    }
}

<?php
/**
 * Created by PhpStorm.
 * User: robertlemm
 * Date: 01/05/2018
 * Time: 10:12
 */

Use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    public function run()
    {
        $users = [
            ['first_name' => 'Aron', 'last_name' => 'James', 'legacy_id' => '224411'],
            ['first_name' => 'Lucretia', 'last_name' => 'Collier-Curtis', 'legacy_id' => '50'],
            ['first_name' => 'Jordan', 'last_name' => 'Poole', 'legacy_id' => '144278'],
            ['first_name' => 'Reno', 'last_name' => 'Minopoli', 'legacy_id' => '153794'],
            ['first_name' => 'Jonathan', 'last_name' => 'Halford', 'legacy_id' => '23'],
            ['first_name' => 'Greg', 'last_name' => 'Smith', 'legacy_id' => '10'],
            ['first_name' => 'Daniel', 'last_name' => 'Pietsch', 'legacy_id' => '176950'],
            ['first_name' => 'Calvino', 'last_name' => 'Elliott', 'legacy_id' => '185'],
            ['first_name' => 'Scott', 'last_name' => 'Watson', 'legacy_id' => '217306'],
            ['first_name' => 'Di', 'last_name' => 'Goodgroves', 'legacy_id' => '58'],
            ['first_name' => 'Paige', 'last_name' => 'Thomas', 'legacy_id' => '225604'],
            ['first_name' => 'Thomas', 'last_name' => 'Allcock', 'legacy_id' => '155793'],
            ['first_name' => 'Aled', 'last_name' => 'Evans', 'legacy_id' => '251468'],
            ['first_name' => 'leigh', 'last_name' => 'rostron', 'legacy_id' => '176945'],
            ['first_name' => 'Dan', 'last_name' => 'Lee', 'legacy_id' => '257293'],
            ['first_name' => 'Lisa', 'last_name' => 'McMahon', 'legacy_id' => '253278'],
            ['first_name' => 'Thomas', 'last_name' => 'Rogers', 'legacy_id' => '224410'],
            ['first_name' => 'Paul', 'last_name' => 'Berry', 'legacy_id' => '37'],
            ['first_name' => 'Emily', 'last_name' => 'Peach', 'legacy_id' => '217309'],
            ['first_name' => 'Rhys', 'last_name' => 'Yates', 'legacy_id' => '4'],
            ['first_name' => 'Michael', 'last_name' => 'Considine', 'legacy_id' => '48'],
            ['first_name' => 'Michael', 'last_name' => 'Rigby', 'legacy_id' => '47'],
            ['first_name' => 'Selina', 'last_name' => 'Shuttleworth', 'legacy_id' => '105'],
            ['first_name' => 'Eddie', 'last_name' => 'Ash', 'legacy_id' => '172967'],
            ['first_name' => 'Daniel', 'last_name' => 'Richardson', 'legacy_id' => '87'],
            ['first_name' => 'Andrew', 'last_name' => 'Nelson', 'legacy_id' => '9'],
            ['first_name' => 'Matthew', 'last_name' => 'Jones', 'legacy_id' => '8'],
            ['first_name' => 'James', 'last_name' => 'Platt', 'legacy_id' => '226297'],
            ['first_name' => 'Aidan', 'last_name' => 'Carter', 'legacy_id' => '231533'],
            ['first_name' => 'Garrick', 'last_name' => 'Bruce', 'legacy_id' => '231534'],
            ['first_name' => 'James', 'last_name' => 'Taylor', 'legacy_id' => '153851'],
            ['first_name' => 'Alec', 'last_name' => 'Harden-Henry	', 'legacy_id' => '255198'],
            ['first_name' => 'Gideon', 'last_name' => 'Filer', 'legacy_id' => '248487'],
            ['first_name' => 'Wayne', 'last_name' => 'Lenord', 'legacy_id' => '237411'],
            ['first_name' => 'Julian', 'last_name' => 'Butterworth', 'legacy_id' => '237412'],
            ['first_name' => 'Angilina', 'last_name' => 'Stylianou', 'legacy_id' => '231532'],
            ['first_name' => 'Nick', 'last_name' => 'Sugden', 'legacy_id' => '155737'],
            ['first_name' => 'Rebecca', 'last_name' => 'Fletcher', 'legacy_id' => '176'],
            ['first_name' => 'Ryan', 'last_name' => 'OBoyle', 'legacy_id' => '139'],
            ['first_name' => 'Daryl', 'last_name' => 'Richardson', 'legacy_id' => '243709'],
            ['first_name' => 'Perry', 'last_name' => 'Wakefield', 'legacy_id' => '258635'],
        ];
        App\User::truncate();
        DB::table('allocations')->truncate();
        DB::table("role_user")->truncate();
        DB::table("team_user")->truncate();

        foreach ($users as $user) {
            $newUser = new \App\User;
            $newUser->name = $user['first_name'] . " " . $user['last_name'];
            $newUser->email = strtolower($user['first_name']) . "." . strtolower($user['last_name']) . "@NewCo.networks.com";
            $newUser->password = \Illuminate\Support\Facades\Hash::make(strtoupper($user['first_name'][0]) . strtoupper($user['last_name'][0] . "!23456"));
            $newUser->image = "images/Sales-Module/default.png";
            $newUser->api_token = md5(microtime());
            $newUser->legacy_id = $user['legacy_id'];
            $newUser->active = 1;
            $newUser->save();

            $roles = App\Role::inRandomOrder()->take(rand(1, 4))->get();
            $newUser->roles()->attach($roles);

            $teams = App\Team::inRandomOrder()->take(1)->get();

            $newUser->teams()->attach($teams);
        }

        // Do API user
        $apiUser = new \App\User;
        $apiUser->name = "API USER";
        $apiUser->email = "api.do.not.delete@NewCo.networks.com";
        $apiUser->password = \Illuminate\Support\Facades\Hash::make('pokemon2018'); // No Idea
        $newUser->image = "images/Sales-Module/default.png";
        $newUser->api_token = "18a4b32d242debe5e675f7d9657a8d90"; // key for other services
        $newUser->legacy_id = 0;
        $newUser->active = 0;
        $newUser->save();


        /*  RANDOM SEEDER ---
         *  factory(\App\User::class, 25)->create()->each(function ($u) {
            $roles = App\Role::inRandomOrder()->take(rand(1, 4))->get();
            $u->roles()->attach($roles);

            $teams = App\Team::inRandomOrder()->take(1)->get();

            $u->teams()->attach($teams);

            $u->save();
        });
         */


    }
}
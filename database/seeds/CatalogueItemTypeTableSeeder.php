<?php

use Illuminate\Database\Seeder;

class CatalogueItemTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table("catalogue_item_types")->insert([
                [
                    'title' => 'Singular'
                ],
                [
                    'title' => 'Subscription',
                ]
            ]
        );
    }
}

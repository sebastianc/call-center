<?php
/**
 * Created by PhpStorm.
 * User: robertlemm
 * Date: 01/05/2018
 * Time: 09:53
 */

use Faker\Generator as Faker;

$factory->define(App\Team::class, function (Faker $faker)  {
    return [
        'name' => "Team ".strtoupper($faker->randomLetter)
    ];
});

<?php

use Faker\Generator as Faker;

$factory->define(App\Role::class, function (Faker $faker) {
    return [
        'title' => $faker->randomElement([
            'Account Manager',
            'Product Manager',
            'Membership Manager',
            'Renewals Manager',
            'Team Leader'
        ])
    ];
});

<?php
/**
 * Created by PhpStorm.
 * User: robertlemm
 * Date: 11/05/2018
 * Time: 14:42
 */

use Faker\Generator as Faker;


$factory->define(App\Interest::class, function (Faker $faker) {

    $carbon =  \Carbon\Carbon::now();

    return [
        'lead_id' => rand(1,100),
        'catalogue_item_id' => rand(1,6),
        'interest_percentage' => rand(1,100),
        'sold' => 0,
        'sale_value' => 0,
        'created_at' => $carbon->addDays(rand(1,90))->format("Y-m-d H:i:s"),
    ];
});
<?php
/**
 * Created by PhpStorm.
 * User: robertlemm
 * Date: 11/05/2018
 * Time: 14:31
 */

use Faker\Generator as Faker;


$factory->define(App\Note::class, function (Faker $faker) {

    $carbon =  \Carbon\Carbon::now();

    return [
        'text' => $faker->text(400),
        'note_type_id' => rand(1,3),
        'user_id' => rand(1,25),
        'lead_id' => rand(1,100),
        'interest_id' => rand(0,80),
        'created_at' => $carbon->addDays(rand(1,90))->format("Y-m-d H:i:s"),
    ];
});
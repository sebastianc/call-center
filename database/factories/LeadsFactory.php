<?php
/**
 * Created by PhpStorm.
 * User: robertlemm
 * Date: 10/05/2018
 * Time: 08:43
 */

use Faker\Generator as Faker;

$factory->define(App\Lead::class, function (Faker $faker)  {

    return [
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'email_address' => $faker->email,
        'phone_number'=> $faker->phoneNumber,
        /**
         * This will create leads that belong to a user in a team that isn't team_id;
         * not necessarily a problem but it will create slightly odd data.
         */
        'team_id' => rand(1,2),
        'user_id' => rand(1,20),
    ];
});

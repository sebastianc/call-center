<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCallbacksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('callbacks')) {
            Schema::create('callbacks', function (Blueprint $table) {
                $table->increments('id');
                $table->integer("user_id");
                $table->integer("lead_id");
                $table->timestamp("callback_time");
                $table->integer("is_priority")->default(0);
                $table->integer("is_qualified")->default(0);
                $table->timestamps();
                $table->dateTime("deleted_at")->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('callbacks');
    }
}

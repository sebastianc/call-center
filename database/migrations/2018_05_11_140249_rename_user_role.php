<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameUserRole extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('user_roles') && !Schema::hasTable('role_user')) {
            Schema::rename('user_roles', 'role_user');
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('role_user') && !Schema::hasTable('user_roles')) {
            Schema::rename('role_user', 'user_roles');
        }
    }
}

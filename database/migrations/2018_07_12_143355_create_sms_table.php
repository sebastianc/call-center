<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSmsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('sms')) {
            Schema::create('sms', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('lead_id')->index();
                $table->integer('user_id')->index();
                $table->string('phone_number')->default('')->nullable();
                $table->text('message')->nullable();
                $table->integer('reply_to')->index();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sms');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndexForLeadNotesMigration extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->index('legacy_id');
        });
        Schema::table('leads', function (Blueprint $table) {
            $table->index('member_id');
        });
        Schema::table('calls', function (Blueprint $table) {
            $table->index('user_id');
            $table->index('lead_id');
        });
        Schema::table('callbacks', function (Blueprint $table) {
            $table->index('user_id');
            $table->index('lead_id');
        });
        Schema::table('notes', function (Blueprint $table) {
            $table->index('user_id');
            $table->index('lead_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('users', 'legacy_id')) {
            Schema::table('users', function (Blueprint $table) {
                $table->dropIndex('legacy_id');
            });
        }
        if (Schema::hasColumn('leads', 'member_id')) {
            Schema::table('leads', function (Blueprint $table) {
                $table->dropIndex('member_id');
            });
        }
        if (Schema::hasColumn('calls', 'user_id') && Schema::hasColumn('calls', 'lead_id')) {
            Schema::table('calls', function (Blueprint $table) {
                $table->dropIndex('user_id');
                $table->dropIndex('lead_id');
            });
        }
        if (Schema::hasColumn('callbacks', 'user_id') && Schema::hasColumn('callbacks', 'lead_id')) {
            Schema::table('callbacks', function (Blueprint $table) {
                $table->dropIndex('user_id');
                $table->dropIndex('lead_id');
            });
        }
        if (Schema::hasColumn('notes', 'user_id') && Schema::hasColumn('notes', 'lead_id')) {
            Schema::table('notes', function (Blueprint $table) {
                $table->dropIndex('user_id');
                $table->dropIndex('lead_id');
            });
        }
    }
}

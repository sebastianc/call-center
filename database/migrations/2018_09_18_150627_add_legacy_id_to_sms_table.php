<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLegacyIdToSmsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('sms', 'legacy_id')) {
            Schema::table('sms', function (Blueprint $table) {
                $table->integer('legacy_id')->index();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('sms', 'legacy_id')) {
            Schema::table('sms', function (Blueprint $table) {
                $table->dropColumn('legacy_id');
            });
        }
    }
}

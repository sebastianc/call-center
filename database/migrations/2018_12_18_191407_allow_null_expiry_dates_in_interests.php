<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AllowNullExpiryDatesInInterests extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('interests', function (Blueprint $table) {
            $table->dateTime('expiry_date')->nullable()->change();
        });

        DB::table('interests')
            ->where('expiry_date', '0000-00-00 00:00:00')
            ->update(['expiry_date'=>null]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('interests', function (Blueprint $table) {
            // Difficult to roll this back as we can't change a column to a timestamp
        });
    }
}

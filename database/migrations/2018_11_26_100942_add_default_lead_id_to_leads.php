<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDefaultLeadIdToLeads extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('leads', 'lead_status_id')) {
            Schema::table('leads', function (Blueprint $table) {
                $table->integer('lead_status_id')->default(94)->change();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('leads', 'lead_status_id')) {
            Schema::table('leads', function (Blueprint $table) {
                $table->integer('lead_status_id')->default(0)->change();
            });
        }
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateLeadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('leads', 'renewal_manager_id')) {
            Schema::table("leads",function (Blueprint $table){
                $table->integer("renewal_manager_id")->after('user_id');
                $table->dateTime("callback")->after('renewal_manager_id');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('leads', 'renewal_manager_id')) {
            Schema::table("leads",function (Blueprint $table){
                $table->dropColumn("renewal_manager_id");
                $table->dropColumn("callback");
            });
        }
    }
}

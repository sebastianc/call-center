<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameCallhistoryTableToCalls extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('call_history') && !Schema::hasTable('calls')) {
            Schema::rename('call_history', 'calls');
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('calls') && !Schema::hasTable('call_history')) {
            Schema::rename('calls', 'call_history');
        }
    }
}

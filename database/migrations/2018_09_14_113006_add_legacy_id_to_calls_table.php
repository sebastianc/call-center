<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLegacyIdToCallsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('calls', 'legacy_id')) {
            Schema::table('calls', function (Blueprint $table) {
                $table->integer('legacy_id')->index();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('calls', 'legacy_id')) {
            Schema::table('calls', function (Blueprint $table) {
                $table->dropColumn('legacy_id');
            });
        }
    }
}

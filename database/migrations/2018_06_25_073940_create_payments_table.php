<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('payments')){
            Schema::create('payments', function (Blueprint $table) {
                $table->increments('id');
                $table->decimal("amount",10,2); // just in-case we sell a product for $million
                $table->boolean("paid");
                $table->integer("payment_type");
                $table->string("reference",48)->nullable();
                $table->string("unique_order_id",24);
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}

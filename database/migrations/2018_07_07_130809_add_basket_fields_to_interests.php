<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBasketFieldsToInterests extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('interests', function (Blueprint $table) {
            $table->dateTime('sold_on')->nullable();
            $table->boolean('in_basket')->default(false);
            $table->integer('discount_percentage')->nullable()->default(0);

            $table->boolean('sold')->nullable()->change();
            $table->decimal('sale_value',8,2)->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('interests', function (Blueprint $table) {
            $table->dropColumn('sold_on');
            $table->dropColumn('in_basket');
            $table->dropColumn('discount_percentage');
        });
    }
}

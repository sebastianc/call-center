<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUriToRoles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('roles', 'uri')) {
            Schema::table('roles', function (Blueprint $table) {
                $table->string('uri')->default('')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('roles', 'uri')) {
            Schema::table('roles', function (Blueprint $table) {
                $table->dropColumn('uri');
            });
        }
    }
}

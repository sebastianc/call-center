<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Addexpiryandrenewal extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('interests', function(Blueprint $table){
            if(!Schema::hasColumn('interests', 'expiry_date')) {
                $table->timestamp('expiry_date');
            }
            if(!Schema::hasColumn('interests', 'renewal')) {
                $table->tinyInteger('renewal')->default(0);
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('interests', function(Blueprint $table){
            $table->dropColumn('expiry_date');
            $table->dropColumn('renewal');
        });
    }
}

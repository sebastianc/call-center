<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSurveysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('surveys')) {
            Schema::create('surveys', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('lead_id')->index();

                $table->enum('sector', [
                    'Not-for-profit',
                    'Private',
                    'Public'
                ])->nullable()->default(null);
                $table->enum('expertise', [''])->nullable()->default(null);
                $table->enum('industries', [''])->nullable()->default(null);
                $table->enum('markets', [''])->nullable()->default(null);

                $table->enum('where_in_career', [
                    'Aspiring',
                    'Transitional',
                    'Established'
                ])->nullable()->default(null);

                $table->enum('require_first_role', [
                    'One Month',
                    'Two Months',
                    'Three Months',
                    'Four Months',
                    'Five Months',
                    'Six Months',
                    'Seven Months',
                    'Eight Months',
                    'Nine Months',
                    'Ten Months',
                    'Eleven Months',
                    'Twelve Months',
                    'More Than Twelve Months'
                ])->nullable()->default(null);

                $table->enum('expected_salary', [
                    '£30,000 - £50,000',
                    '£50,000 - £75,000',
                    '£75,000 - £100,000',
                    '£100,000 - £150,000',
                    '£150,000+'
                ])->nullable()->default(null);

                $table->enum('expected_day_rate', [
                    '£200',
                    '£250',
                    '£300',
                    '£350',
                    '£400',
                    '£450',
                    '£500',
                    '£550',
                    '£600',
                    '£650',
                    '£700',
                    '£750',
                    '£750 +'
                ])->nullable()->default(null);

                $table->enum('consecutive_roles', [
                    '1',
                    '2',
                    'A Portfolio'
                ])->nullable()->default(null);

                $table->enum('money_to_invest', [
                    'Yes',
                    'No'
                ])->nullable()->default(null);

                $table->enum('days_per_month', [
                    '1',
                    '2',
                    '3',
                    '4',
                    '5',
                    '6',
                    '7',
                    '8',
                    '9',
                    '10',
                    '11',
                    '12',
                    '13',
                    '14',
                    '15',
                    '16',
                    '17',
                    '18',
                    '19',
                    '20',
                    '21',
                    '22',
                    '23',
                    '24',
                    '25'
                ])->nullable()->default(null);

                $table->enum('currently_in_role', [
                    'Yes',
                    'No'
                ])->nullable()->default(null);

                $table->enum('vip', [
                    'Yes',
                    'No'
                ])->nullable()->default(null);

                $table->enum('networks_interested_in', [
                    'Non-Executive Directors',
                    'The Consultant Hub',
                    'Finance Director Network',
                    'Investor Director',
                    'Women Directors'
                ])->nullable()->default(null);

                $table->enum('type_of_role', [
                    'Non-Exec Role',
                    'Consultancy Role'
                ])->nullable()->default(null);

                $table->enum('training_and_development', [
                    'Ahead of the Curve',
                    'Online Training',
                    'Events'
                ])->nullable()->default(null);


                $table->timestamps();
            });

            /**
             * Add some SET values; not always great design practice but justifiable here I believe,
             * as it avoids the need for a large number of very specific related tables purely for surveys.
             * It's also unlikely that these options will change over time or need to be CMS-driven,
             * so it's legitimate to require development time for any such changes (ie, to update
             * the database schema, rather than allowing users to change these options themselves).
             */
            DB::statement("ALTER TABLE `surveys` CHANGE `expertise` `expertise` SET('Audit', 'Brand Management', 'Business Analyst', 'Business Development', 'Change Management', 'Charity / Non-Profit', 'Civil Engineering', 'Commercial', 'Communications', 'Compliance', 'CRM', 'Customer Service', 'Cyber Security', 'Data Protection', 'Defence', 'Design and Creative', 'Digital', 'Economics and Environmental Consulting', 'Engineering', 'Facilities Management', 'Finance / Accounting', 'Founder / Start-up', 'Fundraising', 'Governance', 'Health and Safety', 'HR Consulting', 'Infrastructure', 'Investment', 'IT / Software Development', 'Learning and Development', 'Legal', 'Management', 'Marketing and Sales', 'Medical', 'Network Consultant', 'Non-executive', 'Operations', 'Organisational Development', 'Outsourcing', 'Policy Development', 'Private', 'Procurement', 'Product Development', 'Project/Program Management', 'Public', 'Public / Patient Involvement', 'Quality Assurance', 'Quantity Surveyor', 'Real Estate / Property', 'Regulatory', 'Retail', 'Risk', 'Security', 'Site / Property Management', 'Social Care', 'Stakeholder Engagement', 'Strategy', 'Succession Planning', 'Supply Chain', 'Tax', 'Technology');");

            DB::statement("ALTER TABLE `surveys` CHANGE `industries` `industries` SET('Advertising', 'Marketing &amp; PR', 'Agriculture', 'Animal Welfare', 'Arts', 'Building and Maintenance', 'Business Transformation', 'Charity/Not for Profit', 'Chemicals', 'Community Improvement', 'Consultancy', 'Design &amp; Creative', 'Distribution/Logistics', 'Ecommerce', 'Education', 'Energy &amp; Utilities', 'Engineering', 'Entertainment', 'Financial Services', 'Fintech', 'FMCG', 'Food', 'Franchising', 'General Management', 'Government - Central and Local', 'Greentech/Cleantech', 'Healthcare and Pharma', 'Hospitality', 'Information Technology', 'Insurance', 'Investment', 'Legal', 'Manufacturing', 'Natural Resources', 'Printing / Publishing / Media', 'Private Equity &amp; Venture Capital', 'Professional Services', 'Property &amp; Construction', 'Recruitment', 'Retail', 'Science / Research', 'Social Care', 'Software', 'Sport', 'Leisure &amp; Travel', 'Sustainability', 'Technology',  'Transport', 'Automotive and Aviation');");

            DB::statement("ALTER TABLE `surveys` CHANGE `markets` `markets` SET('Africa', 'Asia', 'Australasia', 'China', 'Europe', 'North America', 'Russia', 'South America', 'UK');");
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('surveys');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditSmsTableInboundFlag extends Migration
{
    public function up()
    {
        if (!Schema::hasColumn('sms', 'inbound')) {
            Schema::table('sms', function (Blueprint $table) {
                $table->integer("inbound")->default(0)->after("message");
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sms', function (Blueprint $table) {
            $table->dropColumn("inbound");
        });
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveTeamLeaderIdFromTeam extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('team') && !Schema::hasTable('teams')) {
            Schema::rename('team', 'teams');
        }
        if (Schema::hasColumn('teams', 'team_leader_id')) {
            Schema::table('teams', function (Blueprint $table) {
                $table->dropColumn('team_leader_id');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('teams') && !Schema::hasTable('team')) {
            Schema::rename('teams', 'team');
        }
        if (!Schema::hasColumn('team', 'team_leader_id')) {
            Schema::table('team', function (Blueprint $table) {
                $table->integer("team_leader_id",false,false);
            });
        }
    }
}

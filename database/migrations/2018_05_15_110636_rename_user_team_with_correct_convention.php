<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameUserTeamWithCorrectConvention extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /**
         * Accepted convention (and the one Laravel assumes)
         * is singular_singular form in alphabetical order
         */
        if (Schema::hasTable('user_teams') && !Schema::hasTable('team_user')) {
            Schema::rename('user_teams', 'team_user');
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('team_user') && !Schema::hasTable('user_teams')) {
            Schema::rename('team_user', 'user_teams');
        }
    }
}

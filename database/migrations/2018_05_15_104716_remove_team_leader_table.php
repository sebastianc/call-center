<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveTeamLeaderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('team_leaders');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (!Schema::hasTable('team_leaders')) {
            Schema::create('team_leaders', function (Blueprint $table) {
                $table->increments('id');
                $table->string("first_name",100);
                $table->string("last_name",100);
                $table->timestamps();
            });
        }
    }
}

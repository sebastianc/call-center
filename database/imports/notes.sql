INSERT INTO
  `__NEW_DATABASE__`.`notes`
  (
    `text`,
    `note_type_id`,
    `user_id`,
    `lead_id`,
    `interest_id`,
    `created_at`,
    `legacy_id`
  )
  SELECT
    `__OLD_DATABASE__`.`communication_note`.`note`,
    1 AS note_type_id,
    `__OLD_DATABASE__`.`communication_note`.`staff_id`,
    `__OLD_DATABASE__`.`communication_note`.`person_id`,
    0 AS interest_id,
    `__OLD_DATABASE__`.`communication_note`.`created_at`,
    `__OLD_DATABASE__`.`communication_note`.`id`
    FROM `__OLD_DATABASE__`.`communication_note`
    WHERE NOT EXISTS (
      SELECT `__NEW_DATABASE__`.`notes`.`id` FROM `__NEW_DATABASE__`.`notes`
        WHERE `__NEW_DATABASE__`.`notes`.`legacy_id` = `__OLD_DATABASE__`.`communication_note`.`id`
      )
    GROUP BY `__OLD_DATABASE__`.`communication_note`.`id` ORDER BY `__OLD_DATABASE__`.`communication_note`.`id` ASC;

UPDATE `__NEW_DATABASE__`.`sms` AS sms
  LEFT JOIN (
    SELECT `id`, `member_id` FROM `__NEW_DATABASE__`.`leads`
  ) AS leads ON sms.`lead_id` = leads.`member_id`
  SET sms.`lead_id` = IFNULL(leads.`id`, 999666999)
  WHERE NOT EXISTS (
    SELECT `__NEW_DATABASE__`.`leads`.`id` FROM `__NEW_DATABASE__`.`leads`
      WHERE sms.`lead_id` = `__NEW_DATABASE__`.`leads`.`id`
  );

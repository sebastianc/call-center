UPDATE `__NEW_DATABASE__`.`notes` AS notes
  LEFT JOIN (
    SELECT `id`, `member_id` FROM `__NEW_DATABASE__`.`leads`
  ) AS leads ON notes.`lead_id` = leads.`member_id`
  SET notes.`lead_id` = IFNULL(leads.`id`, 999666999)
  WHERE NOT EXISTS (
    SELECT `__NEW_DATABASE__`.`leads`.`id` FROM `__NEW_DATABASE__`.`leads`
      WHERE notes.`lead_id` = `__NEW_DATABASE__`.`leads`.`id`
  );

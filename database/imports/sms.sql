INSERT INTO
  `__NEW_DATABASE__`.`sms`
  (
    `phone_number`,
    `message`,
    `created_at`,
    `inbound`,
    `lead_id`,
    `user_id`,
    `read`,
    `legacy_id`
  )
  SELECT
    `__OLD_DATABASE__`.`communication_sms_message`.`phone_number`,
    `__OLD_DATABASE__`.`communication_sms_message`.`message`,
    `__OLD_DATABASE__`.`communication_sms_message`.`created_at`,
    IF(`__OLD_DATABASE__`.`communication_sms_message`.`sms_type_id` = 1, 0, 1) AS inbound,
    IF(`__OLD_DATABASE__`.`communication_sms_message`.`sms_type_id` != 1, `__OLD_DATABASE__`.`communication_sms_message`.`recipient_id`, `__OLD_DATABASE__`.`communication_sms_message`.`sender_id`) AS lead_id,
    IF(`__OLD_DATABASE__`.`communication_sms_message`.`sms_type_id` != 1, `__OLD_DATABASE__`.`communication_sms_message`.`sender_id`, `__OLD_DATABASE__`.`communication_sms_message`.`recipient_id`) AS user_id,
    IF(`__OLD_DATABASE__`.`communication_sms_message`.`read_at` IS NULL, 0, 1) AS read_message,
    `__OLD_DATABASE__`.`communication_sms_message`.`id`
    FROM `__OLD_DATABASE__`.`communication_sms_message`
    WHERE NOT EXISTS (
      SELECT `__NEW_DATABASE__`.`sms`.`id` FROM `__NEW_DATABASE__`.`sms`
        WHERE `__NEW_DATABASE__`.`sms`.`legacy_id` = `__OLD_DATABASE__`.`communication_sms_message`.`id`
      )
    GROUP BY `__OLD_DATABASE__`.`communication_sms_message`.`id` ORDER BY `__OLD_DATABASE__`.`communication_sms_message`.`id` ASC;

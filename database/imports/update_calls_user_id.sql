UPDATE `__NEW_DATABASE__`.`calls` AS calls
  LEFT JOIN (
    SELECT `id`, `legacy_id` FROM `__NEW_DATABASE__`.`users`
  ) AS users ON calls.`user_id` = users.`legacy_id`
  SET calls.`user_id` = IFNULL(users.`id`, 13370)
  WHERE NOT EXISTS (
    SELECT `__NEW_DATABASE__`.`users`.`id` FROM `__NEW_DATABASE__`.`users`
      WHERE calls.`user_id` = `__NEW_DATABASE__`.`users`.`id`
  );

INSERT INTO
  `__NEW_DATABASE__`.`calls`
  (
    `user_id`,
    `lead_id`,
    `created_at`,
    `answered`,
    `pitched`,
    `legacy_id`
  )
  SELECT
    `__OLD_DATABASE__`.`communication_call`.`call_initiator`,
    `__OLD_DATABASE__`.`communication_call`.`call_receiver`,
    `__OLD_DATABASE__`.`communication_call`.`created_at`,
    (
      SELECT COUNT(`__OLD_DATABASE__`.`communication_call_log`.`id`) FROM `__OLD_DATABASE__`.`communication_call_log`
        WHERE `__OLD_DATABASE__`.`communication_call_log`.`call_id` = `__OLD_DATABASE__`.`communication_call`.`id`
          AND `__OLD_DATABASE__`.`communication_call_log`.`stage_id` = 2
    ) AS answers,
    (
      SELECT COUNT(`__OLD_DATABASE__`.`communication_call_log`.`id`) FROM `__OLD_DATABASE__`.`communication_call_log`
        WHERE `__OLD_DATABASE__`.`communication_call_log`.`call_id` = `__OLD_DATABASE__`.`communication_call`.`id`
          AND `__OLD_DATABASE__`.`communication_call_log`.`stage_id` = 6
    ) AS pitched,
    `__OLD_DATABASE__`.`communication_call`.`id`
    FROM `__OLD_DATABASE__`.`communication_call`
    WHERE NOT EXISTS (
      SELECT `__NEW_DATABASE__`.`calls`.`id` FROM `__NEW_DATABASE__`.`calls`
        WHERE `__NEW_DATABASE__`.`calls`.`legacy_id` = `__OLD_DATABASE__`.`communication_call`.`id`
      )
    ORDER BY `__OLD_DATABASE__`.`communication_call`.`id` ASC;

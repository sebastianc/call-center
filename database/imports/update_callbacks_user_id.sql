UPDATE `__NEW_DATABASE__`.`callbacks` AS callbacks
  LEFT JOIN (
    SELECT `id`, `legacy_id` FROM `__NEW_DATABASE__`.`users`
  ) AS users ON callbacks.`user_id` = users.`legacy_id`
  SET callbacks.`user_id` = IFNULL(users.`id`, 13370)
  WHERE NOT EXISTS (
    SELECT `__NEW_DATABASE__`.`users`.`id` FROM `__NEW_DATABASE__`.`users`
      WHERE callbacks.`user_id` = `__NEW_DATABASE__`.`users`.`id`
  );

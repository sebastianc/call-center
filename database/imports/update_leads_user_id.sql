UPDATE `__NEW_DATABASE__`.`leads` AS leads
  LEFT JOIN (
    SELECT `id`, `legacy_id` FROM `__NEW_DATABASE__`.`users`
  ) AS users ON leads.`user_id` = users.`legacy_id`
  SET leads.`user_id` = IFNULL(users.`id`, 13370)
  WHERE NOT EXISTS (
    SELECT `__NEW_DATABASE__`.`users`.`id` FROM `__NEW_DATABASE__`.`users`
      WHERE leads.`user_id` = `__NEW_DATABASE__`.`users`.`id`
  );

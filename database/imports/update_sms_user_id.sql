UPDATE `__NEW_DATABASE__`.`sms` AS sms
  LEFT JOIN (
    SELECT `id`, `legacy_id` FROM `__NEW_DATABASE__`.`users`
  ) AS users ON sms.`user_id` = users.`legacy_id`
  SET sms.`user_id` = IFNULL(users.`id`, 13370)
  WHERE NOT EXISTS (
    SELECT `__NEW_DATABASE__`.`users`.`id` FROM `__NEW_DATABASE__`.`users`
      WHERE sms.`user_id` = `__NEW_DATABASE__`.`users`.`id`
  );

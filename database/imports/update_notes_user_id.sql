UPDATE `__NEW_DATABASE__`.`notes` AS notes
  LEFT JOIN (
    SELECT `id`, `legacy_id` FROM `__NEW_DATABASE__`.`users`
  ) AS users ON notes.`user_id` = users.`legacy_id`
  SET notes.`user_id` = IFNULL(users.`id`, 13370)
  WHERE NOT EXISTS (
    SELECT `__NEW_DATABASE__`.`users`.`id` FROM `__NEW_DATABASE__`.`users`
      WHERE notes.`user_id` = `__NEW_DATABASE__`.`users`.`id`
  );

INSERT INTO
  `__NEW_DATABASE__`.`users`
  (
    `name`,
    `email`,
    `legacy_id`,
    `active`,
    `created_at`,
    `updated_at`,
    `deleted_at`
  )
  SELECT
    CONCAT(`__OLD_DATABASE__`.`person`.`first_name`, " ", `__OLD_DATABASE__`.`person`.`last_name`) AS full_name,
    `__OLD_DATABASE__`.`authentication`.`email_address`,
    `__OLD_DATABASE__`.`person`.`id`,
    IF(`__OLD_DATABASE__`.`person`.`deleted_at` IS NULL, 1, 0),
    `__OLD_DATABASE__`.`person`.`created_at`,
    `__OLD_DATABASE__`.`person`.`updated_at`,
    `__OLD_DATABASE__`.`person`.`deleted_at`
  FROM `__OLD_DATABASE__`.`person`
  INNER JOIN `__OLD_DATABASE__`.`authentication` ON `__OLD_DATABASE__`.`person`.`id` = `__OLD_DATABASE__`.`authentication`.`person_id`
  WHERE `__OLD_DATABASE__`.`person`.`type` = 2
    AND `__OLD_DATABASE__`.`person`.`deleted_at` IS NULL
    AND (
      SELECT COUNT(`__OLD_DATABASE__`.`sales_staff`.`id`) FROM `__OLD_DATABASE__`.`sales_staff`
        WHERE `__OLD_DATABASE__`.`sales_staff`.`staff_id` = `__OLD_DATABASE__`.`person`.`id`
    ) > 0
    AND
      NOT EXISTS (
        SELECT `__NEW_DATABASE__`.`users`.`id` FROM `__NEW_DATABASE__`.`users`
          WHERE `__NEW_DATABASE__`.`users`.`legacy_id` = `__OLD_DATABASE__`.`person`.`id`
      )
  GROUP BY `__OLD_DATABASE__`.`authentication`.`email_address` ORDER BY `__OLD_DATABASE__`.`person`.`id` ASC;

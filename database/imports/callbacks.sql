INSERT INTO
  `__NEW_DATABASE__`.`callbacks`
  (
    `user_id`,
    `lead_id`,
    `callback_time`,
    `is_priority`,
    `is_qualified`,
    `created_at`,
    `legacy_id`
  )
  SELECT
    `__OLD_DATABASE__`.`communication_callback`.`staff_id`,
    `__OLD_DATABASE__`.`communication_callback`.`person_id`,
    `__OLD_DATABASE__`.`communication_callback`.`scheduled_date`,
    `__OLD_DATABASE__`.`communication_callback`.`priority`,
    0 AS qualified,
    `__OLD_DATABASE__`.`communication_callback`.`created_at`,
    `__OLD_DATABASE__`.`communication_callback`.`id`
    FROM `__OLD_DATABASE__`.`communication_callback`
    WHERE NOT EXISTS (
      SELECT `__NEW_DATABASE__`.`callbacks`.`id` FROM `__NEW_DATABASE__`.`callbacks`
        WHERE `__NEW_DATABASE__`.`callbacks`.`legacy_id` = `__OLD_DATABASE__`.`communication_callback`.`id`
      )
    ORDER BY `__OLD_DATABASE__`.`communication_callback`.`id` ASC;

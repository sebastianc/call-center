INSERT INTO
  `__NEW_DATABASE__`.`leads`
  (
    `id`,
    `first_name`,
    `last_name`,
    `email_address`,
    `phone_number`,
    `user_id`,
    `lead_status_id`,
    `member_id`,
    `team_id`,
    `created_at`,
    `updated_at`
  )
  SELECT
    `__OLD_DATABASE__`.`person`.`id`,
    `__OLD_DATABASE__`.`person`.`first_name`,
    `__OLD_DATABASE__`.`person`.`last_name`,
    `__OLD_DATABASE__`.`authentication`.`email_address`,
    `__OLD_DATABASE__`.`person`.`phone_number`,
    IFNULL((SELECT staff.id
      FROM `__OLD_DATABASE__`.`communication_callback`
        LEFT JOIN `__OLD_DATABASE__`.`person` AS staff
          ON `__OLD_DATABASE__`.`communication_callback`.`staff_id` = staff.id
      WHERE `__OLD_DATABASE__`.`communication_callback`.`person_id` = `__OLD_DATABASE__`.`person`.`id`
        AND `__OLD_DATABASE__`.`communication_callback`.`scheduled_date` >= NOW()
      ORDER BY `__OLD_DATABASE__`.`communication_callback`.`created_at` DESC
      LIMIT 1), IFNULL((SELECT `__OLD_DATABASE__`.`communication_call`.`call_initiator`
        FROM `__OLD_DATABASE__`.`communication_call`
          LEFT JOIN `__OLD_DATABASE__`.`person` AS staff
            ON `__OLD_DATABASE__`.`communication_call`.`call_initiator` = staff.id
        WHERE `__OLD_DATABASE__`.`communication_call`.`call_receiver` = `__OLD_DATABASE__`.`person`.`id`
        ORDER BY `__OLD_DATABASE__`.`communication_call`.`created_at` DESC
        LIMIT 1), NULL)) AS owner_id,
    IFNULL((SELECT
      CASE cs.name
      WHEN 'Overpayment' THEN 22
      WHEN 'Online Reviews' THEN 24
      WHEN 'Mis-sell' THEN 19
      WHEN (cs.name = 'Website' AND cs.communication_tag_id = 13) THEN 21
      WHEN (cs.name = 'Website' AND cs.communication_tag_id = 2) THEN 32
      WHEN (cs.name = 'Website' AND cs.communication_tag_id = 5) THEN 49
      WHEN 'No Job' THEN 27
      WHEN 'Job Pages' THEN 20
      WHEN 'Approve' THEN 39
      WHEN '> 1000' THEN 43
      WHEN 'Changed Mind' THEN 28
      WHEN 'Conflict of Interest' THEN 84
      WHEN 'Resolved - Unhappy' THEN 60
      WHEN 'Other ' THEN 18
      WHEN 'Put Off By Marketing' THEN 10
      WHEN 'Sales Calls' THEN 29
      WHEN 'Marketing' THEN 23
      WHEN 'Decline' THEN 40
      WHEN 'Unsuitable Profession' THEN 70
      WHEN 'Further Action' THEN 44
      WHEN '500-1000' THEN 42
      WHEN 'Emigrated' THEN 85
      WHEN 'Too Junior' THEN 69
      WHEN 'Foreign Lead' THEN 73
      WHEN 'Did Not See Value' THEN 77
      WHEN 'Deceased' THEN 81
      WHEN '0-500' THEN 41
      WHEN 'Successful - Not Needed' THEN 83
      WHEN 'Unhappy - Lead Passed to Customer Care' THEN 62
      WHEN 'Phone' THEN 16
      WHEN 'No Longer Applicable' THEN 79
      WHEN 'Unhappy With Service' THEN 78
      WHEN 'Too Early' THEN 7
      WHEN 'Warmed Up' THEN 86
      WHEN 'Pursuing Different Career' THEN 80
      WHEN (cs.name = 'Won''t Pay' AND cs.communication_tag_id = 8) THEN 6
      WHEN (cs.name = 'Won''t Pay' AND cs.communication_tag_id = 12) THEN 76
      WHEN 'TEST Record' THEN 72
      WHEN 'Unwilling To Talk' THEN 71
      WHEN 'Dead Lead' THEN 88
      WHEN 'Retired' THEN 82
      WHEN 'No Service' THEN 26
      WHEN (cs.name = 'No Longer Contactable' AND cs.communication_tag_id = 11) THEN 75
      WHEN (cs.name = 'No Longer Contactable' AND cs.communication_tag_id = 12) THEN 87
      WHEN 'Incorrect Number' THEN 74
      ELSE 94
      END
      FROM `__OLD_DATABASE__`.`communication_note_tag`
        INNER JOIN `__OLD_DATABASE__`.`communication_tag` AS t
          ON `__OLD_DATABASE__`.`communication_note_tag`.`communication_tag_id` = t.id
        INNER JOIN `__OLD_DATABASE__`.`communication_subtag` AS cs
          ON `__OLD_DATABASE__`.`communication_note_tag`.`communication_subtag_id` = cs.id
      WHERE `__OLD_DATABASE__`.`communication_note_tag`.`person_id` = `__OLD_DATABASE__`.`person`.`id`
        AND cs.communication_disposition_code_id IS NOT NULL
      ORDER BY `__OLD_DATABASE__`.`communication_note_tag`.`created_at` DESC
      LIMIT 1), 94) AS closedReason,
    `__OLD_DATABASE__`.`person`.`id`,
    0 AS team_id,
    `__OLD_DATABASE__`.`person`.`created_at`,
    `__OLD_DATABASE__`.`person`.`updated_at`
    FROM `__OLD_DATABASE__`.`person`
      LEFT JOIN `__OLD_DATABASE__`.`authentication` ON `__OLD_DATABASE__`.`person`.`id` = `__OLD_DATABASE__`.`authentication`.`person_id`
    WHERE `__OLD_DATABASE__`.`person`.`type` IN (1, 3)
      AND `__OLD_DATABASE__`.`person`.`deleted_at` IS NULL
      AND
        NOT EXISTS (
          SELECT `__NEW_DATABASE__`.`leads`.`id` FROM `__NEW_DATABASE__`.`leads`
            WHERE `__NEW_DATABASE__`.`leads`.`member_id` = `__OLD_DATABASE__`.`person`.`id`
        )
    GROUP BY `__OLD_DATABASE__`.`person`.`id` ORDER BY `__OLD_DATABASE__`.`person`.`id` ASC;

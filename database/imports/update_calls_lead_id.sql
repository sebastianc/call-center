UPDATE `__NEW_DATABASE__`.`calls` AS calls
  LEFT JOIN (
    SELECT `id`, `member_id` FROM `__NEW_DATABASE__`.`leads`
  ) AS leads ON calls.`lead_id` = leads.`member_id`
  SET calls.`lead_id` = IFNULL(leads.`id`, 999666999)
  WHERE NOT EXISTS (
    SELECT `__NEW_DATABASE__`.`leads`.`id` FROM `__NEW_DATABASE__`.`leads`
      WHERE calls.`lead_id` = `__NEW_DATABASE__`.`leads`.`id`
  );

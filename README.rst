Back Office
===========

Back Office application.

Usage
-----

$ composer install
$ php artisan serve --host=0.0.0.0 --port=8100

Notes
-----
php artisan migrate:refresh --seed

Queue Notes
-----------

On dev - ec2, install supervisord & copy supervisor.conf to /etc/supervisord.d/

On Live - use a similar path and add supervisor to the container

Model de sql grupat pe tipuri de continut incrementabil al unei proprietati de obiect

class GridIal extends BaseGrid
{
    protected $comparable = true;
    protected $compareKey = 'latency_orders_band';
    protected $compareName = 'Latency by order band';
    protected $name = 'Latency by order bands';

    protected function prepareColumns()
    {

        $this->addColumn('band', 'number', [
            'header' => 'Band',
            'hint' => 'Used only to control column sorting. Click this to rank in ascending or descending order of hurdle band size.'

        ]);

        $this->addColumn('orders_band', 'text', [
            'header' => 'Hurdle band',
            'hint' => 'The band of hurdle points, i.e. 6-10 orders. These are clustered together to allow analysis on massive data sets.'
        ]);

        $this->addColumn('average_latency', 'number', [
            'header' => 'Average latency',
            'hint' => 'Average intra-activity latency (IAL) in days denoting the average number of days between orders for customers at this hurdle band.'
        ]);

    }

    protected function getData($from, $to)
    {
        $query = "SELECT 
                        CASE 
                            WHEN dw_order.order_count = '1' THEN 1
                            WHEN dw_order.order_count = '2' THEN 2
                            WHEN dw_order.order_count = '3' THEN 3
                            WHEN dw_order.order_count = '4' THEN 4
                            WHEN dw_order.order_count = '5' THEN 5
                            WHEN dw_order.order_count BETWEEN '6' AND '10' THEN 6
                            WHEN dw_order.order_count BETWEEN '11' AND '20' THEN 7    
                            WHEN dw_order.order_count BETWEEN '21' AND '50' THEN 8	    
                            WHEN dw_order.order_count BETWEEN '51' AND '100' THEN 9	    
                            ELSE 10
                        END AS band,
                        CASE 
                            WHEN dw_order.order_count = '1' THEN '1 order'
                            WHEN dw_order.order_count = '2' THEN '2 orders'
                            WHEN dw_order.order_count = '3' THEN '3 orders'
                            WHEN dw_order.order_count = '4' THEN '4 orders'
                            WHEN dw_order.order_count = '5' THEN '5 orders'
                            WHEN dw_order.order_count BETWEEN '6' AND '10' THEN '6-10 orders'
                            WHEN dw_order.order_count BETWEEN '11' AND '20' THEN '11-20 orders'	    
                            WHEN dw_order.order_count BETWEEN '21' AND '50' THEN '21-50 orders'	    
                            WHEN dw_order.order_count BETWEEN '51' AND '100' THEN '51-100 orders'	    
                            ELSE 'Over 100 orders'
                        END AS orders_band,
                        SUM(IF(DATEDIFF(
                                         dw_order.invoiced_at, 
                                         (SELECT `invoiced_at`
                                          FROM `dw_order` as Order2
                                          WHERE Order2.`customer_id` = dw_order.customer_id
                                          AND Order2.`order_count` = dw_order.order_count -1
                                         )
                                        ) > 0 , 
                                        DATEDIFF(
                                         dw_order.invoiced_at, 
                                         (SELECT `invoiced_at`
                                          FROM `dw_order` as Order2
                                          WHERE Order2.`customer_id` = dw_order.customer_id
                                          AND Order2.`order_count` = dw_order.order_count -1
                                         )
                                        ),
                                        0
                               )
                           ) / COUNT(dw_order.order_count) AS average_latency
                    FROM dw_order
                    WHERE dw_order.invoiced_at BETWEEN :from AND :to ";

        if ($this->getFilter('channel')!='') {
            $query.=" AND dw_order.channel = :channel ";
        }

        $query.=" GROUP BY orders_band ORDER BY band asc";

        $data = select($query, [
            'from' => $from,
            'to' => $to,
            'channel' => $this->getFilter('channel')
        ]);

        return $data;
    }
}
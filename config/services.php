<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => env('SES_REGION', 'us-east-1'),
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],
    'legacy-api-live' => [
        'create-person' => 'https://api.NewCo.networks.com/people?api_key=' . env("LEGACY_API_KEY") . '&noRedirect=1',
        'person-history' => 'https://api.NewCo.networks.com/communications/$memberID/history/all',
        'person-engagement' => 'https://api.NewCo.networks.com/people/$memberID/engagement'
    ],
    'legacy-api-dev' => [
        'create-person' => 'http://dev-api.NewCo.networks.com/people?api_key=' . env("LEGACY_API_KEY") . '&noRedirect=1',
        'person-history' => 'http://dev-api.NewCo.networks.com/communications/:memberID/history/all',
        'person-engagement' => 'http://dev-api.NewCo.networks.com/people/$memberID/engagement'

    ],
    'payment-service' => [
        'development' => 'https://dev-payments.NewCo.networks.com/payments',
        'production'  => 'https://billing.api.NewCo.networks.com/payments',
    ],
    'payment-subscription' => [
        'development' => 'https://dev-payments.NewCo.networks.com/subscription',
        'production'  => 'https://billing.api.NewCo.networks.com/subscription',
    ],
    'payment-plans' => [
        'development' => 'https://dev-payments.NewCo.networks.com/products',
        'production'  => 'https://billing.api.NewCo.networks.com/products',
    ]

];

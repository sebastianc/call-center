<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\User;
use App\Team;


Auth::routes();

Route::get('/', function () {
    return redirect()->route('crm.dashboard');
});

Route::prefix('crm')->name('crm.')->middleware(['auth'])->group(function () {
    /**
     * User-facing screens
     */
    Route::get('/', 'CRMController@dashboard')->name('dashboard'); // Base CRM route
    Route::get('table/{list}', 'CRMController@listObjects')->name('list');
    Route::get('edit/{class}/{id?}', 'CRMController@editObject')->name('edit');
    Route::get('create/new/{class}', 'CRMController@createObject')->name('create');
    Route::get('history/{class}/{id}', "CRMController@history")->name('history');

    /**
     * Datatables
     */
    Route::get('data/{list}/{parent_id?}', 'DataController@getData')->name('dataTableData');
    Route::post('editor-update/interest', 'DataController@processInterestUpdate')->name('update-interest');
    Route::post('editor-update/{list}', 'DataController@processEditorUpdate')->name('update');
    Route::post('increase-discounts/{lead_id}', 'DataController@increaseDiscounts')->name('increase-discounts');

    /**
     * Form posts
     */
    Route::post('update/team/{team_id?}', 'FormController@updateTeam')->name('update-team');

    Route::post('update/lead/{id}', 'FormController@updateLead')->name('update-lead');
    Route::post('update/lead/{id}/status', 'FormController@updateStatus')->name('update-status');
    Route::post('update/lead/{id}/owner', 'FormController@updateOwner')->name('update-owner');
    Route::post('update/lead/{id}/callback', 'FormController@updateCallback')->name('update-callback');
    Route::post('update/call', 'FormController@updateCall')->name('update-call');

    Route::post('update/lead/{id}/note/add', 'FormController@addNote')->name('add-note');
    Route::post('update/lead/{id}/sms/add', 'SMSController@sendSMS')->name('send-sms');

    Route::get('update/lead/{id}/delete-callback/{callbackId}', 'FormController@deleteCallback')->name('delete-callback');

    Route::post('create/user', 'FormController@createUser')->name('create-user');
    Route::post('update/user/{id}', 'FormController@editUser')->name('update-user');
    Route::post('update/user_image/{id}', 'FormController@editUserImage')->name('update-image');

    Route::post('member-survey', 'FormController@processMemberSurvey')->name('member-survey');

    Route::get('get-blocking-modal/{lead_id?}', 'CRMController@getBlockingModal')->name('get-blocking-modal');

    /**
     * Front-end scripts for returning content blocks to Ajax
     */
    Route::any('ajax/block/{block}/{id?}', 'CRMController@renderAjaxBlock')->name('ajax-block');

    // Impersonate routes ::
    Route::get('impersonate/start/{who}', function ($who) {
        $im = app('impersonate');
        $iam = $im->findUserById(\Illuminate\Support\Facades\Auth::user()->id);
        $whoiwanttobe = $im->findUserById($who);
        $im->take($iam, $whoiwanttobe);
        return redirect('/crm');
    });
    Route::get('impersonate/stop', function () {
        $im = app('impersonate');
        $im->leave();
        return redirect('/crm');
    })->name('stop-impersonation');

    /**
     * Manage Catalogue Items
     */

    Route::get('catalogue/view/{action}/{id?}','CatalogueItemController@views')->name('catalogue-items');
    Route::post('catalogue/update/{id}','CatalogueItemController@update')->name('update-catalogue-item');
    Route::post('catalogue/create','CatalogueItemController@create')->name('create-catalogue-item');
    Route::get('catalogue/delete/{id}','CatalogueItemController@delete')->name('delete-catalogue-item');
    Route::get('catalogue/reactivate/{id}','CatalogueItemController@reactivate')->name('reactivate-catalogue-item');
});

//Payment Routes
Route::prefix('payment')->name('payment.')->middleware(['auth'])->group(function () {

    Route::get('build/{id}','PaymentController@orderBuilder')->name('build');
    Route::post('full/process/{id}','PaymentController@processPayment')->name('full-payment');
    Route::post('partial/process/{id}','PaymentController@processPartPayment')->name('partial-payment');
    Route::get('defer/process/{id}','PaymentController@defferPayment')->name('defer-payment');
    Route::post('process/subscription','PaymentController@processSubscriptionPayment');
    Route::get('view/outstanding','PaymentController@viewOutstanding');
    Route::post('update/outstanding','PaymentController@outstandingPaymentUpdate')->name('additional-payment');

});
// SMS routes
Route::prefix('sms')->name('sms.')->middleware(['auth'])->group(function () {

    Route::get('balance','SMSController@readBalance');
    Route::get('send','SMSController@sendMessage');


});
// Reporting
Route::prefix('report')->name('report.')->middleware(['auth'])->group(function () {
    Route::get('/all', 'ReportingController@reportList')->name('exportable');
    Route::get('leadStatus/{startPoint}/{endPoint}', 'ReportingController@leadStatusRequest');
    Route::get('leadCallStats/{startPoint}/{endPoint}', 'ReportingController@leadCallsRequest');
    Route::get('leadCallbacks', 'ReportingController@leadCallsBacksRequest');
    Route::get('leadSources/{length?}', 'ReportingController@leadSourceRequest');
    Route::get('userSales/{id}','ReportingController@userSalesRequest');
    Route::get('teamSales/{teamID}','ReportingController@teamSalesRequest');

});


// Test Routes
Route::prefix('test')->name('test.')->group(function () {

    Route::get('allocation','APIController@leadDistribution');

    Route::get('curl','ExternalController@makePerson');

    Route::get('sms',"SMSController@readMessages");
    Route::get('sms-verify/{recipient}',"SMSController@verifyNumber");

    Route::get('sales',"PaymentController@updateInterestsPartial");

    Route::get('regex-test',function (){
        $lead = \App\Lead::find(54);
        echo "Is mobile ? ". $lead->isUkMobile();
    });

    Route::get('engagement/{leadID}','ExternalController@getEngagementHistory');


});

// Base Home route
Route::get('/home', 'HomeController@index')->name('home');

// entry point for
Route::post('/external/sms/inbound', 'SMSController@readMessage');

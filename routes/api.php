<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api','throttle:10,1')->get('/user', function (Request $request) {
    return $request->user();
});


Route::middleware('auth:api','throttle:50,1')->post("/lead/precapture","APIController@generateNewLead");
Route::middleware('auth:api','throttle:50,1')->post("/lead/member","APIController@migrateCustomer");
Route::middleware('auth:api','throttle:50,1')->post("/lead/email","APIController@recordEmailActions");